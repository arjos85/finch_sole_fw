#This sets the root directory so the code knows where to find the SDK
export NRFSDK=$(pwd)/sdk
export NRFBASE=$(pwd)
echo "Setting NRFSDK to $NRFSDK"
echo "You need to run this as '. ./setup.sh' so it's exported into the global shell"