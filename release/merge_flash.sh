export NRFSDK=$(pwd)/sdk
export NRFBASE=$(pwd)
echo "Setting NRFSDK to $NRFSDK"

# Combine them
echo "Combining the files with settings"
cd $NRFBASE/release/output
cp ../s132_nrf52_2.0.1_softdevice.hex softdevice.hex
cp ../app_valid_setting_apply_nRF52832.hex app_valid_setting_apply.hex

# Generate a private key and store it in a file named private.pem
#nrfutil keys --gen-key key.pem

# Generate Bootloader DFU settings file, which must be present on the last page of available flash memory for the bootloader to function correctly
# No need at this stage since we already have the file app_valid_setting_apply_nRF52832.hex
#nrfutil settings generate --family NRF52 --application application.hex --application-version X --bootloader-version Y --bl-settings-version Z sett.hex

# Generate a DFU package in debug mode (no need to specify versions for hardware and firmware)
# According to Nordic support, with the actual version of SDK (11.0) it is only possible to perform DFU for Application, SD or BL only or BL+SD together. 
nrfutil dfu genpkg --bootloader bootloader.hex --softdevice softdevice.hex BL_SD_dfu_package.zip
nrfutil dfu genpkg --application application.hex APP_dfu_package.zip


mergehex --merge softdevice.hex bootloader.hex --output SD_BL.hex
mergehex --merge SD_BL.hex application.hex --output SD_BL_APP.hex
mergehex --merge SD_BL_APP.hex app_valid_setting_apply.hex --output SD_BL_APP_valid.hex

echo "Done and file is ready for burning"
ls -l SD_BL_APP_valid.hex

../burn.sh
