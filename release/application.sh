export NRFSDK=$(pwd)/sdk
export NRFBASE=$(pwd)
echo "Setting NRFSDK to $NRFSDK"

# Build the app
echo "Building the application"
cd $NRFBASE/droplet/application
make clean
make application.out
cp _build/application.hex $NRFBASE/release/output