export NRFSDK=$(pwd)/sdk
export NRFBASE=$(pwd)
echo "Setting NRFSDK to $NRFSDK"

# Build the build loader
echo "Building the bootloader"
cd $NRFBASE/droplet/bootloader
make clean
make
cp _build/bootloader.hex $NRFBASE/release/output