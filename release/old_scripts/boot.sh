#This will make a release/burnable build


# Build the build loader
echo "Building the bootloader"
cd $NRFBASE/droplet/bootloader
make clean
make
cp _build/bootloader.hex $NRFBASE/release/output

# Combine them
echo "Combining the files with settings"
cd $NRFBASE/release/output
cp ../s132_nrf52_2.0.1_softdevice.hex softdevice.hex
cp ../app_valid_setting_apply_nRF52832.hex app_valid_setting_apply.hex

$NRFBASE/tools/mergehex/mergehex --merge softdevice.hex bootloader.hex --output SD_BL.hex
$NRFBASE/tools/mergehex/mergehex --merge SD_BL.hex application.hex --output SD_BL_APP.hex
$NRFBASE/tools/mergehex/mergehex --merge SD_BL_APP.hex app_valid_setting_apply.hex --output SD_BL_APP_valid.hex

echo "Burning the release"
cd $NRFBASE/release/output
$NRFBASE/tools/nrfjprog/nrfjprog --family NRF52 -e
$NRFBASE/tools/nrfjprog/nrfjprog --family NRF52 --program SD_BL_APP_valid.hex -r 
