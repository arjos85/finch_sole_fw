#This will make a release/burnable build

echo "Prepping build"
rm -rf output
mkdir output

# Build the build loader
echo "Building the bootloader"
cd $NRFBASE/droplet/bootloader
make clean
make
cp _build/bootloader.hex $NRFBASE/release/output

# Build the app
echo "Building the application"
cd $NRFBASE/droplet/application
make clean
make
cp _build/application.hex $NRFBASE/release/output

# Combine them
echo "Combining the files with settings"
cd $NRFBASE/release/output
cp ../s132_nrf52_2.0.1_softdevice.hex softdevice.hex
cp ../app_valid_setting_apply_nRF52832.hex app_valid_setting_apply.hex

$NRFBASE/tools/mergehex/mergehex --merge softdevice.hex bootloader.hex --output SD_BL.hex
$NRFBASE/tools/mergehex/mergehex --merge SD_BL.hex application.hex --output SD_BL_APP.hex
$NRFBASE/tools/mergehex/mergehex --merge SD_BL_APP.hex app_valid_setting_apply.hex --output SD_BL_APP_valid.hex

echo "Done and file is ready for burning"
ls -l SD_BL_APP_valid.hex
