/* Copyright (c) 2013 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/**@file
 *
 * @defgroup ble_sdk_app_bootloader_main main.c
 * @{
 * @ingroup dfu_bootloader_api
 * @brief Bootloader project main file.
 *
 * -# Receive start data packet.
 * -# Based on start packet, prepare NVM area to store received data.
 * -# Receive data packet.
 * -# Validate data packet.
 * -# Write Data packet to NVM.
 * -# If not finished - Wait for next packet.
 * -# Receive stop data packet.
 * -# Activate Image, boot application.
 *
 */
#include "dfu_transport.h"
#include "bootloader.h"
#include "bootloader_util.h"
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_soc.h"
#include "app_error.h"
#include "nrf_gpio.h"
#include "ble.h"
#include "nrf.h"
#include "ble_hci.h"
#include "app_scheduler.h"
#include "app_timer_appsh.h"
#include "nrf_error.h"
#include "bsp.h"
#include "softdevice_handler_appsh.h"
#include "pstorage_platform.h"
#include "nrf_mbr.h"
#include "custom_board.h"
#include "nrf_delay.h"
#include "logger.h"
#include "nrf_nvmc.h"

#include "simple_uart.h"

#define IS_SRVC_CHANGED_CHARACT_PRESENT 1                                                       /**< Include the service_changed characteristic. For DFU this should normally be the case. */

#define BOOTLOADER_BUTTON               BUTTON_1                                                /**< Button used to enter SW update mode. */
#define DIAGS_BUTTON                    BUTTON_2                                                /**< Button used to enter diagnostics mode. */
#define UPDATE_IN_PROGRESS_LED          LED_RED                                                 /**< Led used to indicate that DFU is active. */
#define DEBUG_LED                       LED_GREEN                                               /**< Helper LED */

#define APP_TIMER_PRESCALER             0                                                       /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                                       /**< Size of timer operation queues. */

#define SCHED_MAX_EVENT_DATA_SIZE       MAX(APP_TIMER_SCHED_EVT_SIZE, 0)                        /**< Maximum size of scheduler events. */

#define SCHED_QUEUE_SIZE                20                                                      /**< Maximum number of events in the scheduler queue. */

#define GPREGRET_RMRST_MASK             0x3                                                     /**< The 2 LSBs of GPREGRET register are reserverd for the Roam Reset bits. */



/**@brief Function for switching a LED on.
 * 
 * It simplifies the code change for supporting different PCBs
 */
void led_on(uint32_t pin_number)
{

#if !DROPLET_RELEASE_MODE


#ifdef LED_CONNECTED_TO_VDD
    // LED are direcly connected to VDD and GPIO pin <pin_number>
    nrf_gpio_pin_clear(pin_number);
#else // LED_CONNECTED_TO_VDD
    // LED are direcly connected to GND and GPIO pin <pin_number>
    nrf_gpio_pin_set(pin_number);
#endif // LED_CONNECTED_TO_VDD


#endif // DROPLET_RELEASE_MODE

}

/**@brief Function for switching a LED off.
 * 
 * It simplifies the code change for supporting different PCBs
 */
void led_off(uint32_t pin_number)
{

#if !DROPLET_RELEASE_MODE


#ifdef LED_CONNECTED_TO_VDD
    // LED are direcly connected to VDD and GPIO pin <pin_number>
    nrf_gpio_pin_set(pin_number);
#else // LED_CONNECTED_TO_VDD
    // LED are direcly connected to GND and GPIO pin <pin_number>
    nrf_gpio_pin_clear(pin_number);
#endif // LED_CONNECTED_TO_VDD


#endif // DROPLET_RELEASE_MODE

}


/**@brief Function for error handling, which is called when an error has occurred.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of error.
 *
 * @param[in] error_code  Error code supplied to the handler.
 * @param[in] line_num    Line number where the handler is called.
 * @param[in] p_file_name Pointer to the file name.
 */
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
    //led_on(ASSERT_LED_PIN_NO);
    
    // This call can be used for debug purposes during application development.
    // @note CAUTION: Activating this code will write the stack to flash on an error.
    //                This function should NOT be used in a final product.
    //                It is intended STRICTLY for development/debugging purposes.
    //                The flash write will happen EVEN if the radio is active, thus interrupting
    //                any communication.
    //                Use with care. Un-comment the line below to use.
    //ble_debug_assert_handler(error_code, line_num, p_file_name);
    
    RMLOG("Error 0x%08X at line %u in file %s", error_code, line_num, p_file_name);
    
    // On assert, the system can only recover on reset.
    NVIC_SystemReset();
}


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}

/**
 * Beeps buzzer using delay, so no GPIOTE which sucks up juice
 */

void buzzer_direct_buzz(void)
{
#if 0
    uint16_t i=0;
    
    nrf_gpio_cfg_output(BUZZER_OUT_0);
    nrf_gpio_cfg_output(BUZZER_OUT_1);
    nrf_gpio_cfg_output(BUZZER_IN_0);
    
    nrf_gpio_pin_clear(BUZZER_OUT_0);
    nrf_gpio_pin_clear(BUZZER_OUT_1);
    nrf_gpio_pin_set(BUZZER_IN_0);
    
    for (i=0; i<4300*4; i++) {
        nrf_gpio_pin_toggle(BUZZER_OUT_0);
        nrf_gpio_pin_toggle(BUZZER_OUT_1);
        nrf_gpio_pin_toggle(BUZZER_IN_0);
        nrf_delay_us((1000000/4700)/2);
    }
#endif
}

/**
 * Read the model number from the two pins
 */

uint8_t model_identifier()
{
#if 0
    nrf_gpio_cfg_sense_input(MODEL_0,
                             BUTTON_PULL,
                             NRF_GPIO_PIN_SENSE_LOW);
    
    nrf_gpio_cfg_sense_input(MODEL_1,
                             BUTTON_PULL,
                             NRF_GPIO_PIN_SENSE_LOW);
    
    return (nrf_gpio_pin_read(MODEL_0) | (nrf_gpio_pin_read(MODEL_1) << 1));
#else
    return (0);
#endif
}


/**
 * Performs some diagnostics
 */


void perform_diagnostics(void)
{
    
    RMLOG(" - Starting diagnostics");
    RMLOG(" - Testing the LEDs");
    
    // Toggle the LEDS
    led_on(LED_1);
    nrf_delay_us(1000000);
    led_on(LED_2);
    nrf_delay_us(1000000);
    led_off(LED_1);
    nrf_delay_us(1000000);
    led_off(LED_2);
    nrf_delay_us(1000000);
    
    
    // Tickle the Piezo
    RMLOG(" - Testing the Piezo buzzer");
    buzzer_direct_buzz();
    
    // Read and display the model number
    RMLOG(" - Testing the model (read 0x%02X)", model_identifier());
    
    // NFC, lets skip now, just confirm it's pinned to pads
    RMLOG(" - Skipping NFC test (confirm its pinned out to connector/pads)");
    
    
    RMLOG(" - Diagnostics complete, please power cycle");
    
}


/**@brief Function for clearing the LEDs.
 *
 * @details Clears all LEDs used by the application.
 *
 * Starting from LifeSole Rev.2 LEDs are directly connected to ground, while before they were connected to Vdd
 */
static void leds_off(void)
{
    // From Rev.2 LEDs are switched off by CLEARing the relevant GPIO pins
    led_off(LED_RED);
    led_off(LED_GREEN);
}

/**@brief Function for initialization of LEDs.
 */
static void leds_init(void)
{
    nrf_gpio_cfg_output(LED_RED);
    nrf_gpio_cfg_output(LED_GREEN);
    
    leds_off();
}


/**@brief Function for initializing the timer handler module (app_timer).
 */
static void timers_init(void)
{
    // Initialize timer module, making it use the scheduler.
    APP_TIMER_APPSH_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, true);
    RMLOG(" - Timer initialised");
}


/**@brief Function for initializing the button module.
 */
static void buttons_init(void)
{
    if (BUTTONS_NUMBER) {
        nrf_gpio_cfg_sense_input(BOOTLOADER_BUTTON,
                                 BUTTON_PULL,
                                 NRF_GPIO_PIN_SENSE_LOW);
        
        nrf_gpio_cfg_sense_input(DIAGS_BUTTON,
                                 BUTTON_PULL,
                                 NRF_GPIO_PIN_SENSE_LOW);
        
        // Give it time to settle (100 ms)
        nrf_delay_us(100000);
    }
    else {
        RMLOG(" - No buttons on this board");
    }
}


/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the scheduler in the main loop after a BLE stack
 *          event has been received.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void sys_evt_dispatch(uint32_t event)
{
    pstorage_sys_event_handler(event);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 *
 * @param[in] init_softdevice  true if SoftDevice should be initialized. The SoftDevice must only
 *                             be initialized if a chip reset has occured. Soft reset from
 *                             application must not reinitialize the SoftDevice.
 */

static void ble_stack_init(bool init_softdevice)
{
    uint32_t         err_code;
    sd_mbr_command_t com = {SD_MBR_COMMAND_INIT_SD, };
    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
    
    if (init_softdevice)
    {
        err_code = sd_mbr_command(&com);
        APP_ERROR_CHECK(err_code);
    }
    
    err_code = sd_softdevice_vector_table_base_set(BOOTLOADER_REGION_START);
    APP_ERROR_CHECK(err_code);
    
    SOFTDEVICE_HANDLER_APPSH_INIT(&clock_lf_cfg, true);
    
    // Enable BLE stack.
    ble_enable_params_t ble_enable_params;
    // Only one connection as a central is used when performing dfu.
    err_code = softdevice_enable_get_default_config(1, 1, &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);

}


/**@brief Function for event scheduler initialization.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}


/**@brief Function for bootloader main entry.
 */
int main(void)
{

    // Initial check if nRF52's NFC pins (NFC1 and NFC2) are set as GPIO pins (P0.09 and P0.10)
    if (NRF_UICR->NFCPINS & 0x00000001) {
        nrf_nvmc_write_word((uint32_t) &(NRF_UICR->NFCPINS), 0xFFFFFFFE);

        NVIC_SystemReset();
    }
    else
    {
        //NRF_POWER->GPREGRET = 0;
        //RMLOG("DFU: NEW FIRMARE 4.0!");
    }

    uint32_t err_code;
    // GPREGRET (8bits) stores information about Proximity (bit 7), Lifesole State Machine (bits 6-2) and DFU request (bits 1-0)
    bool     app_reset = (NRF_POWER->GPREGRET & GPREGRET_RMRST_MASK);
    bool     dfu_start = false;

    // LEDs initialization
    leds_init();


    // Log startup message
    log_init();

    RMLOG("Roam Creative Ltd - Lifesole 2017");
    RMLOG(" - Bootloader started");

    // This check ensures that the defined fields in the bootloader corresponds with actual
    // setting in the nRF51/nRF52 chip.
    APP_ERROR_CHECK_BOOL(*((uint32_t *)NRF_UICR_BOOT_START_ADDRESS) == BOOTLOADER_REGION_START);
    RMLOG("NRF_FICR->CODEPAGESIZE : 0x%X, CODE_PAGE_SIZE: 0x%X ", NRF_FICR->CODEPAGESIZE, CODE_PAGE_SIZE);

    // TODO: check why following test fails!
    APP_ERROR_CHECK_BOOL(NRF_FICR->CODEPAGESIZE == CODE_PAGE_SIZE);

    bool app_valid = bootloader_app_is_valid(DFU_BANK_0_REGION_START);
    //bootloader_app_is_valid(DFU_BANK_1_REGION_START);

    RMLOG("Roam Creative Ltd - Lifesole 2017");
    RMLOG(" - Bootloader started");
    
    // Initialize.
    if (app_reset || !app_valid) {
        // No Scheduler/Timer needed when in fast-boot mode
        timers_init();
    }
    
    buttons_init();

#if 0
    // Perform diagnostics (ALWAYS)
    if (((nrf_gpio_pin_read(DIAGS_BUTTON) == 0) ? true: false)) {
        RMLOG(" - Diagnostics button detected");
        perform_diagnostics();
    }
    
    // Give it a tickle
    buzzer_direct_buzz();
#endif
    
    (void)bootloader_init();

        
    if (bootloader_dfu_sd_in_progress())
    {
        RMLOG(" - DFU in progress");
        
        led_on(UPDATE_IN_PROGRESS_LED);
        
        err_code = bootloader_dfu_sd_update_continue();
        APP_ERROR_CHECK(err_code);
        
        ble_stack_init(!app_reset);
        scheduler_init();
        
        err_code = bootloader_dfu_sd_update_finalize();
        APP_ERROR_CHECK(err_code);
        
        led_off(UPDATE_IN_PROGRESS_LED);
    }
    else
    {

        RMLOG(" - No DFU in progress");

        
        if (!app_reset && app_valid) {
            // If no DFU has been requested, skip the BLE initialization and go straight to the application
            // The following is a reduced version of ble_stack_init()
            RMLOG(" - NOT Initing BLE");
            RMLOG(" - NOT Initing Scheduler");
            RMLOG(" - Initing Softdevice Handler");
    
            uint32_t         err_code;
            sd_mbr_command_t com = {SD_MBR_COMMAND_INIT_SD, };
            err_code = sd_mbr_command(&com);
            APP_ERROR_CHECK(err_code);
        }
        else {
            // If we're in special Roam reset
            switch (NRF_POWER->GPREGRET & GPREGRET_RMRST_MASK)
            {
                case 0x01:
                    RMLOG(" - DFU from app restart");
                    app_reset = true;
                    ble_stack_init(true);
                    break;

                case 0x02:
                    // We restarted after timeout in bootloader... but don't do DFU again unless of course invalid image..
                    RMLOG(" - DFU from timeout");
                    ble_stack_init(true);
                    break;

                default:
                    RMLOG(" - DFU from app restart");
                    // Follow the original, which is DFU from the app...
                    //ble_stack_init(!app_reset);
                    ble_stack_init(true);
            }

            // Enable DC/DC converter on Lifesole rev. 3
#ifdef DCDCEN
            RMLOG(" - Enabling DC/DC converter (Radio power saving)");
            sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
#else
            RMLOG(" - No DC/DC converter support");
#endif
        
            if (app_reset)
            {
                /* Test value to use to get out of DFU mode if nRF reboots because of a timeout or an error */
                sd_power_gpregret_clr(0xFF);
            }

            RMLOG(" - Initing Scheduler");
            scheduler_init();

        }
    }
    
    dfu_start  = app_reset;

    /*
     * If we have any buttons (yes it'll fail if we used IO line 0..) lets check it
     */
    
    if (BUTTONS_NUMBER && BOOTLOADER_BUTTON) {
        if (((nrf_gpio_pin_read(BOOTLOADER_BUTTON) == 0) ? true: false)) {
            RMLOG(" - Bootloader button detected");
            dfu_start |= true;
        }
    }
    else {
        RMLOG(" - No bootloader button");
    }

    app_valid = bootloader_app_is_valid(DFU_BANK_0_REGION_START);
    //bootloader_app_is_valid(DFU_BANK_1_REGION_START);

    if (dfu_start || (!app_valid))
    {
        RMLOG(" - Starting DFU advertising");

        if (!app_valid) {
            RMLOG("Application at DFU_BANK_0_REGION_START is not valid!");
        }
        
        led_off(LED_RED);
        
        // Initiate an update of the firmware.
        err_code = bootloader_dfu_start();

        APP_ERROR_CHECK(err_code);
        
        led_on(LED_RED);
    }
    
    if (bootloader_app_is_valid(DFU_BANK_0_REGION_START))
    {
        leds_off();
        
        // Select a bank region to use as application region.
        // @note: Only applications running from DFU_BANK_0_REGION_START is supported.
        bootloader_app_start(DFU_BANK_0_REGION_START);
    }
#if 0    
    else 
    {
        if (bootloader_app_is_valid(DFU_BANK_1_REGION_START))
        {            
            leds_off();
            RMLOG(" - Starting application from DFU_BANK_1_REGION_START");
            
            // Select a bank region to use as application region.
            // @note: Only applications running from DFU_BANK_0_REGION_START is supported.
            bootloader_app_start(DFU_BANK_1_REGION_START);
        }
    }
#endif

    RMLOG(" - Resetting");

    leds_off();
    
    NVIC_SystemReset();
    
}
