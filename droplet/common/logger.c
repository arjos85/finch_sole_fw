/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * Module: Logger
 *
 * Sends to local UART when not building for release.
 *
 * (c) Roam Creative Ltd 2014
 *
 */
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "custom_board.h"
#include "logger.h"
#include "simple_uart.h"


uint8_t bLoggingReady=false;

/*
 * Init for 38k baud
 */

void log_init(void)
{
#if DROPLET_USE_UART_LOGGING
	// Actual baudrate: 256000
	simple_uart_config (RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, HWFC, UARTE_BAUDRATE_BAUDRATE_Baud230400);
	bLoggingReady = true;
#endif
}

/*
 * Disable the logging (for when the UART is needed for the ESP module
 */

void log_shutdown(void)
{
	//RMLOG("Shutting down the logging UART connection");
	bLoggingReady = false;
}


/*
 * Logs with args, builds a string etc..
 */

void log_output(const char *fmt, ...)
{
#if DROPLET_USE_UART_LOGGING

	// Sanity
	if (!bLoggingReady) {
		return;
	}
		
	char tmp_buff[128];
	va_list ap;
    va_start(ap, fmt);
    vsnprintf(tmp_buff, sizeof(tmp_buff), fmt, ap);
    va_end(ap);
				
	// Print newline
	simple_uart_putstring((const uint8_t *) tmp_buff);
	simple_uart_putstring((const uint8_t *) "\r\n");
    
#endif
}

/*
 * Logs a string out to the UART
 */

void log_string(char *string)
{
#if DROPLET_USE_UART_LOGGING
	simple_uart_putstring((const uint8_t *)string);
#endif
}


