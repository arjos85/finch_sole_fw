/*
 * This is the board for the Droplet CR2477 based on the nRF52832
 *
 * Contains our sensors
 *  - 9DOF
 *  - Capacitive touch
 *
 * REMEMBER TO MAP THE HARDWARE PIN NUMBER TO THE P0.xx I/O LINE NUMBER
 *
 */

#ifndef DROPLET_CR_52_H
#define DROPLET_CR_52_H

#include "nrf_gpio.h"

#if USING_DK
#error "You can't run this on the DK"
#endif


// ------------------------------------------ BSP HW Defines ---------------------------------------------

#define BOARD_NAME      "Lifesole"

#define BUTTONS_NUMBER 0
#define BUTTON_START   0
#define BUTTON_1       0
#define BUTTON_2       0
#define BUTTON_STOP    0

#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP
#define BUTTONS_LIST { BUTTON_1, BUTTON_2}

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)
#define BSP_BUTTON_1_MASK (1<<BSP_BUTTON_1)


// Used starting from LifeSole Rev.2 for reducing power consumption when using Radio module  
#define DCDCEN


// Define NRF_DEV_KIT when firmware used for prototyping with Nordic nRF52 Dev Kit
#undef NRF_DEV_KIT

#ifndef NRF_DEV_KIT

// Not validated on this board - http://smittytone.wordpress.com/2013/04/15/connect-a-raspberry-pi-to-a-mac-using-a-usb-serial-adapter/
// Used by UART 0 to connect to outside world for debug port
//#define RX_PIN_4_DBG   29
#ifdef RX_PIN_4_DBG
#define RX_PIN_NUMBER  26 // Actually not used/wired at all! Before it was set to 30 (now used for testing)  // Pin 	-> FTDI Red
#else
#define RX_PIN_NUMBER  29 // Actually not used/wired at all! Before it was set to 30 (now used for testing)  // Pin 	-> FTDI Red
#endif
#define TX_PIN_NUMBER  28                           		// Pin  -> FTDI Yellow
#define CTS_PIN_NUMBER 0
#define RTS_PIN_NUMBER 0
#define HWFC           false

#define SEMTECH_SX9300_ADDR       0x28
#define SEMTECH_POWER               16 // SX_EN on schematic
#define SEMTECH_SVDD                27
#define SEMTECH_TXEN                14
#define SEMTECH_NRST                15

#define SEMTECH_I2C_CONFIG_SDA      12
#define SEMTECH_I2C_CONFIG_SCL       9
#define SEMTECH_INTERRUPT            8

#define ADXL_POWER                  31
#define ADXL_SCK_PIN                 2
#define ADXL_MOSI_PIN                3
#define ADXL_MISO_PIN                4
#define ADXL_CS_PIN                  5
#define ADXL_INT_PIN                 6
#define ADXL_IRQ_PRIORITY           APP_IRQ_PRIORITY_LOW

#define LED_START      20
#define LED_1          20
#define LED_2          21
#define LED_STOP       21


#else // #ifndef NRF_DEV_KIT

// Not validated on this board - http://smittytone.wordpress.com/2013/04/15/connect-a-raspberry-pi-to-a-mac-using-a-usb-serial-adapter/
// Used by UART 0 to connect to outside world for debug port
#define RX_PIN_NUMBER  21 // not used (RESET GPIO pin)		// Pin 	-> FTDI Red
#define TX_PIN_NUMBER  22									// Pin  -> FTDI Yellow
#define CTS_PIN_NUMBER 0
#define RTS_PIN_NUMBER 0
#define HWFC           false

#define SEMTECH_SX9300_ADDR       0x2B 
#define SEMTECH_POWER               18 // SX_EN on schematics
#define SEMTECH_SVDD                17
#define SEMTECH_TXEN                11
#define SEMTECH_NRST                12

#define SEMTECH_I2C_CONFIG_SDA      13
#define SEMTECH_I2C_CONFIG_SCL      14
#define SEMTECH_INTERRUPT           15

#define ADXL_POWER                  27
#define ADXL_SCK_PIN                26
#define ADXL_MOSI_PIN                2
#define ADXL_MISO_PIN               25
#define ADXL_CS_PIN                 24
#define ADXL_INT_PIN                23
#define ADXL_IRQ_PRIORITY           APP_IRQ_PRIORITY_LOW

#define LED_START      20
#define LED_1          20
#define LED_2          19
#define LED_STOP       19

#define TEST_PIN_BOOTLOADER         28
#define TEST_PIN_MAIN               29

#endif // #ifndef NRF_DEV_KITs


// LEDs definitions for Droplet
#define LEDS_NUMBER    2

#define LED_RED    					LED_1
#define LED_GREEN  					LED_2

#define LED_3          LED_2

#define LEDS_LIST {LED_1, LED_2}

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK)

/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  LEDS_MASK


// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,            \
.rc_ctiv       = 0,                                \
.rc_temp_ctiv  = 0,                                \
.xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}


#endif
