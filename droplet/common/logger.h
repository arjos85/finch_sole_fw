
#ifndef LOGGER_H__
#define LOGGER_H__

/*lint ++flb "Enter library region" */

#include <stdbool.h>
#include <stdint.h>
#include "custom_board.h"

//#define DROPLET_USE_UART_LOGGING true       Disable due to application definition elsewhere

// Macros
 #if !DROPLET_USE_UART_LOGGING
 	#define RMLOG(fmt, ...)
 #else
	#define RMLOG(fmt, ...) log_output(fmt, ##__VA_ARGS__)
#endif

// Functions
void log_init(void);
void log_shutdown(void);
void log_string(char *string);
void log_output(const char *fmt, ...);


/*lint --flb "Leave library region" */

#endif // LOGGER_H__
