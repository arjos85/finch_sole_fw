/*
 * This is the board for the Droplet CR2477 based on the nRF52832
 *
 * Contains our sensors
 *  - 9DOF
 *  - Capacitive touch
 *
 * REMEMBER TO MAP THE HARDWARE PIN NUMBER TO THE P0.xx I/O LINE NUMBER
 *
 */

#ifndef DROPLET_CR_52_H
#define DROPLET_CR_52_H

#include "nrf_gpio.h"

#if USING_DK
#error "You can't run this on the DK"
#endif

// ------------------------------------------ BSP HW Defines ---------------------------------------------

#define BOARD_NAME      "Lifesole"

// LEDs definitions for Droplet
#define LEDS_NUMBER    2

#define LED_START      16
#define LED_1          16
#define LED_2          20
#define LED_STOP       20

#define LED_3          LED_2

#define LEDS_LIST {LED_1, LED_2}

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK)

/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  LEDS_MASK

#define BUTTONS_NUMBER 0
#define BUTTON_START   0
#define BUTTON_1       0
#define BUTTON_2       0
#define BUTTON_STOP    0

#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP
#define BUTTONS_LIST { BUTTON_1, BUTTON_2}

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)
#define BSP_BUTTON_1_MASK (1<<BSP_BUTTON_1)


// Not validated on this board - http://smittytone.wordpress.com/2013/04/15/connect-a-raspberry-pi-to-a-mac-using-a-usb-serial-adapter/
// Used by UART 0 to connect to outside world for debug port

#define RX_PIN_NUMBER  26 // Actually not used/wired at all! Before it was set to 30 (now used for testing)  // Pin 	-> FTDI Red
#define RX_PIN_4_DBG   30
#define TX_PIN_NUMBER  31									// Pin  -> FTDI Yellow
#define CTS_PIN_NUMBER 0
#define RTS_PIN_NUMBER 0
#define HWFC           false

#define LED_RED    					LED_1
#define LED_GREEN  					LED_2

#define SEMTECH_SX9300_ADDR       	0x2B          // We are 0x28 on our board.. 
#define SEMTECH_POWER               26
#define SEMTECH_TXEN                25
#define SEMTECH_SVDD                24
#define SEMTECH_NRST                23

#define SEMTECH_I2C_CONFIG_SDA      20
#define SEMTECH_I2C_CONFIG_SCL      21
#define SEMTECH_INTERRUPT           22

#define ADXL_SCK_PIN                3
#define ADXL_MOSI_PIN               4
#define ADXL_MISO_PIN               5
#define ADXL_CS_PIN                 12
#define ADXL_INT_PIN                14
#define ADXL_IRQ_PRIORITY           APP_IRQ_PRIORITY_LOW


//#define DCDCEN



// Low frequency clock source to be used by the SoftDevice
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,            \
.rc_ctiv       = 0,                                \
.rc_temp_ctiv  = 0,                                \
.xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}


#endif
