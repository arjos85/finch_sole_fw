/* 
 * Lifesole Application
 * --------------------
 *
 * Author: Chris Moore
 * 
 * LifeSole main flow via a state machine
 *
 * Other parts of the systems interact with this by trying to set events. They do not change the states. Only the state machine
 *  processor here can shift states depending on the state transition table.
 *
 *
 *
 * 
 * (c) Roam Creative Ltd 2017
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "droplet_shoe.h"
#include "logger.h"
#include "nrf_drv_gpiote.h"
#include "droplet.h"
#include "nrf_delay.h"
#include "softdevice_handler.h"
#include "nrf52_bitfields.h"
#include "app_scheduler.h"
#include "app_timer.h"
#include "sx9300.h"
#include "ADXL362.h"
#include "lifesole.h"
#include "logger.h"
#include "nrf_drv_saadc.h"


/*****************************************************************************************************************
 *
 *                                    Local Function Forward Definition
 *
 *****************************************************************************************************************/

static void lifesole_stop_timer(void);
static void lifesole_set_timer(uint32_t seconds);
static void lifesole_set_sleep(void);
static void lifesole_clear_sleep(void);
static int8_t lifesole_get_state(void);
static int8_t lifesole_get_event(void);
//static void lifesole_set_event(int8_t event);
static void lifesole_set_event(void * event, uint16_t data_size);

#ifdef CONCURRENT_EVENTS_SUPPORT
// TODO: Functions used for handling concurrent events
static uint16_t lifesole_get_state_event_mask(lifesole_state_t state_id);
static void lifesole_init_event_masks(void);
#endif

/* State transitions functions */
void ls_invalid(void);
void ls_first_time_use(void);
void ls_waiting_motion(void);
void ls_motion_detected(void);
void ls_motion_delay_check(void);
void ls_body_detected(void);
void ls_waiting_no_motion(void);
void ls_no_motion_detected(void);
void ls_alert(void);
void ls_alert_finished(void);
void ls_enter_configuration(void);
void ls_exit_configuration(void);


/*****************************************************************************************************************
 *
 *                                    External Functions
 *
 *****************************************************************************************************************/

extern void ble_stack_init(void);
extern void nrf52_pheriperals_check(void);


/*****************************************************************************************************************
 *
 *                                             Static Variables
 *
 *****************************************************************************************************************/

/*
 *  Used in main loop for detecting wether to go into System Off or System On low power mode
 */
static uint8_t lifesole_should_sleep = false;


#ifdef DROPLET_USE_UART_LOGGING

/*
 * State name strings
 */

static const char *state_names[] = {

    "LS_STATE_INVALID", 
    "LS_STATE_FIRST_TIME_USE", 
    "LS_STATE_WAITING_MOTION", 
    "LS_STATE_MOTION_DETECTED",
    "LS_STATE_MOTION_DELAY_CHECK",
    "LS_STATE_BODY_DETECTED",
    "LS_STATE_WAITING_NO_MOTION",
    "LS_STATE_NO_MOTION_DETECTED",
    "LS_STATE_ALERT",
    "LS_STATE_ALERT_FINISHED",
    "LS_STATE_ENTER_CONFIGURATION",
    "LS_STATE_EXIT_CONFIGURATION",

};


/*
 * Event name strings
 */

static const char *event_names[] = {

    "LS_EVENT_NO_EVENT", 
    "LS_EVENT_FACTORY_RESTART", 
    "LS_EVENT_WATCHDOG_RESTART", 
    "LS_EVENT_BROWNOUT_RESTART", 
    "LS_EVENT_UPGRADE_RESTART", 
    "LS_EVENT_TIMER_EXPIRED", 
    "LS_EVENT_MOTION_CONFIGURE", 
    "LS_EVENT_MOTION_TRIGGERED", 
    "LS_EVENT_NO_MOTION_TRIGGERED",
    "LS_EVENT_BODY_DETECTED",
    "LS_EVENT_NO_BODY_DETECTED",
    "LS_EVENT_ENTER_CONFIG_MODE",
    "LS_EVENT_EXIT_CONFIG_MODE",
    "LS_EVENT_ALERT_FIRED",

};

#endif

/*
 * State function handlers. It ** MUST ** match the order of the enums for lifesole_state_t
 */

lifesole_handler_t lifesole_handlers[LS_STATE_MAX] = {
    
    ls_invalid,                         /* LS_STATE_INVALID */
    ls_first_time_use,                  /* LS_STATE_FIRST_TIME_USE */
    ls_waiting_motion,                  /* LS_STATE_WAITING_MOTION */
    ls_motion_detected,                 /* LS_STATE_MOTION_DETECTED */
    ls_motion_delay_check,              /* LS_STATE_MOTION_DELAY_CHECK */
    ls_body_detected,                   /* LS_STATE_BODY_DETECTED */
    ls_waiting_no_motion,               /* LS_STATE_WAITING_NO_MOTION */
    ls_no_motion_detected,              /* LS_STATE_NO_MOTION_DETECTED */
    ls_alert,                           /* LS_STATE_ALERT */
    ls_alert_finished,                  /* LS_STATE_ALERT */
    ls_enter_configuration,             /* LS_STATE_ENTER_CONFIGURATION */
    ls_exit_configuration,              /* LS_STATE_EXIT_CONFIGURATION */

};

/*
 * An entry for each state, the event and the new state enum
 */

lifesole_transition_t lifesole_transition[LS_STATE_MAX][LS_EVENT_MAX] = {

    { /* Current State : INVALID */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_INVALID},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

  
    { /* Current State : FIRST_TIME_USE */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 1 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_WAITING_MOTION},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_WAITING_MOTION */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_MOTION_DETECTED},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_WAITING_MOTION},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_WAITING_MOTION},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_MOTION_DETECTED */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 1 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_MOTION_DELAY_CHECK},
     { 2 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_ENTER_CONFIGURATION},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_MOTION_DETECTED},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_MOTION_DELAY_CHECK */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 1 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_ENTER_CONFIGURATION},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_WAITING_MOTION},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_BODY_DETECTED */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_NO_MOTION_DETECTED},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_WAITING_NO_MOTION */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_NO_MOTION_DETECTED */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 2 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_ALERT},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_WAITING_MOTION},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 1 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_ALERT},
    },

    { /* Current State :  LS_STATE_ALERT */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 2 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_ALERT_FINISHED},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},           //LS_STATE_WAITING_MOTION},     
     { 1 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_ALERT_FINISHED},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_ALERT_FINISHED},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State : LS_START_ALERT_FINISHED */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_ENTER_CONFIGURATION},   //LS_STATE_BODY_DETECTED}
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_WAITING_MOTION},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_INVALID},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_ENTER_CONFIGURATION */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_EXIT_CONFIGURATION},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_ENTER_CONFIGURATION},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_EXIT_CONFIGURATION},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

    { /* Current State :  LS_STATE_EXIT_CONFIGURATION */
     { 0 /*LS_EVENT_NO_EVENT*/,            LS_STATE_INVALID},
     { 0 /*LS_EVENT_FACTORY_RESTART*/,     LS_STATE_FIRST_TIME_USE},
     { 0 /*LS_EVENT_WATCHDOG_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_BROWNOUT_RESTART*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_UPGRADE_RESTART*/,     LS_STATE_INVALID},

     { 0 /*LS_EVENT_TIMER_EXPIRED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_MOTION_CONFIGURE*/,    LS_STATE_INVALID},     
     { 0 /*LS_EVENT_MOTION_TRIGGERED*/,    LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_MOTION_TRIGGERED*/, LS_STATE_INVALID},
     { 0 /*LS_EVENT_BODY_DETECTED*/,       LS_STATE_INVALID},
     { 0 /*LS_EVENT_NO_BODY_DETECTED*/,    LS_STATE_INVALID},
     
     { 0 /*LS_EVENT_ENTER_CONFIG_MODE*/,   LS_STATE_INVALID},
     { 0 /*LS_EVENT_EXIT_CONFIG_MODE*/,    LS_STATE_BODY_DETECTED},

     { 0 /*LS_EVENT_ALERT_FIRED*/,         LS_STATE_INVALID},
    },

};


#ifdef CONCURRENT_EVENTS_SUPPORT  

/*
 *  Event-masks used for managing multiple events
 */
uint16_t lifesole_states_event_mask[LS_STATE_MAX];

#endif


/*
 * Our single instance of this state machine. Don't access this directly. Use the setter/getters
 */

lifesole_instance_t lifesole;


/*
 * Timer used by the state machine
 */

APP_TIMER_DEF(m_lifesole_timer_id);


/*****************************************************************************************************************
 *
 *                                             Extern Functions
 *
 *****************************************************************************************************************/

extern void advertising_start(uint8_t alerting, uint8_t timeout);
extern void advertising_stop(void);
extern void prepare_advertising(void);
extern void battery_level_check(void);


/*****************************************************************************************************************
 *
 *                                                State Handling Functions
 *
 *****************************************************************************************************************/


/*
 * State handler : Factory reset or reset due to power outage or watchdog
 */

void ls_first_time_use(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    RMLOG ("Initialising all device drivers");

    /* Power on the motion sensor and initialise it */
    ADXL362_Start();

    /* Ensure SX9300 is powered off */
    sx9300_power_off();

    /* Motion sensor has been configured, transition to waiting for motion */
    lifesole_set_event_scheduler(LS_EVENT_MOTION_CONFIGURE);

}


/*
 * State handler : Configure it to wait for movement
 */

void ls_waiting_motion(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);


    /*
     * In order to know if the SX9300 can be powered off, check wether last event was a proximity one.
     * Otherwise perform a proximity check in case body had been previously detected.
     */

    switch (lifesole_get_event())
    {
        case LS_EVENT_BODY_DETECTED:

            /* If body was detected, we have to keep the SX9300 powered */
            sx9300_sleep();
                        
            /*
             * Check for motion detection in case we missed something in previous states
             */
            adxl_sw_irq_emulator();

            break;


        case LS_EVENT_NO_BODY_DETECTED:

            /* We ensure SX9300 is completely powered off */
            sx9300_power_off();
                        
            /*
             * Check for motion detection in case we missed something in previous states
             */
            adxl_sw_irq_emulator();

            break;
        

        default:

            if (lifesole_get_proximity_gpregret())
            {
                /* Body had been previously detected */
                RMLOG("SX9300: Body had been detected previously. Let's double check!");

                /* Activate the sensor and enable Compensation and Convertion Done interrupts */
                sx9300_wake();
                sx9300_enable_conv_done_irq();

                /* Let's skip lifesole_set_sleep() in order to allow the proximity measurement to take place */
                return;
            }
            else
            {
                /* No proximity event have brought us here and Body had not been detected previously  */
                RMLOG("SX9300: Body had NOT been detected previously. Powering-off the sensor");

                /* We ensure SX9300 is completely powered off */
                sx9300_power_off();
                
            }
    }


    /*
     * Case: We ended up being in motion while we configured waiting for motion, this would fire the interrupt before the sleep, it would then
     *  defer it to the scheduler, the scheduler would never run until later (when we wake up from some other interrupt)
     *
     * What we should do is schedule the lifesole_sleep function, so that if an event is fired ahead of it or we process until the event queue is empty..
     *
     * INFO: The motion sensor sets one bit in the GPIO SENSE register when it fires a trigger. Once we go into System OFF, we will come straight out of
     * it because the SENSE reg has already been set.
     */

    lifesole_set_sleep();

}

/*
 * State handler : Check if there is a body detected in the shoe. 
 */

void ls_motion_detected(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state transitions */
    lifesole_clear_sleep();

    /* Let's re-enforce the Inactivity time: Inactivity could have been disabled or Inactivity time modified in one of the previous states */
    ADXL362_SetupInactivityDetection(1, ADXL362_INACTIVITY_THRESHOLD , ((12.5 * LIFESOLE_INACTIVITY_SECONDS)/2));
    
    /* We wamt to initialize the SX9300 only at first run of ls_motion_detected() */
    if (lifesole_get_event() == LS_EVENT_MOTION_TRIGGERED) 
    {
        /* Power on and configure the proximity sensor with default parameters (@30ms) */
        sx9300_init();
        /* Enforcing fastscan in case we come from another state which has enabled slowscan */
        sx9300_enable_fastscan();
        /* Let's enforce that we want to wake the sensor in case SX9300 was kept powered but asleep */
        sx9300_wake();
        sx9300_enable_conv_done_irq();
    }

    /* In case no body has been detected, let's switch indo Doze mode and go into System Off  */
    if (lifesole_get_event() == LS_EVENT_NO_BODY_DETECTED) 
    {
        /*
         * No body there, configure the proxmity to go into snooze scan and interrupt mode. Interrupt will fire LS_EVENT_BODY_DETECTED's '
         */
         sx9300_start_slowscan();

        /* 
         * Motion sensor is already setup to trigger after LIFESOLE_INACTIVITY_SECONDS so we just go to sleep and wait now for either event
         */
#ifdef SX9300_FPC_TEST
        /* In case FPC is under test, let's keep LifeSole in System On and get some LOG messages about the reading from SX9300 */
        sx9300_enable_conv_done_irq();
        nrf52_pheriperals_check();

#else 
        /* If we are not testing the FPC, we can go into System Off */
        lifesole_set_sleep();

#endif

    }

}


/*
 * State handler : We came here when we were waiting for either a proximity event or inactivity timeout. Basically, after the device is picked up we are 
 *                  trying to detect if going back to sleep or someone inserting their foot.
 */

void ls_motion_delay_check(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state switch */
    lifesole_clear_sleep();

    /*
     * If a proximity measurement in not already in progres, let's start one.
     */
    if(sx9300_measurement_in_progress())
    {
        /*
         * A proximity measurement has already started after ls_motion_detected() and while in System Off.
         * Let's wait for the trigger from the prox sensor.
         */
        RMLOG("Waiting for SX9300 to trigger an event based on its measurement already in progress");
    }
    else
    {
        RMLOG("The SX9300 has not started a measurement yet, let's force an instantaneous proximity check");

        /*! Let's trigger a reading of the sensor's registers now, otherwise in the worst case we have to wait for another DOZEPERIOD */
        sx9300_sw_irq_emulator();

        /* Stop slow proximity scan */
        sx9300_stop_slowscan();
        
        /* Configure the proximity sensor for an instantaneous measurement */
        sx9300_wake();
    }

}


/*
 * State handler : body had been detected before entering configuration state. For reducing the usage of SX9300 (power-saving),
 * body is assumed to be still there (it will be checked again once INACTIVITY will be detected).
 */

void ls_body_detected(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    /*
     * Let's check if we have "ignored" a change in motion, INACTIVITY might have been detected in a previous state, but discarded as an invalid event
     */     

    adxl_sw_irq_emulator();


    /*
     * The next phase is setting motion mode for a longer inactivty time out
     */

    RMLOG("Reconfiguring motion timeouts for inactivity to %u seconds", LIFESOLE_SAFETY_INACTIVITY_SECONDS);

    ADXL362_SetupInactivityDetection(1, ADXL362_INACTIVITY_THRESHOLD , ((12.5 * LIFESOLE_SAFETY_INACTIVITY_SECONDS)/2));


    /*
     * Triggering a self-test procedure in order to get the INACTIVITY detection "unstuck"
     */
    
    ADXL362_SelfTest();


    /*
     * Motion sensor is already setup to trigger after LIFESOLE_INACTIVITY_SECONDS so we just go to sleep and wait now for a motion event
     */
    RMLOG ("Waiting for inactivty now that the foot is in there");

    lifesole_set_sleep();

}


/*
 * State handler :: NOT USED
 */

void ls_waiting_no_motion(void)
{
    RMLOG("\nNOT USED :: We entered %s", __FUNCTION__);
}


/*
 * State handler :: We come here after foot is inserted and then there is no motion detected for xx seconds. We will check if the foot is there.
 */

void ls_no_motion_detected(void)
{

    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state switch */
    lifesole_clear_sleep();


    /*
     * Check if foot is there, if it is we need to either count the number of times (difficult with resets 0'ing all variables!) or just use the programmed value
     * in the inactivity so we're not waking up all the time.
     */

    /* Configure the proximity sensor for an instantaneous measurement */
    sx9300_wake();
    sx9300_enable_conv_done_irq();
    /* Enforcing fastscan in case we come from another state which has enabled slowscan */
    sx9300_enable_fastscan();

}


/*
 * State handler :: In alert state we want to enable the Bluetooth advertising for a period of time, we need to keep the chip awake
 *                   and then afterwards (say 20 seconds of advertising to make sure it went through)
 */

void ls_alert(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state switch */
    lifesole_clear_sleep();


    /* Disable SX9300 and put it into lowest power mode (body detected) */
    sx9300_sleep();

    /* Triggers a battery measurement. Battery level will be advertised via BLE */
    battery_level_check();

    /*
     * Enable the BLE stack and the advertising event
     */

    RMLOG ("Trigger BLE advertising of alert");

    /* Init BLE stack */
    ble_stack_init();

    /* Setup BLE advertising */
    prepare_advertising();

    /* Start BLE advertising */
    advertising_start(true, LIFESOLE_ALERT_SECONDS);


    /*
     * Setup a timer that fires an event so we transition to ls_alert_finished() and handle the tidy up and resumption of sleep
     * by firing the LS_EVENT_TIMER_EXPIRED which it'll do.
     */

    lifesole_set_timer(LIFESOLE_ALERT_SECONDS);

}


/*
 * State handler :: Alert finished due to timeout (or could possibly be acknowledgement)
 */

void ls_alert_finished(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state switch */
    lifesole_clear_sleep();

    /* Stop the BLE advertising */
    advertising_stop();

    /* Stop timer as could have come here from motion */
    lifesole_stop_timer();

    // Let's check if we have "ignored" a change in motion, INACTIVITY might have been detected previously
    adxl_sw_irq_emulator();
    
}


/*
 * State handler :: Open the device up for configuration and advertise so we can show a 'foot detected' event
 */

void ls_enter_configuration(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    if ((lifesole_get_event() == LS_EVENT_BODY_DETECTED) ||     // From Motion Detected or Motion Delay Check
        (lifesole_get_event() == LS_EVENT_MOTION_TRIGGERED) )   // From Alert State
    {
        /* To avoid to go to sleep in case of multiple-events and multiple state switch */
        lifesole_clear_sleep();

        /* Triggers a battery measurement. Battery level will be advertised via BLE */
        battery_level_check();

        /*
        * Enable the BLE stack and the advertising event
        */

        RMLOG ("Trigger BLE advertising for starting a configuration process");

        /* Init BLE stack */
        ble_stack_init();
        
        /* Setup BLE advertising */
        prepare_advertising();

        /* Start BLE advertising */
        advertising_start(false, LIFESOLE_CONFIG_SECONDS);
        

        /*
        * Setup a timer that fires an event so we transition to ls_exit_configuration() by firing the LS_EVENT_TIMER_EXPIRED which it'll do.
        */
        lifesole_set_timer(LIFESOLE_CONFIG_SECONDS);
    }

    
    if (lifesole_get_event() == LS_EVENT_ENTER_CONFIG_MODE) 
    {
        /* Stop timer */
        lifesole_stop_timer();
        /* Start timer with enough time to allow completion of user configuratio */
        lifesole_set_timer(LIFESOLE_CONFIG_SESSION_SECONDS);
    }

}


/*
 * State handler :: Arrived here because we timed out.
 */

void ls_exit_configuration(void)
{
    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state switch */
    lifesole_clear_sleep();

    // WARNING: Need to check if a connected configuration session as stopping advertising might kill that connection...

    /* Stop the BLE advertising */
    advertising_stop();
    
    /* Stop timer (though its a single shot that got us here to technically shouldn't need to) */
    lifesole_stop_timer();
    

    // No need to check for proximity again and motion status.
    // Let's trigger a transition to LS_STATE_BODY_DETECTED state where all these checks will be performed
    lifesole_set_event_scheduler(LS_EVENT_EXIT_CONFIG_MODE);

}


/*
 * State handler : we enter in this state in case of an event that triggers an invalid state transition
 */

void ls_invalid(void)
{   
    RMLOG("\nWe entered %s", __FUNCTION__);

    /* To avoid to go to sleep in case of multiple-events and multiple state switch */
    lifesole_clear_sleep();

    /* Request transition into Sytem Off mode depending on the last valid state */
    switch(lifesole_get_state())
    {
        case LS_STATE_WAITING_MOTION:

        case LS_STATE_MOTION_DETECTED:

        case LS_STATE_BODY_DETECTED:

            lifesole_set_sleep();

            break;

    }

}



/*****************************************************************************************************************
 *
 *                                                    Functions
 *
 *****************************************************************************************************************/


/*
 * Retrieves the state from flash / saved area
 */

int8_t lifesole_get_state(void)
{
    uint8_t gpregret;

    gpregret = (NRF_POWER->GPREGRET);

    return ((gpregret & LIFESOLE_GPREGRET_STATE_MASK) >> LIFESOLE_GPREGRET_STATE_POS);
}


/*
 * Sets the state to flash / saved area
 */

void lifesole_set_state(int8_t state)
{
    uint32_t err_code;
    uint8_t new_gpregret;
    uint8_t old_gpregret = (NRF_POWER->GPREGRET);
#ifdef DROPLET_USE_UART_LOGGING
    int8_t prev_state = lifesole.state;
#endif // DROPLET_USE_UART_LOGGING


    // Update global state variable
    lifesole.state = state;


    /* Clear the State Machine state's bits */
    new_gpregret = old_gpregret & ~LIFESOLE_GPREGRET_STATE_MASK;
    /* ...and replace them with the new one */
    new_gpregret |= ((lifesole.state << LIFESOLE_GPREGRET_STATE_POS) & LIFESOLE_GPREGRET_STATE_MASK);

    RMLOG("\nState changing from %s (0x%02X) ==>> %s (0x%02X)",  state_names[prev_state], old_gpregret, state_names[state], new_gpregret);


    /* Clear and update GPREGRET */
    err_code = sd_power_gpregret_clr(POWER_GPREGRET_GPREGRET_Msk);
    APP_ERROR_CHECK(err_code);

    err_code = sd_power_gpregret_set(new_gpregret);
    APP_ERROR_CHECK(err_code);

}


/*
 * Sets the body proximity status to flash / saved area
 */

void lifesole_set_proximity_gpregret(bool prox)
{
    uint32_t err_code;
    uint8_t new_gpregret;


    RMLOG("Proximity being set to %s in GPREGRET register.", prox?"True":"false");

    /* Clear the proximity bit */
    new_gpregret = (NRF_POWER->GPREGRET) & ~LIFESOLE_GPREGRET_PROX_MASK;
    /* ...and update it */
    new_gpregret |= (prox? 1<<LIFESOLE_GPREGRET_PROX_POS : 0);

    /* Clear and update GPREGRET */
    err_code = sd_power_gpregret_clr(POWER_GPREGRET_GPREGRET_Msk);
    APP_ERROR_CHECK(err_code);

    err_code = sd_power_gpregret_set(new_gpregret);
    APP_ERROR_CHECK(err_code);

}


/*
 * Gets the body proximity status from flash / saved area
 */

bool lifesole_get_proximity_gpregret(void)
{
    /* Check if SX9300 power status and proximity flag match */
    if (!sx9300_get_power_status())
    {
        lifesole_set_proximity_gpregret(false);
        return false;
    } 
    else {
        return (bool) ((NRF_POWER->GPREGRET) & LIFESOLE_GPREGRET_PROX_MASK);
    }
}


/*
 * Retrieves the event from the lifesole global variable
 */

int8_t lifesole_get_event(void)
{
    return (lifesole.next_event);
}


/*
 * Sets the event to flash / saved area
 */

void lifesole_set_event_scheduler(int8_t event)
{
    
    uint32_t err_code;

    RMLOG("Scheduling event: %s",  event_names[event]);
    err_code = app_sched_event_put((void *) &event, sizeof(int8_t), (app_sched_event_handler_t) lifesole_set_event);
    
    if (err_code != NRF_ERROR_NO_MEM)
    {
        APP_ERROR_CHECK(err_code);
    }

#ifdef RX_PIN_4_DBG

    nrf_gpio_cfg_output(RX_PIN_4_DBG);

    if (event == LS_EVENT_BODY_DETECTED)
    {
    	nrf_gpio_pin_set(RX_PIN_4_DBG);
        RMLOG("nrf_gpio_pin_set(RX_PIN_4_DBG)");
    }

    if (event == LS_EVENT_NO_BODY_DETECTED)
    {
    	nrf_gpio_pin_clear(RX_PIN_4_DBG);
        RMLOG("nrf_gpio_pin_clear(RX_PIN_4_DBG)");
    }

#endif

 }


/*
 * Sets the event to flash / saved area
 */

static void lifesole_set_event(void * event, uint16_t data_size)
{

    /* Update our event */
    lifesole.next_event = *((int8_t *) event);

    /* Only print if we're firing a valid event */
    if (lifesole.next_event != LS_EVENT_NO_EVENT) {
        RMLOG("Fire event: %s",  event_names[lifesole.next_event]);
        lifesole_process();
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t) lifesole_process);
    }

}


/*
 * Sets ready for sleep mode
 */

static void lifesole_set_sleep(void) 
{
    lifesole_should_sleep = true;
    RMLOG("Setting request to SLEEP\n");
}


/*
 * Reset sleep mode request
 */

static void lifesole_clear_sleep(void)
{
    lifesole_should_sleep = false;
    RMLOG("Clearing previous request to SLEEP if any");
}


/*
 * Gets the sleep mode flag
 */

uint8_t lifesole_get_sleep(void) 
{
    return(lifesole_should_sleep);
}


/*
 * Put the device into sleep state (System Off)
 */

void lifesole_sleep(void)
{

    if (sx9300_get_power_status())
    {
        /* Power off the Sx9300 digital interface before System Off -> Power consumption reduction */
        twi_master_uninit();    // Gives a power saving of about 10-13uA when in System Off
    }
    
#if defined SX9300_TEST_MODE || defined SX9300_FPC_TEST
    nrf52_pheriperals_check();
#endif

    RMLOG("\nEntering Sleep\n\n==================== nRF52832 in SYSTEM OFF (Lowest Power Mode) ====================\n\n");

    /* Clear latch */
    NRF_P0->LATCH = 0xFFFFFFFF;

    /* Power off (will wake from interrupts) */
    sd_power_system_off();
    
}


/*
 * Handles restoring driver states
 */

static void lifesole_restore_drivers(void)
{
    RMLOG ("Restoring drivers interrupt handlers and SPI/TWI interfaces");
    
    /*
     * If after System Off the proximity sensor is already powered, then let's do a partial initialisation.
     * Otherwise let's keep it powered off.
     */
    if (sx9300_get_power_status())
    {
        sx9300_init();
    }

    /* Motion sensor is supposed to be always powered */
    ADXL362_Init();

}


/*
 * Handles restoring driver states, latch register will be cleared before we sleep
 */

static void lifesole_latched_trigger(void)
{
    uint32_t latch;

    // Load it
    latch = NRF_P0->LATCH;
    
    RMLOG ("Using the latch (0x%08X), determine what fired the wakeup", latch);

    /* Case if SX9300 has triggered an interrupt */
    if (latch & (1 << SEMTECH_INTERRUPT))
    {
        RMLOG("Was SX9300");

        /* Clear the relevant bit */
        latch &= ~(1 << SEMTECH_INTERRUPT);
        
        /* Schedule a spin of the relevant interrupt handler */
        sx9300_sw_irq_emulator();
    }
    
    /* Case if ADXL362 has triggered an interrupt */
    if (latch & (1 << ADXL_INT_PIN))
    {
        RMLOG("Was ADXL362");

        /* Clear the relevant bit */
        latch &= ~(1 << ADXL_INT_PIN);
        
        /* Schedule a spin of the relevant interrupt handler */
        adxl_sw_irq_emulator();
    }

    /* If latch variable still contains bit set for interrupts from other GPIOs */
    if (latch)
    {
        RMLOG("Unexpected event(s): LATCH Reg = 0x%02X", latch);
    }
    
    // Clear latch register
    NRF_P0->LATCH = 0xFFFFFFFF;

}


/*
 * Handler for timer
 */

static void timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
    lifesole_set_event_scheduler(LS_EVENT_TIMER_EXPIRED);
}


/*
 * Create timer
 */

void lifesole_init_timer(void)
{

    uint32_t err_code;

    RMLOG("Creating timer for general usage");
    
    err_code = app_timer_create(&m_lifesole_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                timeout_handler);
    APP_ERROR_CHECK(err_code);

}


/*
 * Set a timer for a period of seconds
 */

static void lifesole_set_timer(uint32_t seconds)
{
    RMLOG("Starting single shot timer for %u seconds\n\n",seconds);
    app_timer_start(m_lifesole_timer_id, APP_TIMER_TICKS((seconds * 1000), APP_TIMER_PRESCALER), NULL);
}


/*
 * Stops a timer
 */

static void lifesole_stop_timer(void)
{
    RMLOG("Stopping timer");
    app_timer_stop(m_lifesole_timer_id);
}


/*
 * Handles the lifesole state machine
 */

void lifesole_process(void)
{
    uint8_t current_state;
    uint8_t next_event;

    /*
     * Here we check if any event has been fired. If so we process the event by evaluating the current state, the event and what state
     * we should be tranisitioning to.
     *
     * We check if there is an invalid state transition and report that as a problem as well and then remain in our current state.
     *
     * Note we could make this detection of invalid state trigger restart etc 
     */

     current_state = lifesole_get_state();
     next_event    = lifesole_get_event();
     
     /* Verify it's a valid state & theres an event to process */
     if (current_state != LS_STATE_INVALID && current_state < LS_STATE_MAX)
     {
        
        /* Let's make sure there is a valid event to process */
        if (next_event != LS_EVENT_NO_EVENT && next_event < LS_EVENT_MAX)
        {

            /* Update that we have no event next time (unless state triggers one) */
            //lifesole_set_event_scheduler(LS_EVENT_NO_EVENT);

            /* Check if its an allowed state transition/event */
            if (lifesole_transition[current_state][next_event].next_state != LS_STATE_INVALID)
            {

                /* Call the state transition handler */
                lifesole_set_state(lifesole_transition[current_state][next_event].next_state);
                lifesole_handlers[lifesole_transition[current_state][next_event].next_state]();

            }
            else
            {
                RMLOG ("Invalid state event transitions from %s with event %s!", state_names[current_state], event_names[next_event]);

                /* Let's keep the device in the correct low power mode depending on actual valid state */
                ls_invalid();
            }

        }
        else
        {
            /* No event to process */
        }

     }
     else
     {
         RMLOG("Invalid state (%u) -- SHOULD RESET TO FACTORY DEFAULTS!", current_state);
         lifesole_set_event_scheduler(LS_EVENT_FACTORY_RESTART);
     }

}


/*
 * Inits the lifesole systems
 */

void lifesole_init(uint32_t reset_reason)
{
    uint8_t state;
    
    /* Init so we have a timer module */
    lifesole_init_timer();

#ifdef CONCURRENT_EVENTS_SUPPORT
    
    /* Init the transition event-masks array */
    lifesole_init_event_masks();

#endif

    /* Retrieve the state from GPREGRET */
    state = lifesole_get_state();
    
    /* Restore state if we're in an invalid state or we reset for any reason *OTHER THAN* a proper wakeup due to GPIO */
    if ((state == LS_STATE_INVALID) || (reset_reason != POWER_RESETREAS_OFF_Msk))
    {

        RMLOG ("Setting to factory reset state (GPREGRET = 0x%02X)", NRF_POWER->GPREGRET);
        lifesole_set_proximity_gpregret(false);
        lifesole_set_state(LS_STATE_FIRST_TIME_USE);
        lifesole_set_event_scheduler(LS_EVENT_FACTORY_RESTART);

    }
    else
    {
        
        RMLOG ("Restoring preserved state (%u) with GPIO wakeup %s", state, (reset_reason & POWER_RESETREAS_OFF_Msk) ? "fired" : "not fired");
        lifesole.state = state;
        lifesole_set_state(state);


        /*
         * Restore sensor to proper configuration
         */

        lifesole_restore_drivers();


        /*
         * Check for what sensor has triggered and execute proper irq handlers
         */

        lifesole_latched_trigger();

    }

}


#ifdef CONCURRENT_EVENTS_SUPPORT

/*
 * Initialise relevant-event mask for each state
 */

void lifesole_init_event_masks(void)
{
    uint16_t event_mask = 0;

    for (lifesole_state_t state_id = LS_STATE_INVALID; state_id < LS_STATE_MAX; state_id++)
    {
        event_mask = 0;

        for (lifesole_event_t event_id = LS_EVENT_NO_EVENT; event_id < LS_EVENT_MAX; event_id++)
        {
            if (lifesole_transition[state_id][event_id].next_state != LS_STATE_INVALID)
            {
                event_mask |= (1 << event_id);
            }
        }

        lifesole_states_event_mask[state_id] = event_mask;
    }

    lifesole_get_state_event_mask(lifesole_get_state());
}


/*
 * Return the relevant-event mask for the required state
 */

uint16_t lifesole_get_state_event_mask(lifesole_state_t state_id)
{
    if ((state_id > LS_STATE_INVALID)  && (state_id < LS_STATE_MAX))
    {
        return lifesole_states_event_mask[state_id];
    }
    else
    {
        return 0;
    }
}


/*
 * Add event for handling multiple concurrent events 
 */

void lifesole_add_event(int8_t event)
{
    
}

#endif // CONCURRENT_EVENTS_SUPPORT
