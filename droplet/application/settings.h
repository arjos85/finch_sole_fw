/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * This is the firmware for Roam Creative Ltd iBeacon implementation with various enhancements
 *  to aid battery life and detection.
 *
 * (c) Roam Creative Ltd 2014
 *
 */

#ifndef SETTINGS_H__
#define SETTINGS_H__

// --------------------------- INCLUDES ----------------------------------

#include <stdbool.h>
#include <stdint.h>
#include "app_util.h"
#include "app_timer.h"
#include "ble_bas.h"

// --------------------------- Functions ----------------------------------

bool settings_init(void);
void load_flash_to_ram(void);
void settings_erase(void);
void *settings_address(void);

void save_to_storage(uint8_t module, void *data, uint16_t length, uint8_t write);

uint32_t pstorage_access_wait(void);
void wait_for_flash_and_reset(void);

void set_device_name(uint8_t *name, uint8_t length);
uint8_t *get_device_name(void);


// --------------------------- CONSTANTS ----------------------------------


// --------------------------- ENUM TYPES ----------------------------------

typedef enum
{
    SETTINGS_MODULE_NONE    = 0,
    SETTINGS_MODULE_RMCS
} settings_module_t;



// ------------------------------ STRUCTS ----------------------------------



#endif // SETTINGS_H__
