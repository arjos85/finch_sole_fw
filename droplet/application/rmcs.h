/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * This is the firmware for Roam Creative Ltd iBeacon implementation with various enhancements
 *  to aid battery life and detection.
 *
 * (c) Roam Creative Ltd 2014
 *
 */

#ifndef RMCS_H__
#define RMCS_H__

// --------------------------- INCLUDES ----------------------------------

#include <stdbool.h>
#include <stdint.h>
#include "app_util.h"
#include "app_timer.h"
#include "ble_bas.h"

// --------------------------- Functions ----------------------------------

void registerRMCS(void);
void resetRMCSConfig(void);
void restoreRMCSConfig(void *saved, uint16_t length);

void update_counter1(uint32_t value);
void update_counter2(uint32_t value);

uint8_t config_is_locked(void);
uint8_t config_is_changed(void);
uint8_t config_attempt_change(void);
void config_now_changed(void);

void security_connection(void);
void security_disconnection(void);
void security_violation(void);

void erase_and_restart(void);

uint8_t device_is_locked(void);

// --------------------------- CONSTANTS ----------------------------------


// --------------------------- ENUM TYPES ----------------------------------

typedef enum
{
    SECURITY_TMR_IDLE       = 0,
    SECURITY_TMR_KICKOFF    = 1,
    SECURITY_TMR_PROTECT    = 2
} security_timer_t;


typedef enum
{
    SECURITY_CTRL_IDLE      = 0,
    SECURITY_CTRL_PASSWORD  = 1,
    SECURITY_CTRL_IDENTIFY  = 2,
    SECURITY_CTRL_CHALLENGE = 3,
    
    SECURITY_CTRL_RESET     = 10,
    
} security_ctrl_t;

typedef enum
{
    SECURITY_MODE_NO_PASSWORD = 0,
    SECURITY_MODE_PASSWORD    = 1,
    SECURITY_MODE_CHALLENGE   = 2

} security_mode_t;

typedef enum
{
    DEVICE_CTRL_DFU           = (1 << 15),
    DEVICE_CTRL_ERASE         = (1 << 14),
    
 
    DEVICE_CTRL_FLASH_LED     = (1 << 12),
    DEVICE_CTRL_SHORT_BUZZ    = (1 << 11),
    DEVICE_CTRL_LONG_BUZZ     = (1 << 10),
    DEVICE_CTRL_BEEPBEEP_BUZZ = (1 << 9),
    
    DEVICE_CTRL_LONG_VIBE     = (1 << 2),
    DEVICE_CTRL_SHORT_VIBE    = (1 << 1),

    DEVICE_CTRL_RESTART 	  = (1 << 0)
    
} device_ctrl_t;



// ------------------------------ STRUCTS ----------------------------------



#endif // RMCS_H__
