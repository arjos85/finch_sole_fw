/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * Roam Configuration Service
 *      - Security Modes
 *      - Counters
 *
 * (c) Roam Creative Ltd 2014
 *
 */

#include "ble_roam.h"
#include <string.h>
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"
#include "droplet.h"
#include "rmcs.h"



/**@brief Connect event handler.
 *
 * @param[in]   p_rmcs      Roam Configuration Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_rmcs_t * p_rmcs, ble_evt_t * p_ble_evt)
{
    p_rmcs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Disconnect event handler.
 *
 * @param[in]   p_rmcs      Roam Configuration Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_rmcs_t * p_rmcs, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_rmcs->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Write event handler.
 *
 * @param[in]   p_rmcs      Roam Configuration Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_rmcs_t * p_rmcs, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if (p_rmcs->beacon_write_handler == NULL) {
        return;
    }
    
    // Check with RMCS policy (if its locked, only allow writing to the specific chars)
    
	if ((p_evt_write->handle == p_rmcs->rmcs_counter1_char_handles.value_handle) &&
       (p_evt_write->len == 4)) {
            p_rmcs->beacon_write_handler(p_rmcs, rmcs_counter1_char_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_counter2_char_handles.value_handle) &&
       (p_evt_write->len == 4)) {
            p_rmcs->beacon_write_handler(p_rmcs, rmcs_counter2_char_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_challenge_char_handles.value_handle) &&
        (p_evt_write->len == 20)) {
        p_rmcs->beacon_write_handler(p_rmcs, rmcs_challenge_data, p_evt_write->data);
    }

    if ((p_evt_write->handle == p_rmcs->rmcs_tx_level_char_handles.value_handle) &&
        (p_evt_write->len == 1)) {
        p_rmcs->beacon_write_handler(p_rmcs, rmcs_tx_level_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_pri_timeout_char_handles.value_handle) &&
        (p_evt_write->len == 2)) {
        p_rmcs->beacon_write_handler(p_rmcs, rmcs_pri_timeout_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_pri_rate_char_handles.value_handle) &&
        (p_evt_write->len == 2)) {
        p_rmcs->beacon_write_handler(p_rmcs, rmcs_pri_rate_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_sec_rate_char_handles.value_handle) &&
        (p_evt_write->len == 2)) {
        p_rmcs->beacon_write_handler(p_rmcs, rmcs_sec_rate_data, p_evt_write->data);
    }
    

    if ((p_evt_write->handle == p_rmcs->rmcs_devicectrl_char_handles.value_handle) &&
        (p_evt_write->len == 2)) {
        p_rmcs->beacon_write_handler(p_rmcs, rmcs_devicectrl_data, p_evt_write->data);
    }
    
    if ((p_evt_write->handle == p_rmcs->rmcs_securityctrl_char_handles.value_handle) &&
       (p_evt_write->len == 2)) {
            p_rmcs->beacon_write_handler(p_rmcs, rmcs_securityctrl_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_securitymode_char_handles.value_handle) &&
       (p_evt_write->len == 2)) {
            p_rmcs->beacon_write_handler(p_rmcs, rmcs_securitymode_data, p_evt_write->data);
    }
    if ((p_evt_write->handle == p_rmcs->rmcs_passcode_char_handles.value_handle) &&
       (p_evt_write->len == 20)) {
            p_rmcs->beacon_write_handler(p_rmcs, rmcs_passcode_data, p_evt_write->data);
    }


}

/*
 * Handler for processing BLE events for this particular service
 */

void ble_rmcs_on_ble_evt(ble_rmcs_t * p_rmcs, ble_evt_t * p_ble_evt)
{
    
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_rmcs, p_ble_evt);
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_rmcs, p_ble_evt);
            break;
            
        case BLE_GATTS_EVT_WRITE:
            on_write(p_rmcs, p_ble_evt);
            break;
            
        default:
            break;
    }
}


/**@brief Add Service characteristic.
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_counter1_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    

    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_COUNTER1_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 4;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 4;
    attr_char_value.p_value      = &p_rmcs_init->pData->counter1[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_counter1_char_handles);
}

/**@brief Add Service characteristic.
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_counter2_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    

    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_COUNTER2_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 4;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 4;
    attr_char_value.p_value      = &p_rmcs_init->pData->counter2[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_counter2_char_handles);
}


/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_tx_level_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_TX_LEVEL_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 1;
    attr_char_value.p_value      = &p_rmcs_init->pData->tx_level;
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_tx_level_char_handles);
}

/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_pri_timeout_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_PRI_TIMEOUT_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 2;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 2;
    attr_char_value.p_value      = &p_rmcs_init->pData->adv_primary_timeout[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_pri_timeout_char_handles);
}

/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_pri_rate_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_PRI_RATE_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 2;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 2;
    attr_char_value.p_value      = &p_rmcs_init->pData->adv_primary_rate[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_pri_rate_char_handles);
}

/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_sec_rate_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_SEC_RATE_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 2;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 2;
    attr_char_value.p_value      = &p_rmcs_init->pData->adv_secondary_rate[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_sec_rate_char_handles);
}

/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_devicectrl_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_DEVICECTRL_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 2;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 2;
    attr_char_value.p_value      = &p_rmcs_init->pData->devicectrl[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                           &attr_char_value,
                                           &p_rmcs->rmcs_devicectrl_char_handles);
}


/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_securityctrl_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    

    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_SECURITYCTRL_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 2;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 2;
    attr_char_value.p_value      = &p_rmcs_init->pData->securityctrl[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_rmcs->rmcs_securityctrl_char_handles);
}

/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_securitymode_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    

    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_SECURITYMODE_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 2;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 2;
    attr_char_value.p_value      = &p_rmcs_init->pData->securitymode[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_rmcs->rmcs_securitymode_char_handles);
}
#if 0 // Unsupported
/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_challenge_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    

    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_CHALLENGE_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 20;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 20;
    attr_char_value.p_value      = &p_rmcs_init->pData->challenge[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_rmcs->rmcs_challenge_char_handles);
}
#endif

/**@brief Add Roam Configuration characteristic
 *
 * @param[in]   p_rmcs       Roam Configuration Service structure.
 * @param[in]   p_rmcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rmcs_passcode_char_add(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_BEACON_PASSCODE_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    
    attr_md.vloc       = BLE_GATTS_VLOC_USER; //BLE_GATTS_VLOC_STACK;           // Roam Extension change to use shared memory
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 20;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = 20;
    attr_char_value.p_value      = &p_rmcs_init->pData->passcode[0];
    
    return sd_ble_gatts_characteristic_add(p_rmcs->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_rmcs->rmcs_passcode_char_handles);
}

/*
 * Initialise the Service
 */

uint32_t ble_rmcs_init(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init)
{
    uint32_t   err_code;
    ble_uuid_t ble_uuid;

    // Initialize service structure
    p_rmcs->conn_handle       	 = BLE_CONN_HANDLE_INVALID;
    p_rmcs->beacon_write_handler = p_rmcs_init->write_handler;
    
    // Add base UUID to softdevice's internal list. 
    ble_uuid128_t base_uuid = RMCS_UUID_BASE;
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_rmcs->uuid_type);
    if (err_code != NRF_SUCCESS) {
        return err_code;
    }
    
    ble_uuid.type = p_rmcs->uuid_type;
    ble_uuid.uuid = RMCS_UUID_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_rmcs->service_handle);
    if (err_code != NRF_SUCCESS) {
        return err_code;
    }
    
	/*
	 * Add all the Characteristics to the Service
	 */

    err_code = rmcs_counter1_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS) {
        return err_code;
    }

    err_code = rmcs_counter2_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS) {
        return err_code;
    }
#if 0 // Not supported yet
    err_code = rmcs_challenge_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
#endif

    err_code = rmcs_tx_level_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = rmcs_pri_timeout_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = rmcs_pri_rate_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = rmcs_sec_rate_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = rmcs_devicectrl_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = rmcs_securityctrl_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = rmcs_securitymode_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
        
    err_code = rmcs_passcode_char_add(p_rmcs, p_rmcs_init);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
    return NRF_SUCCESS;
}

