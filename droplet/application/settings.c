/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * This module is responsible for saving/restoring settings to and from flash. A bit tight on time so
 *  only some settings have been ported to this style yet.
 *
 * (c) Roam Creative Ltd 2014
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "ble_conn_params.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "pstorage.h"
#include "app_timer.h"
#include "app_button.h"
#include "app_timer.h"
#include "ble_roam.h"
#include "droplet.h"
#include "logger.h"
#include "simple_uart.h"
#include "settings.h"
#include "rmcs.h"
#include "app_scheduler.h"

#include "nrf_delay.h"



/*****************************************************************************************************************
 *
 *                                           External Data
 *
 *****************************************************************************************************************/


/*****************************************************************************************************************
 *
 *                                              Variables
 *
 *****************************************************************************************************************/

flash_db_t                  *p_flash_db;
flash_db_t                  ram_db;

static pstorage_handle_t    pstorage_block_id;

/*****************************************************************************************************************
 *
 *                                                Functions
 *
 *****************************************************************************************************************/

//------------------------------------------------------------------//
/**
 * @brief Function for handling pstorage events
 */
static void pstorage_ntf_cb(pstorage_handle_t *  p_handle,
                            uint8_t              op_code,
                            uint32_t             result,
                            uint8_t *            p_data,
                            uint32_t             data_len)
{
    APP_ERROR_CHECK(result);
}

/*
 * Waits for flash access/storage to finish
 */

uint32_t pstorage_access_wait(void)
{
    uint32_t count = 1;
    uint32_t err_code;
    
    do
    {    
        app_sched_execute(); 
        // Check if storage access is in progress.
        err_code = pstorage_access_status_get(&count);
        if (err_code != NRF_SUCCESS)
        {
            return err_code;
        }
        
    }
    while(count != 0);
    
    return NRF_SUCCESS;
}

void wait_for_flash_and_reset(void)
{
    uint32_t err_code;
    
    err_code = pstorage_access_wait();
    APP_ERROR_CHECK(err_code);
        
    NVIC_SystemReset();
    //sd_nvic_SystemReset();
}

void *settings_address(void)
{
#if 0 // On new nordic it can't even 'fake' write to flash
    return(&p_flash_db->data);
#else
    return(&ram_db);
#endif
}

/*
 * Clear it all our
 */

void settings_erase(void)
{
    uint32_t err_code;
    err_code = pstorage_clear(&pstorage_block_id, sizeof(flash_db_t));
    APP_ERROR_CHECK(err_code);

#if 0 // Problem for calls when ble connected..?
    err_code = pstorage_access_wait();
    APP_ERROR_CHECK(err_code);
#endif
}

/**
 * Saves to flash
 */

static void persist_to_storage(void)
{
    uint32_t err_code;
    
    // So on disconnect we get the device restarted
    config_now_changed();
    
    // Zap it down to flash
    err_code = pstorage_clear(&pstorage_block_id, sizeof(flash_db_t));
    APP_ERROR_CHECK(err_code);

    nrf_delay_ms(1);

    err_code = pstorage_store(&pstorage_block_id, (uint8_t *)&ram_db, sizeof(flash_db_t), 0);
    APP_ERROR_CHECK(err_code);

#if 1 // Problem for calls when ble connected..?
    err_code = pstorage_access_wait();
    APP_ERROR_CHECK(err_code);
#endif
}

/*
 * The size of the RMCS storage area in the flash area
 */

static uint16_t size_of_rmcs_storage(void)
{
    return ((uint32_t)&ram_db.data.passcode - (uint32_t)&ram_db.data.tx_level) + sizeof(ram_db.data.passcode);
}

/*
 * Init and setup FLASH storage and load it into RAM
 */

void load_flash_to_ram(void)
{
    uint32_t err_code;
    pstorage_module_param_t pstorage_param;

    // Init the flash based storage and pull out our stored configuration
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);
    
    pstorage_param.cb = pstorage_ntf_cb;
    pstorage_param.block_size = sizeof(flash_db_t);
    RMLOG ("Flash storage requires %u bytes", sizeof(flash_db_t));
    pstorage_param.block_count = 1;
    
    err_code = pstorage_register(&pstorage_param, &pstorage_block_id);
    APP_ERROR_CHECK(err_code);
    
    // Get pointer to read only area and copy it to RAM
    p_flash_db = (flash_db_t *)pstorage_block_id.block_id;
    memcpy(&ram_db, p_flash_db, sizeof(flash_db_t));
}

/*
 * Setup the storage space and init with defaults etc
 */

bool settings_init(void) 
{
    bool magic_was_already_set;

    load_flash_to_ram();

    /*
     * Check if the retrieved data is valid by looking for magic byte. 
     *  If it isn't then recreate with the defaults and flash it back down.
     */
        
    if (p_flash_db->data.magic_byte != MAGIC_FLASH_BYTE)
    {
        RMLOG("Resetting configuration");

        magic_was_already_set = false;
            
        // So flash knows it's 4reals
        memset(&ram_db, 0, sizeof(flash_db_t));
        ram_db.data.magic_byte = MAGIC_FLASH_BYTE;
        memcpy(ram_db.data.device_name, DEVICE_NAME, (sizeof(DEVICE_NAME) < DEVICE_NAME_MAX_LEN) ? sizeof(DEVICE_NAME) : DEVICE_NAME_MAX_LEN-1);
        
        // Get various modules defaults
        resetRMCSConfig();

        // Write
        persist_to_storage();

    }
    else {
        RMLOG("Restoring configuration");
        magic_was_already_set = true;
        restoreRMCSConfig(&ram_db.data.tx_level, size_of_rmcs_storage());
    }

    pstorage_access_wait();

    return magic_was_already_set;
    
}


/*
 * Called by a module when it wants to save some data down
 */

void save_to_storage(uint8_t module, void *data, uint16_t length, uint8_t write)
{
    void        *dest = NULL;
    uint16_t    size  = 0;

    /*
     * Depending on the module we should update the ram_db storage area
     */
    
    switch (module) {
            
        case SETTINGS_MODULE_RMCS:

            // Point to the first element
            dest = &ram_db.data.tx_level;
            size = size_of_rmcs_storage();
            break;

       default:
            return;
    }

    // Verify and then Copy
    if (size != length) {
        RMLOG("CRITICAL: Problem size (%u) with length (%u)", size, length);
        return;
    }
    else {
        memcpy(dest, data, size);
    }
    
    /* Whether we should write it down to flash or not */
    if (write) {
        // Write
        persist_to_storage();
        
    }
    
}

/*
 * Device name doesn't live anywhere else
 */

void set_device_name(uint8_t *name, uint8_t length)
{
    memset(ram_db.data.device_name, 0, sizeof(ram_db.data.device_name));
    memcpy(ram_db.data.device_name, name, (length < DEVICE_NAME_MAX_LEN) ? length : DEVICE_NAME_MAX_LEN-1);
    
    persist_to_storage();
    
}

uint8_t *get_device_name(void)
{
    return (ram_db.data.device_name);
}


/**
 * @}
 */
