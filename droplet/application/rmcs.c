/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * This is the firmware for Roam Creative Ltd iBeacon implementation with various enhancements
 *  to aid battery life and detection.
 *
 * (c) Roam Creative Ltd 2014
 *
 *
 * Need to know that security is enabled, by reading RMCS_UUID_BEACON_SECURITYCTRL_CHAR   0x1004 (if its SECURITY_MODE_PASSWORD (0x01) then password mode is enabled)
 *
 * Then you need to unlock it to allow configuration access
 *
 * Write SECURITY_CTRL_PASSWORD (which is 0x01) to RMCS_UUID_BEACON_SECURITYCTRL_CHAR   0x1003
 * This will set it up to receive the passcode
 *
 * Write the passcode (20 bytes, pad with 0x00’s) to RMCS_UUID_BEACON_PASSCODE_CHAR       0x1005
 *
 *
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "ble_conn_params.h"
#include "ble.h"
//#include "ble_gap.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
// #include "ble_radio_notification.h"      CSM: TODO fix this as unavailable on '52
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "pstorage.h"
//#include "app_gpiote.h"
#include "app_timer.h"
#include "app_button.h"
#include "ble_roam.h"
#include "droplet.h"
#include "logger.h"
#include "simple_uart.h"
#include "rmcs.h"
#include "settings.h"
#include "app_scheduler.h"

/*****************************************************************************************************************
 *
 *                                    Local Function Forward Definition
 *
 *****************************************************************************************************************/

static void saveConfig(uint8_t write);
static void enter_dfu(void);

/*****************************************************************************************************************
 *
 *                                             Extern Variables
 *
 *****************************************************************************************************************/

#ifdef BLE_TEST_MODE
    extern void led_on(uint32_t pin_number);
#endif

extern void advertising_start(void);

extern uint32_t             m_mins_standard;
extern uint32_t             m_mins_slowmode;
extern uint16_t             m_conn_handle;

/*****************************************************************************************************************
 *
 *                                             Static Variables
 *
 *****************************************************************************************************************/
 
ble_rmcs_t           		m_rmcs;                                     /**< Roam Configuration Service */
ble_rmcs_data_t             rcms_data;                                  /**< Roam Config Service data copy */
static uint8_t              m_config_locked  = true;                    /**< Configuration is locked */
static uint8_t              m_config_changed = false;                   /**< Configuration has changed */
static uint8_t              m_locked_out     = false;                   /**< Whether we are currently locked out */
static uint8_t              m_security_ctrl;
static uint8_t              m_security_timer_mode;

APP_TIMER_DEF(m_security_timer_id);                                     /**< Security timer */
APP_TIMER_DEF(m_io_timer_id);                                           /**< IO / Buzz / Vibration / Flash timer */

/*****************************************************************************************************************
 *
 *                                                Functions
 *
 *****************************************************************************************************************/

/**
 * Handler for the security timeout, action depends on security state
 */

static void security_timeout_handler(void * p_context)
{

    UNUSED_PARAMETER(p_context);
    RMLOG("Security timer fired");

    // If we're meant to be determining that we get kicked off
    if (m_security_timer_mode == SECURITY_TMR_KICKOFF) {
        RMLOG ("Booting connection and disabling connectability");
        security_violation();
        return;
    }
    else if (m_security_timer_mode == SECURITY_TMR_PROTECT) {
        RMLOG("Allow connectable advertising again");
        m_locked_out = false;
        advertising_start();
    }

}

/**
 * Handler for the io timeout, action depends on board
 */

static void io_timeout_handler(void * p_context)
{
    
    UNUSED_PARAMETER(p_context);
    RMLOG("IO timer fired");

    // Disable the motor
//    nrf_gpio_pin_clear(VIBRATION_MOTOR_EN);

#if !DROPLET_RELEASE_MODE
    // Turn off the LED
    nrf_gpio_pin_set(LED_RED);
#endif
    
}


/**
 * Called when we receive a new connection, resets everything to default
 */

void security_connection(void)
{
    RMLOG("Securing connection");
    m_security_ctrl  = SECURITY_CTRL_IDLE;
    m_config_changed = false;
    m_config_locked  = false;
    m_locked_out     = false;
    m_security_timer_mode = SECURITY_TMR_IDLE;

    switch (uint16_revdecode(rcms_data.securitymode)) {

        case SECURITY_MODE_PASSWORD:
            RMLOG("Setting up policy for password");
            m_config_locked = true;

            RMLOG("Setting up timers for kickoff after xx seconds..");
            app_timer_start(m_security_timer_id, SEC_CORRECT_ANSWER, NULL);
            m_security_timer_mode = SECURITY_TMR_KICKOFF;
            break;

        default:
            RMLOG("No policy enforced, open config");
            break;

    }

}

/**
 * Called when we are disconnected
 */

void security_disconnection(void)
{
    RMLOG("Secured disconnection");
    m_security_ctrl = SECURITY_CTRL_IDLE;

    // Clear down any timers if active and we're not in timeout
    if (m_security_timer_mode == SECURITY_TMR_KICKOFF) {
        RMLOG("Disabling security kickoff timer");
        app_timer_stop(m_security_timer_id);
        m_security_timer_mode = SECURITY_TMR_IDLE;
    }
    else if (m_security_timer_mode == SECURITY_TMR_PROTECT) {
        RMLOG("We are in protected lock down");
    }

}

/**
 * Successful sequence for unlocking device
 */

 void security_unlock(void) 
 {
    RMLOG("Droplet is unlocked for configuration");

    m_config_locked = false;
    m_security_ctrl = SECURITY_CTRL_IDLE;

    if (m_security_timer_mode == SECURITY_TMR_KICKOFF) {
        RMLOG("Stopping security timer");
        app_timer_stop(m_security_timer_id);
        m_security_timer_mode = SECURITY_TMR_IDLE;
    }
 }

/**
 * Called when they do something we dont like
 */

void security_violation(void)
{
    uint32_t err_code;

    RMLOG("Security violated, forced disconnect");
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
    }

    // Disable connectivity
    m_security_timer_mode = SECURITY_TMR_PROTECT;
    app_timer_stop(m_security_timer_id);
    app_timer_start(m_security_timer_id, SEC_KICKOFF_DELAY, NULL);

    // Disable connectability
    m_locked_out = true;
    RMLOG("Connectability is locked out");

}

/**
 * Returns whether we're currently locked out
 */

uint8_t device_is_locked(void)
{
    return m_locked_out;
}

/**
 * Return whether configuration is locked
 */

uint8_t config_is_locked(void)
{
    return m_config_locked;
}

/**
 * Return whether configuration is changed
 */

uint8_t config_is_changed(void)
{
    return m_config_changed;
}

/**
 * Called when a config change is attempted
 * Returns true = allowed
 * Results false = not allowed and disconnect/violation reported
 */

uint8_t config_attempt_change(void)
{
    RMLOG("Attempting to change config");
    if (m_config_locked) {
        security_violation();
        return (false);
    }

    return (true);
}

/**
 * Sets that the config has been changed
 */

void config_now_changed(void)
{
    RMLOG("Config has been changed");
    m_config_changed = true;
}

/*
 * Delayed restart
 */

static void handle_delayed_restart(void)
{
    wait_for_flash_and_reset();
}

static void delayed_restart(void)
{
    
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t) handle_delayed_restart);
    
}


/*
 * Erases the flash and restarts the droplet, will end up with defaults
 */

void erase_and_restart(void)
{
    RMLOG("Erasing and restarting");
    settings_erase();
    delayed_restart();
}

/*
 * Put the device into the DFU update mode
 */

static void enter_dfu(void)
{
    RMLOG("Setting ready for DFU mode");

#ifdef BLE_TEST_MODE
    led_on(LED_1);
    led_on(LED_2);
#endif

    // Set a flag we can detect in the boot loader
    sd_power_gpregret_set(0x01);
    delayed_restart();

}

/*
 * Restarts the device
 */

void device_restart(void)
{
    RMLOG("Restarting");
    delayed_restart();
}

/*
 * Vibrates the device. It uses the data temporarily stored in counter1 as number of ms
 */

void device_vibrate(uint8_t longvibe)
{
}

/**
 * Update the debug counter
 */

void update_counter1(uint32_t value)
{
	uint32_revencode(value, rcms_data.counter1);
}


/**
 * Update the debug counter
 */

void update_counter2(uint32_t value)
{
	uint32_revencode(value, rcms_data.counter2);
}


/**
 * Handles the dynamic changes to the device flags
 */

static void update_device_ctrl(uint16_t ctrl)
{
    RMLOG("Hit the device control with 0x%X", ctrl);
    
    if (ctrl & DEVICE_CTRL_ERASE) {
        erase_and_restart();
    }
    if (ctrl & DEVICE_CTRL_DFU) {
        enter_dfu();
    }
    if (ctrl & DEVICE_CTRL_RESTART) {
        device_restart();
    }
    if (ctrl & (DEVICE_CTRL_SHORT_VIBE | DEVICE_CTRL_LONG_VIBE)) {
        device_vibrate(ctrl & DEVICE_CTRL_LONG_VIBE);
    }
    
    
#if 0
    if (ctrl & DEVICE_CTRL_FLASH_LED) {
        droplet_flash_led();
    }
    if (ctrl & DEVICE_CTRL_SHORT_BUZZ) {
        droplet_buzz_buzzer(BUZZER_SHORT);
    }
    if (ctrl & DEVICE_CTRL_LONG_BUZZ) {
        droplet_buzz_buzzer(BUZZER_LONG);
    }
    if (ctrl & DEVICE_CTRL_BEEPBEEP_BUZZ) {
        droplet_buzz_buzzer(BUZZER_BEEPBEEP);
    }
#endif
}


/**
 * Handles the dynamic changes to the security flags
 */

static void update_security_ctrl(uint16_t ctrl)
{
    RMLOG("Hit the security control with 0x%X", ctrl);

    if (ctrl == SECURITY_CTRL_PASSWORD) {
        RMLOG("Waiting for password attempt");
        m_security_ctrl = ctrl;
    }
    else if (ctrl == SECURITY_CTRL_RESET) {                             // TODO: REMOVE THIS ATTRIBUTE
        // Reset device..
    }
    else {
        RMLOG("Security set to idle");
        m_security_ctrl = SECURITY_CTRL_IDLE;
    }
}

/**
 * Handles the dynamic changes to the security flags
 */

static void update_security_mode(uint16_t mode)
{
    RMLOG("Updating security mode with 0x%X", mode);

    switch (mode) {

        case SECURITY_MODE_NO_PASSWORD:
        case SECURITY_MODE_PASSWORD:
        case SECURITY_MODE_CHALLENGE:
            uint16_revencode(mode, rcms_data.securitymode);
            saveConfig(true);
            break;

        default:
            RMLOG ("Invalid security mode");
            break;

    }

}

/**
 * Update the TX power level
 */

static void update_tx_level(uint8_t level)
{
    RMLOG("TODO");
}

/**
 * Update the primary advertising timeout
 */

static void update_pri_timeout(uint16_t timeout)
{
    RMLOG("TODO");
}

/**
 * Update the primary advertising rate
 */

static void update_pri_rate(uint16_t level)
{
    RMLOG("TODO");
}

/**
 * Update the secondary advertising rate
 */

static void update_sec_rate(uint16_t level)
{
    RMLOG("TODO");
}

/**
 * Challenge has come in
 */

static void update_security_challenge(uint8_t *data)
{
    RMLOG("Got a challenge");
}

/**
 * Update the passcode
 */

static void update_passcode(uint8_t *data)
{
    /*
     * If the device is locked and we're in "compare password" phase, lets do that
     */

    if (m_config_locked) {

        if (m_security_ctrl != SECURITY_CTRL_PASSWORD) {
            RMLOG("Passcode failed state");
            security_violation();
        }
        else {
            RMLOG("Comparing passcode attempt with stored passcode");
            if (memcmp(data, rcms_data.passcode, 20) == 0) {           // Bad me with 20...
                RMLOG("Passcode matches, unlocked for configuration");
                security_unlock();
            }
            else {
                RMLOG("Passcode failed");
                security_violation();
            }
        }
    }
    else {
        RMLOG("Changing passcode");
        memset((char *)rcms_data.passcode, 0x00,  sizeof(rcms_data.passcode));
        memcpy((char *)rcms_data.passcode, data,  sizeof(rcms_data.passcode));
        
        saveConfig(true);
    }
}

/*
 * This is called when the Roam Configuration service has a characteristic to update
 */

static void rmcs_write_handler(ble_rmcs_t * p_lbs, rmcs_data_type_t type, uint8_t *data)
{
      
    RMLOG("Write handler for %u", type);

    switch(type)
    {
        case rmcs_counter1_char_data:
            if (config_attempt_change()) {
                update_counter1(uint32_revdecode(data));
            }
            break;
    
        case rmcs_counter2_char_data:
            if (config_attempt_change()) {
                update_counter2(uint32_revdecode(data));
            }
            break;

        case rmcs_challenge_data:
            if (config_attempt_change()) {
                // Not using it yet anyway
                update_security_challenge(data);
            }
            break;
            
        case rmcs_tx_level_data:
            if (config_attempt_change()) {
                update_tx_level(*data);
            }
            break;
            
        case rmcs_pri_timeout_data:
            if (config_attempt_change()) {
                update_pri_timeout(uint16_revdecode(data));
            }
            break;
            
        case rmcs_pri_rate_data:
            if (config_attempt_change()) {
                update_pri_rate(uint16_revdecode(data));
            }
            break;
            
        case rmcs_sec_rate_data:
            if (config_attempt_change()) {
                update_sec_rate(uint16_revdecode(data));
            }
            break;

        case rmcs_devicectrl_data:
            if (config_attempt_change()) {
                update_device_ctrl(uint16_revdecode(data));
            }
            break;
            
        case rmcs_securityctrl_data:
            // We allow access to this while locked..
            update_security_ctrl(uint16_revdecode(data));
            break;

        case rmcs_securitymode_data:
            if (config_attempt_change()) {
                update_security_mode(uint16_revdecode(data));
            }
            break;
        
        case rmcs_passcode_data:
            update_passcode(data);                       
            break;

        default:
            break;
    }

}


/*
 * When it's restored from flash, we just copy straight into our array (yes, problems of bad sizing if you fuck around)
 */

void restoreRMCSConfig(void *saved, uint16_t length)
{
    RMLOG("Restoring RMCS config of %u bytes", length);
    
    /* Clear our values out */
    memset((void *)&rcms_data, 0, sizeof(rcms_data));
    
    /* Copy the raw saved data back */
    memcpy(&rcms_data.tx_level, saved, length);

}


/*
 * Get defaults on a configuration reset
 */

void resetRMCSConfig(void)
{
    RMLOG("Resetting the RMCS config to defaults");
    
    // Clear our values out
    memset((void *)&rcms_data, 0, sizeof(rcms_data));
    
    // Set up defaults in the sense_data
    rcms_data.tx_level = (uint8_t)DROPLET_DEFAULT_TX_POWER;
    uint16_revencode(DROPLET_DEFAULT_CHANGE_ADV_RATE, rcms_data.adv_primary_timeout);
    uint16_revencode(DROPLET_DEFAULT_ADV_RATE, rcms_data.adv_primary_rate);
    uint16_revencode(DROPLET_DEFAULT_SECOND_ADV_RATE, rcms_data.adv_secondary_rate);

    uint16_revencode(1000, rcms_data.securityctrl);
    uint16_revencode(DROPLET_DEFAULT_SECURITY_MODE, rcms_data.securitymode);

    // Security settings - Copy the string passcode but remote the \n
    if (sizeof(DROPLET_DEFAULT_PASSCODE)-1 > sizeof(rcms_data.passcode)) {
        RMLOG("CRITICAL: YOUR DEFAULT PASSCODE IS TOO LONG!");
        memcpy((char *)rcms_data.passcode, DROPLET_DEFAULT_PASSCODE, sizeof(rcms_data.passcode));
    }
    else {
        memcpy((char *)rcms_data.passcode, DROPLET_DEFAULT_PASSCODE, sizeof(DROPLET_DEFAULT_PASSCODE)-1);
        RMLOG("Default password is %s", rcms_data.passcode);
    }
    
    // Save back to storage (we don't write on a restore as thats taken care of elsewhere)
    saveConfig(false);
    
}


/*
 * Tells the settings module that it needs to update/save out the data
 */

static void saveConfig(uint8_t write)
{
    // Save back to storage (we don't write on a restore as thats taken care of elsewhere)
    save_to_storage(SETTINGS_MODULE_RMCS, &rcms_data.tx_level,
                    ((uint32_t)&rcms_data.passcode - (uint32_t)&rcms_data.tx_level) + sizeof(rcms_data.passcode),
                    write);
}

/*
 * Register the Roam Configuration Service
 */

void registerRMCS(void)
{
    uint32_t err_code;
    ble_rmcs_init_t rmcs_init;

    // Reset security timer to idle
    m_security_timer_mode = SECURITY_TMR_IDLE;

    // Security timer
    err_code = app_timer_create(&m_security_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                security_timeout_handler);
    APP_ERROR_CHECK(err_code);

    // GPIO timer
    err_code = app_timer_create(&m_io_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                io_timeout_handler);
    APP_ERROR_CHECK(err_code);
    
    /*
     * Init Roam Configuration Service
     */

    rmcs_init.write_handler = rmcs_write_handler;
    rmcs_init.pData = &rcms_data;
    err_code = ble_rmcs_init(&m_rmcs, &rmcs_init);
    APP_ERROR_CHECK(err_code);
    
    RMLOG("Initialised configuration service");

}


/**
 * @}
 */
