/*
 * SX9300 Driver
 * ----------------
 *
 * Author: Chris Moore
 * Notes: 
 *
 *
 * (c) Roam Creative Ltd 2017
 *
 */

#ifndef SX9300_H__
#define SX9300_H__


// --------------------------- INCLUDES ----------------------------------



// --------------------------- DEFINES ----------------------------------

/* Define following macro if TXEN GPIO pin needs to be used for enabling/disabling SX9300. Otherwise TXEN will always be enabled */
#undef SX9300_SLEEP_WITH_TXEN

/* Define following macro if manual compensation is required on loss of body proximity  */
#define FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

/* Upgraded to 11 in order to be able to set/get max # consecutive registers in one write/read */
#define SX9300_TWI_MAX_BUFFER      11


/* SX9300 Registers Addresses */

/* Interrupt & Status Registers */
#define SX9300_REG_IRQ_SRC          0x00
#define SX9300_REG_STAT             0x01
#define SX9300_REG_IRQ_MSK          0x03
/* Proximity Sensing Control Registers */
#define SX9300_REG_PROX_CTRL0       0x06
#define SX9300_REG_PROX_CTRL1       0x07
#define SX9300_REG_PROX_CTRL2       0x08
#define SX9300_REG_PROX_CTRL3       0x09
#define SX9300_REG_PROX_CTRL4       0x0A
#define SX9300_REG_PROX_CTRL5       0x0B
#define SX9300_REG_PROX_CTRL6       0x0C
#define SX9300_REG_PROX_CTRL7       0x0D
#define SX9300_REG_PROX_CTRL8       0x0E
/* Smart Specific Absorption Rate (SAR) Config Registers */
#define SX9300_REG_SAR_CTRL0        0x0F
#define SX9300_REG_SAR_CTRL1        0x10
/* Sensor Data Readback Registers */
#define SX9300_REG_SENSOR_SEL       0x20
#define SX9300_REG_USE_MSB          0x21
#define SX9300_REG_USE_LSB          0x22
#define SX9300_REG_AVG_MSB          0x23
#define SX9300_REG_AVG_LSB          0x24
#define SX9300_REG_DIFF_MSB         0x25
#define SX9300_REG_DIFF_LSB         0x26
#define SX9300_REG_OFFSET_MSB       0x27
#define SX9300_REG_OFFSET_LSB       0x28
#define SX9300_REG_SAR_DELTA        0x29
#define SX9300_REG_SAR_RATIO        0x2A
/* Software Reset */
#define SX9300_REG_RESET            0x7F


/* Default regiters values */

#define SX9300_REG_IRQ_CONV_DONE        0x08    // Conversion Done IRQ Enable
#define SX9300_REG_IRQ_COMP_DONE        0x10    // Compensation Done IRQ Enable
#define SX9300_REG_IRQ_MSK_DOZE_VALUE   0X60    // Close and Far IRQs Enable
#define SX9300_REG_IRQ_MSK_VALUE        0x78    // All above-mentioned IRQs Enable

#define SX9300_REG_SAR_CTRL0_VALUE      0x20    // SAR Engine Debouncer (body un/detection): 2 samples, SARDELTATHRESH (off)
#define SX9300_REG_SAR_CTRL1_VALUE      0x60    // 0x99    // SAR Ratio Threshold ± SAR Hyst (±2)

/* Define SX9300_FPC_TEST if using ROAM's FPC  */

#if defined SX9300_FPC_TEST && defined SX9300_TEST_MODE

/* Register values used for testing Roam FPC */
#ifdef USING_SEMTECH_ELECTRODE

/* Register values used for testing Semtech FPC */
#define SX9300_REG_PROX_CTRL0_VALUE     0x7F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED 0x70    // 4 sensors disabled //0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_SLOW      0x7F    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL1_VALUE     0x43    // Shield Enable, Small input capacitance range
#define SX9300_REG_PROX_CTRL2_VALUE     0x50    // Gain: 8x, 167kHz Sampling Freq, Coarsest capacitance measurement resolution
#define SX9300_REG_PROX_CTRL3_VALUE     0x21    // Doze mode disabled, 2x Doze Scan Period, Low Prox-raw filter strength
#define SX9300_REG_PROX_CTRL4_VALUE     0x40    // Average Threshold value (±128x) = ± 8192 = ± 0x2000
#define SX9300_REG_PROX_CTRL5_VALUE     0x0F    // Averag Debouncer disabled, Lowest Negative- & Highest Positive-average filter strength
#define SX9300_REG_PROX_CTRL6_VALUE     0x09    // Proximity Detection Threshold value = 180
#define SX9300_REG_PROX_CTRL7_VALUE     0x40    // AVGCOMPDIS (off), COMPMETHOD (independent), HYST (32), CLOSEDEB (off), FARDEB (off)
#define SX9300_REG_PROX_CTRL8_VALUE     0x00    // STUCK (off), COMPPRD (off)

#undef SX9300_REG_SAR_CTRL0_VALUE
#define SX9300_REG_SAR_CTRL0_VALUE      0x10    // SAR Engine Debouncer (body un/detection): 2 samples, SARDELTATHRESH (off)
#undef SX9300_REG_SAR_CTRL1_VALUE
#define SX9300_REG_SAR_CTRL1_VALUE      0x32    // 0x99    // SAR Ratio Threshold ± SAR Hyst (±2)

#endif

//#undef  SX9300_REG_IRQ_MSK_VALUE
//#define SX9300_REG_IRQ_MSK_VALUE        0x60

#if defined USING_ROAM_ELECTRODE_V2 || defined USING_ROAM_ELECTRODE_V1

/* Register values used for testing Semtech FPC */
#define SX9300_REG_PROX_CTRL0_VALUE     0x7C    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED 0x70    // 4 sensors disabled //0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_SLOW      0x7C    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL1_VALUE     0x43    // Shield Enable, Small input capacitance range
#define SX9300_REG_PROX_CTRL2_VALUE     0x50    // Gain: 8x, 167kHz Sampling Freq, Coarsest capacitance measurement resolution
#define SX9300_REG_PROX_CTRL3_VALUE     0x61    // Doze mode disabled, 2x Doze Scan Period, Low Prox-raw filter strength
#define SX9300_REG_PROX_CTRL4_VALUE     0xC0    // Average Threshold value (±128x) = ± 8192 = ± 0x2000
#define SX9300_REG_PROX_CTRL5_VALUE     0x0F    // Averag Debouncer disabled, Lowest Negative- & Highest Positive-average filter strength
#define SX9300_REG_PROX_CTRL6_VALUE     0x0D    // Proximity Detection Threshold value = 180
//#define SX9300_REG_PROX_CTRL6_VALUE_OUT 0x03    // Proximity Detection Threshold value = 800
#define SX9300_REG_PROX_CTRL7_VALUE     0x00    // AVGCOMPDIS (off), COMPMETHOD (independent), HYST (32), CLOSEDEB (off), FARDEB (off)
#define SX9300_REG_PROX_CTRL8_VALUE     0x00 

#undef SX9300_REG_SAR_CTRL0_VALUE
#define SX9300_REG_SAR_CTRL0_VALUE      0x10    // SAR Engine Debouncer (body un/detection): 2 samples, SARDELTATHRESH (off)
#undef SX9300_REG_SAR_CTRL1_VALUE
#define SX9300_REG_SAR_CTRL1_VALUE      0x3    // 0x99    // SAR Ratio Threshold ± SAR Hyst (±2)

#endif // USING_ROAM_ELECTRODE_V2


#elif defined SX9300_TEST_MODE

/* Register values used for testing Semtech FPC */
#define SX9300_REG_PROX_CTRL0_VALUE     0x7F    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED 0x00    // 4 sensors disabled //0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_SLOW      0x7F    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL1_VALUE     0x43    // Shield Enable, Small input capacitance range
#define SX9300_REG_PROX_CTRL2_VALUE     0x77    // Gain: 8x, 167kHz Sampling Freq, Finest capacitance measurement resolution
#define SX9300_REG_PROX_CTRL3_VALUE     0x01    // Doze mode disabled, 2x Doze Scan Period, Low Prox-raw filter strength
#define SX9300_REG_PROX_CTRL4_VALUE     0x40    // Average Threshold value (±128x) = ± 8192 = ± 0x2000
#define SX9300_REG_PROX_CTRL5_VALUE     0x0F    // Averag Debouncer disabled, Lowest Negative- & Highest Positive-average filter strength
#define SX9300_REG_PROX_CTRL6_VALUE     0x09    // 0x0C = 240 // 0x04 = 80 // Proximity Detection Threshold value = 180
#define SX9300_REG_PROX_CTRL7_VALUE     0x00    // AVGCOMPDIS (off), COMPMETHOD (independent), HYST (32), CLOSEDEB (off), FARDEB (off)
#define SX9300_REG_PROX_CTRL8_VALUE     0x00    // STUCK (off), COMPPRD (off)


#else // Release mode

#ifdef USING_SEMTECH_ELECTRODE

/* Register values used for testing Semtech FPC */
#define SX9300_REG_PROX_CTRL0_VALUE     0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED 0x00    // 4 sensors disabled //0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_SLOW      0x7F    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL1_VALUE     0x43    // Shield Enable, Small input capacitance range
#define SX9300_REG_PROX_CTRL2_VALUE     0x50    // Gain: 8x, 167kHz Sampling Freq, Coarsest capacitance measurement resolution
#define SX9300_REG_PROX_CTRL3_VALUE     0x21    // Doze mode disabled, 2x Doze Scan Period, Low Prox-raw filter strength
#define SX9300_REG_PROX_CTRL4_VALUE     0x40    // Average Threshold value (±128x) = ± 8192 = ± 0x2000
#define SX9300_REG_PROX_CTRL5_VALUE     0x0F    // Averag Debouncer disabled, Lowest Negative- & Highest Positive-average filter strength
#define SX9300_REG_PROX_CTRL6_VALUE     0x05    // Proximity Detection Threshold value = 180
//#define SX9300_REG_PROX_CTRL6_VALUE_OUT 0x08 //0x0E    // Proximity Detection Threshold value = 800
#define SX9300_REG_PROX_CTRL7_VALUE     0x00    // AVGCOMPDIS (off), COMPMETHOD (independent), HYST (32), CLOSEDEB (off), FARDEB (off)
#define SX9300_REG_PROX_CTRL8_VALUE     0x00    // STUCK (off), COMPPRD (off)

#undef SX9300_REG_SAR_CTRL0_VALUE
#define SX9300_REG_SAR_CTRL0_VALUE      0x10    // SAR Engine Debouncer (body un/detection): 2 samples, SARDELTATHRESH (off)
#undef SX9300_REG_SAR_CTRL1_VALUE
#define SX9300_REG_SAR_CTRL1_VALUE      0x03    // 0x99    // SAR Ratio Threshold ± SAR Hyst (±2)

#else // USING_SEMTECH_ELECTRODE


#ifdef USING_ROAM_ELECTRODE_V2

/* Register values used for testing Semtech FPC */
#define SX9300_REG_PROX_CTRL0_VALUE     0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED 0x00    // 4 sensors disabled //0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_SLOW      0x7F    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL1_VALUE     0x43    // Shield Enable, Small input capacitance range
#define SX9300_REG_PROX_CTRL2_VALUE     0x50    // Gain: 8x, 167kHz Sampling Freq, Coarsest capacitance measurement resolution
#define SX9300_REG_PROX_CTRL3_VALUE     0x21    // Doze mode disabled, 2x Doze Scan Period, Low Prox-raw filter strength
#define SX9300_REG_PROX_CTRL4_VALUE     0xC0    // Average Threshold value (±128x) = ± 8192 = ± 0x2000
#define SX9300_REG_PROX_CTRL5_VALUE     0x0F    // Averag Debouncer disabled, Lowest Negative- & Highest Positive-average filter strength
#define SX9300_REG_PROX_CTRL6_VALUE     0x04    // Proximity Detection Threshold value =  80 ( 0x30  - 0x50 - [0x70])
#define SX9300_REG_PROX_CTRL6_VALUE_OUT 0x05    // Proximity Detection Threshold value = 100 ([0x44] - 0x64 -  0x84 )
#define SX9300_REG_PROX_CTRL7_VALUE     0x00    // AVGCOMPDIS (off), COMPMETHOD (independent), HYST (32), CLOSEDEB (off), FARDEB (off)
#define SX9300_REG_PROX_CTRL8_VALUE     0x00 

#undef SX9300_REG_SAR_CTRL0_VALUE
#define SX9300_REG_SAR_CTRL0_VALUE      0x10    // SAR Engine Debouncer (body un/detection): 2 samples, SARDELTATHRESH (off)
#undef SX9300_REG_SAR_CTRL1_VALUE
#define SX9300_REG_SAR_CTRL1_VALUE      0x03    // 0x99    // SAR Ratio Threshold ± SAR Hyst (±2)

#else // USING_ROAM_ELECTRODE_V2


#ifdef USING_ROAM_ELECTRODE_V1

#define SX9300_ACTIVE_SENSORS           0x0C    // bit0: CS0A, bit1: CS0B, bit2: CS1A, bit3:CS1B, bit4-7: not relevant

/* Register values used for testing Semtech FPC */
#define SX9300_REG_PROX_CTRL0_VALUE     0x0C    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED 0x00    // 4 sensors disabled //0x0F    // SCANPERIOD  30ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL0_SLOW      0x7C    // SCANPERIOD 400ms, All 4 sensors/electrodes enabled
#define SX9300_REG_PROX_CTRL1_VALUE     0x43    // Shield Enable, Small input capacitance range
#define SX9300_REG_PROX_CTRL2_VALUE     0x30    // Gain: 8x, 167kHz Sampling Freq, Coarsest capacitance measurement resolution
#define SX9300_REG_PROX_CTRL3_VALUE     0x21    // Doze mode disabled, 2x Doze Scan Period, Low Prox-raw filter strength
#define SX9300_REG_PROX_CTRL4_VALUE     0x40    // Average Threshold value (±128x) = ± 8192 = ± 0x2000
#define SX9300_REG_PROX_CTRL5_VALUE     0x0F    // Averag Debouncer disabled, Lowest Negative- & Highest Positive-average filter strength
#define SX9300_REG_PROX_CTRL6_VALUE     0x04    // Proximity Detection Threshold value =  80 ( 0x30  - 0x50 - [0x70])
#define SX9300_REG_PROX_CTRL6_VALUE_OUT 0x05    // Proximity Detection Threshold value = 100 ([0x44] - 0x64 -  0x84 )
#define SX9300_REG_PROX_CTRL7_VALUE     0x00    // AVGCOMPDIS (off), COMPMETHOD (independent), HYST (32), CLOSEDEB (off), FARDEB (off)
#define SX9300_REG_PROX_CTRL8_VALUE     0x00 

#undef SX9300_REG_SAR_CTRL0_VALUE
#define SX9300_REG_SAR_CTRL0_VALUE      0x10    // SAR Engine Debouncer (body un/detection): 2 samples, SARDELTATHRESH (off)
#undef SX9300_REG_SAR_CTRL1_VALUE
#define SX9300_REG_SAR_CTRL1_VALUE      0x03    // 0x99    // SAR Ratio Threshold ± SAR Hyst (±2)

#endif // USING_ROAM_ELECTRODE_V1


#endif // USING_ROAM_ELECTRODE_V2


#endif // USING_SEMTECH_ELECTRODE


#endif // SX9300_FPC_TEST or SX9300_TEST_MODE


// --------------------------- STRUCTS -----------------------------------



// --------------------------- ENUM TYPES ----------------------------------



// -------------------------- Functions ----------------------------------

/*! Initializes the device. */
void sx9300_init(void);

/*! Power up SX9300 and configure I2C interface */
bool sx9300_power_on(void);

/*! Power off SX9300 and uninint I2C and interrupt */
void sx9300_power_off(void);

/*! Reset SX9300 internal registers to default working values */
void sx9300_reset(void);

/*! Set TXEN GPIO pin (enable SX9300 sensing functionalities) and configure IRQ/GPIOTE handler */
void sx9300_wake(void);

/*! Clear TXEN GPIO pin (disable SX9300 sensing functionalities) */
void sx9300_sleep(void);

/*! Check SEMTECH_POWER GPIO pin status */
bool sx9300_get_power_status(void);

/*! Check SEMTECH_TXEN GPIO pin status */
bool sx9300_get_txen_status(void);

/*! Enable Doze Mode and start a slow scan */
void sx9300_start_slowscan(void);

/*! Stop slowscan and revert sensor configuration to default  */
void sx9300_stop_slowscan(void);

/*! Enable the sensor to generate a specified number of Conversion Done interrupts */
void sx9300_enable_conv_done_irq(void);

/*! Emulate a SW interrupt by running the Scheduler function for the IRQ Handler */
void sx9300_sw_irq_emulator(void);

/*! Check if SX9300 has started performing a measurement */
bool sx9300_measurement_in_progress(void);

/*! Function used for loop-testing the sensor */
void sx9300_test(void);

void twi_master_uninit(void);

void sx9300_twi_gpio_uninit(void);

void sx9300_enable_fastscan(void);


#endif // SX9300_H__
