/* 
 * SX9300 Driver
 * ----------------
 *
 * Author: Chris Moore
 * Notes:  
 *
 *
 * (c) Roam Creative Ltd 2017
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "ble_conn_params.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "pstorage.h"
#include "math.h"
#include "app_timer.h"
#include "app_button.h"
#include "droplet.h"
#include "logger.h"
#include "simple_uart.h"
#include "settings.h"
#include "nrf.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "nrf_delay.h"
#include "nrf_drv_twi.h"
#include "app_util_platform.h"
#include "app_scheduler.h"
#include "lifesole.h"

#include "sx9300.h"


/*****************************************************************************************************************
 *
 *                                             Constants
 *
 *****************************************************************************************************************/
#ifdef USING_ROAM_ELECTRODE_V1
/**< Maximum number of consecutive conversions to ignore before starting a measurment */
#define SX9300_MAX_IGNORED_SAMPLES      10
#else

#ifdef USING_ROAM_ELECTRODE_V2
/**< Maximum number of consecutive conversions to ignore before starting a measurment */
#define SX9300_MAX_IGNORED_SAMPLES      10
#else

/**< Maximum number of consecutive conversions to ignore before starting a measurment */
#define SX9300_MAX_IGNORED_SAMPLES      10
#endif // USING_ROAM_ELECTRODE_V2
#endif // USING_ROAM_ELECTRODE_V1


/**< Maximum number (i.e. timeout) of consecutive conversions allowed for a measurement */
#define SX9300_MAX_CONSEC_CONV_DONE     10

/**< Maximum proximity measurement time in seconds */
#define SX9300_MAX_MEASUREMENT_TIME     20

/**< I2C instance  */
static const nrf_drv_twi_t m_twi_master = NRF_DRV_TWI_INSTANCE(0);


/*****************************************************************************************************************
 *
 *                                             Extern Variables
 *
 *****************************************************************************************************************/



/*****************************************************************************************************************
 *
 *                                             Static Variables
 *
 *****************************************************************************************************************/

/**< I2C Initialisation status (Initialised/Not Initialised) */
static bool twi_inited = false;

/**< Whether IRQ interface has been initialised and IRQ handler registered  */
static bool sx9300_irq_registered = false;

/**< Status of SX9300 Conversion Done IRQ mask (Enabled/Disabled) */
static bool sx9300_conv_done_irq_enabled = false;

/**< Counter for number of conversion performed since the beginning of a given measurement */
static int8_t sx9300_conv_done_irq_downcounter = 0;

/**< Used for Log: number of IRQs yet to be serviced by SX9300 driver */
static unsigned int sx9300_irq_counter = 0;

#ifdef FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY
/**< Whether SX9300 has triggered and now is waiting for a compensation */
static bool waiting_for_compensation = false;
#endif

/**< Weather SX9300 has a mesurement in progress */
static bool measurement_in_progress = false;


/*
 * Timer used by the SX9300
 */

APP_TIMER_DEF(m_sx9300_timer_id);


static bool timer_started = false;
static bool force_reset_drv_param = false;


/*****************************************************************************************************************
 *
 *                                          External Functions
 *
 *****************************************************************************************************************/

extern void nrf52_pheriperals_check(void);


/*****************************************************************************************************************
 *
 *                                    Local Function Forward Definition
 *
 *****************************************************************************************************************/

/* I2C functions */
static uint32_t twi_master_init(void);
//static void twi_master_uninit(void);
void sx9300_twi_gpio_uninit(void);
bool sx9300_get_i2c_power_status(void);

/* IRQ functions */
static void sx9300_irq_handler(void);
static void sx9300_irq_scheduler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);

/* Configuration functions */
//static void sx9300_enable_fastscan(void);
static void sx9300_enable_slowscan(void);
static void sx9300_disable_doze_mode(void);

#if defined FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY
static void sx9300_manual_compensation(void);
#endif

/* Check functions */
static bool sx9300_check(void);
static uint8_t sx9300_release_irq_line(void);
static uint8_t sx9300_check_state(uint8_t irq_src);

/* Used for testing when using custom FPCs */
#if defined SX9300_TEST_MODE // || defined SX9300_FPC_TEST
static void sx9300_print_fpc_readings(void);
#endif

#if defined SX9300_FPC_TEST || defined SX9300_TEST_MODE || defined USING_ROAM_ELECTRODE_V2 //|| defined USING_ROAM_ELECTRODE_V1
static void sx9300_read_all_sensors(unsigned char sensor_data[4][10]);
#endif

#if 0 // Not used in this version
static bool sx9300_get_irq_status(bool service_interrupt);
#endif


static void sx9300_init_timer(void);
static void sx9300_set_timer(uint32_t seconds);
static void sx9300_stop_timer(void);
static void sx9300_timeout_handler(void * p_context);


/*****************************************************************************************************************
 *
 *                                                Functions
 *
 *****************************************************************************************************************/

/*
 * Write to the I2C bus (TWI)
 */

static int i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data)
{
    uint32_t err_code=0;
    uint8_t  buffer[SX9300_TWI_MAX_BUFFER+1];

    
    if (!twi_inited)
    {
        RMLOG("SX9300: attempt to write, but TWI not inited yet");

        /* Let's try to initialise it now */
        if (twi_master_init() != 0)
        {
            return -1;
        }
    }

    if (!sx9300_get_i2c_power_status())
    {
        RMLOG("SX9300: i2c_write() SVDD was OFF");
        nrf_gpio_pin_set(SEMTECH_SVDD);
        nrf_delay_ms(1);
    }
    
    if (length > SX9300_TWI_MAX_BUFFER)
    {
        RMLOG("SX9300: Write too big");
        return -1;
    }
    
    /* Prepare the buffer by combining the register address and data */
    buffer[0] = reg_addr;
    memcpy(&buffer[1], data, length);
    
    /* Send it all out */
    err_code = nrf_drv_twi_tx(&m_twi_master, slave_addr, buffer, length+1, false);
    
    if (err_code == NRF_ERROR_BUSY)
    {
        RMLOG("SX9300: i2c_write() NRF_ERROR_BUSY.");
        nrf_delay_ms(100);
    }

    if (err_code == NRF_ERROR_INTERNAL)
    {
        RMLOG("SX9300: i2c_write() NRF_ERROR_INTERNAL.");
        if (!sx9300_get_power_status())
        {
            RMLOG("SX9300: i2c_write(): sensor is powered off.");
            sx9300_power_on();
        }
        nrf_delay_ms(1);
    }

    if (err_code != NRF_SUCCESS)
    {
        RMLOG("SX9300: i2c_write() error 0x%02X: Failed to write register 0x%02X", err_code, reg_addr);
        
        nrf_delay_ms(1);
        err_code = nrf_drv_twi_tx(&m_twi_master, slave_addr, buffer, length+1, false);

        if (err_code != NRF_SUCCESS)
        {
            RMLOG("SX9300: i2c_write() failed again (0x%02X)! Aborting operation.", err_code);
            nrf52_pheriperals_check(); 
            return -1;
        }
    }
    
    return 0;
}


/*
 * Read from the I2C bus (TWI)
 */
static int i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char *data)
{
    uint32_t err_code=0;


    if (!twi_inited)
    {
        RMLOG("SX9300: attempt to read, but TWI not inited yet");

        /* Let's try to initialise it now */
        if (twi_master_init() != 0)
        {
            return -1;
        }

        nrf_delay_ms(1);
    }

    if (!sx9300_get_i2c_power_status())
    {
        RMLOG("SX9300: i2c_write() SVDD was OFF");
        nrf_gpio_pin_set(SEMTECH_SVDD);
        nrf_delay_ms(1);
    }
    
    /* Write the register we want to read */
    err_code = nrf_drv_twi_tx(&m_twi_master, slave_addr, &reg_addr, 1, true);
    
    if (err_code == NRF_ERROR_BUSY)
    {
        RMLOG("SX9300: i2c_read() NRF_ERROR_BUSY.");
        nrf_delay_ms(100);
    }

    if (err_code == NRF_ERROR_INTERNAL)
    {
        RMLOG("SX9300: i2c_read() NRF_ERROR_INTERNAL.");
        if (!sx9300_get_power_status())
        {
            RMLOG("SX9300: i2c_read(): sensor is powered off.");
            sx9300_power_on();
        }
        nrf_delay_ms(1);
    }

    if (err_code != NRF_SUCCESS)
    {
        RMLOG("SX9300: i2c_read() error 0x%02X: Failed to write register 0x%02X to read it. Let's try again...", err_code, reg_addr);
        nrf_delay_ms(1);
        err_code = nrf_drv_twi_tx(&m_twi_master, slave_addr, &reg_addr, 1, true);
        
        if (err_code != NRF_SUCCESS)
        {
            RMLOG("SX9300: i2c_read() failed again (0x%02X)! Aborting operation.", err_code);
            nrf52_pheriperals_check();            
            return -1;
        }
    }
    
    /* Read it */
    err_code = nrf_drv_twi_rx(&m_twi_master, slave_addr, data, length);

    if (err_code != NRF_SUCCESS)
    {
        RMLOG("SX9300: i2c_read() error 0x%02X: Failed to read register 0x%02X. Let's try again...", err_code, reg_addr);
        
        nrf_delay_ms(1);
        err_code = nrf_drv_twi_rx(&m_twi_master, slave_addr, data, length);
        
        if (err_code != NRF_SUCCESS)
        {
            RMLOG("SX9300: i2c_read() failed again (0x%02X)! Aborting operation.", err_code);
            return -1;
        }
    }
    
    return 0;
    
}


/**
 * set_register Sets a register on a device connected via I2C. It accepts the device's address,
 *   register location, and the register value.
 * @param address The address of the I2C device
 * @param r       The register's address on the I2C device
 * @param v       The new value for the register
 */

static void set_register(int address, unsigned char r, unsigned char v)
{
    i2c_write(address, r, 1, &v);
}


/*
 * Set all GPIO pins to default (input, buffer disconnected, no pull)
 */

void sx9300_release_gpio_pins(void)
{
    nrf_gpio_cfg_default(SEMTECH_POWER);
    nrf_gpio_cfg_default(SEMTECH_SVDD);
    nrf_gpio_cfg_default(SEMTECH_NRST);
    nrf_gpio_cfg_default(SEMTECH_INTERRUPT);
    nrf_gpio_cfg_default(SEMTECH_TXEN);

    sx9300_twi_gpio_uninit();

    //nrf_gpio_cfg_default(SEMTECH_I2C_CONFIG_SDA);
    //nrf_gpio_cfg_default(SEMTECH_I2C_CONFIG_SCL);

    //nrf_delay_ms(1); 
}


/*
 * Power off SX9300 and uninint I2C and interrupt
 */

void sx9300_power_off(void)
{
    /* Reset interrupt/event functionality for SX9300 */
    nrf_drv_gpiote_in_uninit(SEMTECH_INTERRUPT);

    RMLOG("SX9300: Powering OFF proximity sensor");

    /* Set TX to a default value: disabled */
    nrf_gpio_pin_clear(SEMTECH_TXEN);
    nrf_gpio_cfg_output(SEMTECH_TXEN);

    /* Power off i2c interface on SX9300 */
    nrf_gpio_pin_clear(SEMTECH_SVDD);
    nrf_gpio_cfg_output(SEMTECH_SVDD);
    
    /* Power-OFF SX9300 proximity sensing IC */
    nrf_gpio_pin_clear(SEMTECH_POWER);
    nrf_gpio_cfg_output(SEMTECH_POWER);
    
    /* Clear nRST line */
    nrf_gpio_pin_clear(SEMTECH_NRST);
    nrf_gpio_cfg_output(SEMTECH_NRST);
    
    /* Sense bit not to be set for SEMTECH_INTERRUPT */
    nrf_gpio_pin_clear(SEMTECH_INTERRUPT);
    nrf_gpio_cfg_default(SEMTECH_INTERRUPT);

    /* Unitialise the I2C interface */
    twi_master_uninit();

    sx9300_release_gpio_pins();

    RMLOG("SX9300: Proximity sensor powered OFF!");

}


/*
 * Power up SX9300 and configure I2C interface
 */

bool sx9300_power_on(void)
{
    
    RMLOG("SX9300: Enabling power to proximity sensor");

    nrf_gpio_pin_clear(SEMTECH_SVDD);
    nrf_gpio_pin_clear(SEMTECH_POWER);
    nrf_gpio_pin_clear(SEMTECH_TXEN);
    nrf_gpio_pin_clear(SEMTECH_NRST);
    
    /* Set the GPIO as output pins */
    nrf_gpio_cfg_output(SEMTECH_POWER);
    nrf_gpio_cfg_output(SEMTECH_SVDD);

#ifdef NRF_DEV_KIT

    /*
     * SX9300 DevKit (used for the demo together with Nordic Dev Kit) provides pullups for TXEN and NRST,
     * thus nRF52 GPIO pins don't need to be forced to have pullups
     */
    nrf_gpio_cfg_output(SEMTECH_TXEN);
    nrf_gpio_cfg_output(SEMTECH_NRST);

#else

    /* SX9300's TXEN and NRST ports require external pullups (as well as SDA, SCA and INTERRUPT ports) */

    nrf_gpio_cfg(
        SEMTECH_TXEN,
        NRF_GPIO_PIN_DIR_OUTPUT,
        NRF_GPIO_PIN_INPUT_DISCONNECT,
        NRF_GPIO_PIN_PULLUP,
        NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);
        
    nrf_gpio_cfg(
        SEMTECH_NRST,
        NRF_GPIO_PIN_DIR_OUTPUT,
        NRF_GPIO_PIN_INPUT_DISCONNECT,
        NRF_GPIO_PIN_PULLUP,
        NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);

#endif

    /* Power the sensor up (enable TI DC/DC converter) */
    nrf_gpio_pin_set(SEMTECH_POWER);
    nrf_delay_ms(1);

    /* Power the I2C interface module */
    nrf_delay_ms(1);


#ifdef SX9300_SLEEP_WITH_TXEN
    /* Let's keep the SX9300 in sleep mode */
    /* nrf_gpio_pin_set(SEMTECH_TXEN); */
#else
    /* Let's ENABLE the SX9300 */
    nrf_gpio_pin_set(SEMTECH_TXEN);
#endif

    /* Let's take the SX9300 out of reset */
    nrf_gpio_pin_set(SEMTECH_NRST);
    nrf_delay_ms(1);

    /* Initialise the nRF52 I2C interface */
    twi_master_init();
    
    return true;

}


/**
 * @brief Initialize the master TWI
 *
 * Function used to initialize master TWI interface that would communicate with device.
 *
 * @return NRF_SUCCESS or the reason of failure
 */

static uint32_t twi_master_init(void)
{
    /* Checks wether TWI has already been initialised */
    if (twi_inited)
    {
        /* No need to initialise it again */
        return 0;
    }

    uint32_t err_code;
    const nrf_drv_twi_config_t config =
    {
        .scl                = SEMTECH_I2C_CONFIG_SCL,
        .sda                = SEMTECH_I2C_CONFIG_SDA,
        .frequency          = TWI0_CONFIG_FREQUENCY,
        .interrupt_priority = TWI0_CONFIG_IRQ_PRIORITY
    };

    if (!sx9300_get_i2c_power_status())
    {
        nrf_gpio_pin_set(SEMTECH_SVDD);
        nrf_delay_ms(10);
    }
    
    err_code = nrf_drv_twi_init(&m_twi_master, &config, NULL /*twi_handler*/, NULL);
    APP_ERROR_CHECK(err_code);
    
    nrf_drv_twi_enable(&m_twi_master);
        
    RMLOG("SX9300: I2C inited at address 0x%02X SCL is %u and SDA is %u",
          SEMTECH_SX9300_ADDR,
          SEMTECH_I2C_CONFIG_SCL,
          SEMTECH_I2C_CONFIG_SDA);
    
    //nrf_delay_ms(1);
    twi_inited = true;

    return err_code;
}

/**
 * @brief Reset the GPIO pins used by TWI interface
 *
 * Function used for allowing twi_master_uninit() keeping the GPIO pins still configured, allowing the SX9300 not to waste current.
 * In fact, SX9300 expect external PULL-UP resistors on SDA and SCL pins while SVDD is High.
 *
 * @return NRF_SUCCESS or the reason of failure
 */
 
void sx9300_twi_gpio_uninit(void)
{
    RMLOG("SX9300: Releasing TWI/I2C GPIO pins...");
    nrf_gpio_cfg_default(SEMTECH_I2C_CONFIG_SDA);
    nrf_gpio_cfg_default(SEMTECH_I2C_CONFIG_SCL);
}


/**
 * @brief Unnitialize the master TWI
 *
 * Function used to uninitialize master TWI interface when powering off the SX9300.
 *
 * @return NRF_SUCCESS or the reason of failure
 */
 
void twi_master_uninit(void)
{
    /* Checks wether TWI has been unitialised already or not yet initialised */
    if (!twi_inited)
    {
        /* No need to uninitialise it again */
        return;
    }

    RMLOG("SX9300: Unitialising TWI/I2C module...");

    nrf_gpio_pin_clear(SEMTECH_SVDD);
    
    nrf_drv_twi_disable(&m_twi_master);
    
    nrf_drv_twi_uninit(&m_twi_master);

    sx9300_twi_gpio_uninit();

    /* Reset configuration for I2C GPIO ports */
//    nrf_gpio_cfg_default(SEMTECH_I2C_CONFIG_SDA);
/*
    nrf_gpio_cfg(
            SEMTECH_I2C_CONFIG_SDA,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
*/
    
//    nrf_gpio_cfg_default(SEMTECH_I2C_CONFIG_SCL);
/*
    nrf_gpio_cfg(
            SEMTECH_I2C_CONFIG_SCL,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
*/
        
    RMLOG("SX9300: I2C interface at address 0x%02X has been unitialised", SEMTECH_SX9300_ADDR);
    
    //nrf_delay_ms(1);
    twi_inited = false;

}


/*
 * Reset/Configure the SX9300 internal register with default customised values for LifeSole application
 */

void sx9300_reset(void)
{
    /* Initialisation of burst write vector for the 11 consecutive PROX and SAR registers */
    unsigned char reg_values[SX9300_TWI_MAX_BUFFER] =
                            {
                                /* Proximity Sensing Control registers */
#ifdef SX9300_SLEEP_WITH_TXEN
                                /* Enable all sensing functionalities (actual activity now depends on TXEN signal) */
                                SX9300_REG_PROX_CTRL0_VALUE,
#else
                                /* DISable all sensing functionalities (actual activity DOES NOT depend on TXEN signal) */
                                SX9300_REG_PROX_CTRL0_ALL_SENS_DISABLED,
#endif
                                SX9300_REG_PROX_CTRL1_VALUE,
                                SX9300_REG_PROX_CTRL2_VALUE,
                                SX9300_REG_PROX_CTRL3_VALUE,
                                SX9300_REG_PROX_CTRL4_VALUE,
                                SX9300_REG_PROX_CTRL5_VALUE,
                                SX9300_REG_PROX_CTRL6_VALUE,
                                SX9300_REG_PROX_CTRL7_VALUE,
                                SX9300_REG_PROX_CTRL8_VALUE,
                                /* Smart SAR Engine Control */
                                SX9300_REG_SAR_CTRL0_VALUE,
                                SX9300_REG_SAR_CTRL1_VALUE
                            };

    RMLOG("SX9300: Resetting SX9300 to default parameters");
    for (int k=0; k<SX9300_TWI_MAX_BUFFER; k++)
    {
        RMLOG("Reg %02d = 0x%02X", SX9300_REG_PROX_CTRL0+k, reg_values[k]);
    }

    /* Interrupts handling and status */
    //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 0x00);                              /* INFO: Only bit-4 (COMPDONEIRW) can be written */

    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_MSK, SX9300_REG_IRQ_MSK_VALUE);
    
    i2c_write(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, SX9300_TWI_MAX_BUFFER, reg_values);
    


#if 0   // Not used in this version

    /* Weird configuration procedure performed by driver for Semtech SX9300DEVKA */

#define SX9300_REG_STAT_VALUE           0x0F

    set_register(SEMTECH_SX9300_ADDR, 0x05, 0x00);                              // Reserved Register
    set_register(SEMTECH_SX9300_ADDR, 0x04, 0x00);                              // Reserved Register
    set_register(SEMTECH_SX9300_ADDR, 0x02, 0x00);                              // Reserved Register
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_STAT, SX9300_REG_STAT_VALUE);  // Read-only Register

#endif

}


/*
 * SX9300 IRQ Handler
 */

static void sx9300_irq_handler(void)
{

    /* IRQ SRC register value*/
    uint8_t irq_src = 0x0;
    /* New event to be generated (LS_EVENT_NO_BODY_DETECTED or LS_EVENT_BODY_DETECTED)*/
    lifesole_event_t new_prox_event;
    
    /* Multiple-IRQ control variables */
#ifdef FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY
    static bool compensation_happened = false;
    static bool waiting_for_second_comp = false;
#endif
    static int skip_readings_when_resuming_after_detection = 0;

    /* Check whether the timer has expired and need to reset the driver to default internal values */
    if (force_reset_drv_param)
    {
        force_reset_drv_param = false;

        /* Let's reset all control variables */
        sx9300_conv_done_irq_enabled = false;
        sx9300_conv_done_irq_downcounter = 0;
        skip_readings_when_resuming_after_detection = 0;
        
#ifdef FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY
        waiting_for_compensation = false;
        compensation_happened = false;     
        waiting_for_second_comp = false;
#endif

        /* Measurement has been completed */    
        measurement_in_progress = false;

        return;

    }


    /* Updates the number of scheduled interrupts that have been serviced */
    if (sx9300_irq_counter) {
        sx9300_irq_counter--;
    }
    else {
        RMLOG("SX9300: sx9300_irq_counter already = 0");
    }

    /* If the sensor is not active, let's consider the scheduled interrupt not valid anymore */
    if (!sx9300_get_power_status() || !sx9300_get_txen_status())
    {
        RMLOG("SX9300: SX9300 is disabled/powered off: interrupt ignored");

        if (sx9300_get_power_status())
        {
            sx9300_release_irq_line();
        }
        return;
    }

    /* Retrieve latest PROX detection from GREGRET register */
    bool previous_body_detection = lifesole_get_proximity_gpregret();

    /* Retrieve the IRQ-SCR internal register from SX9300 */
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 1, &irq_src);

    /* Retrieve REG-STAT register and perform a health-check of sensor interface and unwanted reset events */
    uint8_t sx9300_reg_stat = sx9300_check_state(irq_src);
    
    if (sx9300_reg_stat == 0x0F)
    {
        RMLOG("SX9300: %s(): skipping interrupt because reading was not valid (0x%02X)", __FUNCTION__, sx9300_reg_stat);

        return;
    }

    /* Let's set that a valid measurement has started/is in progress */
    measurement_in_progress = true;

    if (!timer_started)
    {
        sx9300_set_timer(SX9300_MAX_MEASUREMENT_TIME);
    }
    

    /* Keep only the useful data: 4 MSBs, object/body near/far detection for the 2 smart SAR electrodes */
    sx9300_reg_stat = (sx9300_reg_stat & 0xF0) >> 4;

    /* Whether both Smart SAR sensors agree on BODY non/detection */
#ifdef SX9300_ACTIVE_SENSORS
    bool valid_detection = (sx9300_reg_stat==0x0 || sx9300_reg_stat==SX9300_ACTIVE_SENSORS);
#else
    bool valid_detection = (sx9300_reg_stat==0x0 || sx9300_reg_stat==0xF);
#endif // SX9300_ACTIVE_SENSORS

    /* In case at least 1 of the 2 Smart SAR sensors has detected BODY proximity */
    bool new_partial_body_prox = (((sx9300_reg_stat & 0x3)==0x3) ^ ((sx9300_reg_stat & 0xC)==0xC));

    /* Whether the actual reading has detected BODY proximity */
    bool new_body_proximity = (valid_detection && sx9300_reg_stat)? true : false;


    /* Measurement is finished when no Far or Close irq is triggered */
    bool end_of_measurement = (irq_src & 0x60)? false : true;

    /* Whether compesation and conversion have happened */
    bool compensation_done  = (irq_src & 0x10)? true : false;
    bool conversion_done    = (irq_src & 0x08)? true : false;


#ifdef FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

    /* Procedure for handling/monitoring a compensation triggered by the SX9300 driver (total of 2 consecutive compensation are performed) */
    if (waiting_for_compensation)
    {
        if (compensation_done) {
            compensation_happened = true;
        }
        else
        {
            /* After compensation happens, following completion of proximity sensing will be signaled by a Conversion Done interrupt */
            if (compensation_happened && end_of_measurement)
            {
                if (waiting_for_second_comp)
                {
                    RMLOG("SX9300: %s(): COMPENSATION procedure completed: Valid Detection:%d - Body Proximity:%s - Detected Sensor:0x%02X", __FUNCTION__, valid_detection, new_body_proximity?"TRUE":"FALSE", sx9300_reg_stat);
                    
                    /* After SX9300 has completed a reading, go back into sleep */
                    sx9300_sleep();                    

                    /* Let's reset all control variables */
                    sx9300_conv_done_irq_enabled = false;
                    sx9300_conv_done_irq_downcounter = 0;
                    waiting_for_second_comp = false;
                    skip_readings_when_resuming_after_detection = 0;
                    waiting_for_compensation = false;
                    compensation_happened = false;                

                    /* Measurement has been completed */    
                    measurement_in_progress = false;
                    sx9300_stop_timer();

#ifdef SX9300_REG_PROX_CTRL6_VALUE_OUT
                    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE);
#endif

                    /* Let's generate the proper proximity event */
                    lifesole_set_proximity_gpregret(new_body_proximity);
                    lifesole_set_event_scheduler(LS_EVENT_NO_BODY_DETECTED);
                }
                else
                {
                    /* First compensation happened */
                    waiting_for_second_comp = true;
                    compensation_happened = false;
                    sx9300_manual_compensation();
                }
            }
        }

        /* Manual compensation procedure has completed */
        return;
    }

#endif // FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY


    /* No compensation was expected */
    if (compensation_done && sx9300_reg_stat==0x0)
    {
            RMLOG("SX9300: - Unplanned compensation happened on SX9300");
            
            /* After SX9300 has completed an unplanned compensation, go back into sleep and generate an event */
            sx9300_sleep();                    

            /* Measurement has been completed */
            measurement_in_progress = false;
            sx9300_stop_timer();

#ifdef SX9300_REG_PROX_CTRL6_VALUE_OUT
            set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE);
#endif

            /* Compensation has forced the proximity sensor to lose information about proximity detection, the sensor believes no BODY was there */
            lifesole_set_proximity_gpregret(false);
            lifesole_set_event_scheduler(LS_EVENT_NO_BODY_DETECTED);

            return;
    }


    /* Check if "skip_readings_when_resuming_after_detection"" has already been set */
    if (skip_readings_when_resuming_after_detection == 0)
    {
        /* This is the first trigger of a measurement session, let's set SCANPERIOD to the fastest */
        sx9300_enable_fastscan();
        sx9300_enable_conv_done_irq();
        sx9300_disable_doze_mode();

        /* Check whether in last completed measurement, body had been detected */
        if (previous_body_detection)
        {

            /* Body had previously been detected, let's discard the first #SX9300_MAX_IGNORED_SAMPLES samples */
            skip_readings_when_resuming_after_detection = SX9300_MAX_IGNORED_SAMPLES;

            RMLOG("SX9300: Skipping first samples after re-enabling TX (%d)", skip_readings_when_resuming_after_detection);
            
            return;
        }
        else
        {
            skip_readings_when_resuming_after_detection = 1;
        }
    }
    else
    {
        /* As soon as a valid NO-BODY proximity detection occurs, let's stop the skipping of the samples */
        if (end_of_measurement && (sx9300_reg_stat == 0x0))
        {

#ifdef FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

            if (previous_body_detection)
            {
                /* If previously body had been detected, let's perform a compensation for equalising SX9300 internal counters (PROXUSEFUL and PROXAVG) */
                sx9300_manual_compensation();
            }
            else

#endif  // FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

            {
                /* After SX9300 has completed a reading, go back into sleep */
                sx9300_sleep();
                
                /* Let's reset all control variables */
                skip_readings_when_resuming_after_detection = 0;
                sx9300_conv_done_irq_downcounter = 0;
                sx9300_conv_done_irq_enabled = false;
                skip_readings_when_resuming_after_detection = 0;
                
                /* Measurement has been completed */
                measurement_in_progress = false;
                sx9300_stop_timer();

#ifdef SX9300_REG_PROX_CTRL6_VALUE_OUT
            set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE);
#endif

                /* Let's generate the proper proximity event */
                lifesole_set_proximity_gpregret(new_body_proximity);
                lifesole_set_event_scheduler(LS_EVENT_NO_BODY_DETECTED);
            }

            return;

        }

        /* Check whether we still have to discard the sensor readings */
        if(skip_readings_when_resuming_after_detection > 1)
        {
            /* Let's keep discarding the unwanted samples */
            skip_readings_when_resuming_after_detection--;

            return;

        }
        else    // skip_readings_when_resuming_after_detection = 1!
        {
            /* Let's decrement the counter for the valid readings */
            if (sx9300_conv_done_irq_enabled && conversion_done) {
                if (sx9300_conv_done_irq_downcounter > 0)
                {
                    sx9300_conv_done_irq_downcounter--;
                }
            }

        }
    }

    /* Let's stop the measurement as soon as a valid proximity has been detected or when we have reached the maximum number of valid conversions per measurement */
    if ((end_of_measurement && valid_detection) || (sx9300_conv_done_irq_downcounter < 1))
    {
        
        /* Proximity measurement has been completed! */
        RMLOG("+");

        /*
         * Check whether at least 1 sensor has detected BODY proximity or
         * if an OBJECT has been detected while the result from last measurement was NO PROXIMITY,
         * in this last case we assume that the body had not beed correctly been detected.
         * TODO: Sometimes after proximity has been detected (either Body or Object), at following
         *       measurement the sensor is stuck to previous reading and cannot detect change in proximity.
         */
        if (new_body_proximity || new_partial_body_prox) // || (!previous_body_detection && (sx9300_reg_stat > 0x0)) )
        {
            RMLOG("SX9300: Body detected");

            /* Set the event to be generated */
            new_prox_event = LS_EVENT_BODY_DETECTED;
            new_body_proximity = true;

#if defined USING_ROAM_ELECTRODE_V2 //|| defined USING_ROAM_ELECTRODE_V1

            //if (!previous_body_detection)
            {
                uint16_t proxtresh_values[32] = {     0,   20,   40,   60,   80,  100,  120,  140,
                                                    160,  180,  200,  220,  240,  260,  280,  300,
                                                    350,  400,  450,  500,  600,  700,  800,  900,
                                                   1000, 1100, 1200, 1300, 1400, 1500, 1600, 1800 
                                                };

                unsigned char sensor_data[4][10];
                int16_t diff0, diff1, diff;

                sx9300_read_all_sensors(sensor_data);

                if (!new_partial_body_prox)
                {
                    diff0 = ((sensor_data[1][4] & 0xF0)? 0xF000 : 0x0000) | ((sensor_data[1][4]<<8) + sensor_data[1][5]);
                    diff1 = ((sensor_data[3][4] & 0xF0)? 0xF000 : 0x0000) | ((sensor_data[3][4]<<8) + sensor_data[3][5]);
                    RMLOG("SX9300: PROXDIFF-0B 0x%04X, PROXDIFF-1B 0x%04X", diff0, diff1);
                    diff = (diff0 < diff1)? diff0 : diff1;
                }
                else
                {
                    int electrode =  (sx9300_reg_stat == 0x3)? 1 : 3;
                    diff = ((sensor_data[electrode][4] & 0xF0)? 0xF000 : 0x0000) | ((sensor_data[electrode][4]<<8) + sensor_data[electrode][5]);
                    
                    RMLOG("SX9300: PROXDIFF-0%c 0x%04X", electrode==1?'A':'B', diff);
                }

                RMLOG("SX9300: smaller PROXDIFF 0x%04X", diff);

                uint8_t prox_tresh_index = 0xFF;

                if (diff > 0x20 + 0xE0 + 0x44)
                {
                    diff -= 0xE0;
                    int16_t delta0 = 0x0FFF, delta1 = 0x0FFF;

                    for (int k=0; k<32; k++)
                    {
                        if (diff < proxtresh_values[k])
                        {                            
                            delta0 = diff - proxtresh_values[k-1];
                            delta1 = proxtresh_values[k] - diff;
                            prox_tresh_index = (delta0 < delta1)? k-1 : k;
                            RMLOG("SX9300: closer PROXTRESH values %d and %d", delta0, delta1);
                            break;
                        }
                    }
                    
                    if (prox_tresh_index != 0xFF)
                    {                         
                        RMLOG("SX9300: setting SX9300_REG_PROX_CTRL6 to 0x%02X", prox_tresh_index);
                        //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, prox_tresh_index);
                    }
                    else
                    {
                        prox_tresh_index = 31;
                        RMLOG("SX9300: PROXDIFF too big -> set SX9300_REG_PROX_CTRL6 to max  0x%02X", prox_tresh_index);
                        //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, prox_tresh_index);    
                    }
                }
                else
                {
                    prox_tresh_index = SX9300_REG_PROX_CTRL6_VALUE_OUT;                    
                    RMLOG("SX9300: PROXDIFF too small -> default SX9300_REG_PROX_CTRL6_VALUE_OUT 0x%02X", prox_tresh_index);
                    //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE_OUT);
                }

                uint8_t prox_ctrl6;
                i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, 1, &prox_ctrl6);
                if (prox_ctrl6 < prox_tresh_index)
                {
                    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, prox_tresh_index);
                }
                else
                {
                    RMLOG("SX9300: previous SX9300_REG_PROX_CTRL6 (0x%02X) bigger than/equal to the new one! Not changing it!", prox_ctrl6);
                }

            }

#elif defined SX9300_REG_PROX_CTRL6_VALUE_OUT

            set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE_OUT);

#endif  // defined USING_ROAM_ELECTRODE_V2 || defined USING_ROAM_ELECTRODE_V1

        }
        else
        {
            RMLOG("SX9300: Body NOT detected");

#ifdef FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

            if (!valid_detection) // || previous_body_detection)
            {   
                /* In case the proximity detection was uncertain or it is a good chance to perform one, let's trigger a compensation */
                sx9300_manual_compensation();
                
                return;

            }
            else

#endif  // FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

            {
                /* Set the event to be generated */
                new_prox_event = LS_EVENT_NO_BODY_DETECTED;
                new_body_proximity = false;

#ifdef SX9300_REG_PROX_CTRL6_VALUE_OUT
            set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE);
#endif

            }
            
        }

        /* Measurement has been completed */
        measurement_in_progress = false;   
        sx9300_stop_timer();
        
        /* Time to generate an event */
        lifesole_set_proximity_gpregret(new_body_proximity);
        lifesole_set_event_scheduler(new_prox_event);
        
        /* After SX9300 has completed a reading, go back into sleep */
        sx9300_sleep();

        /* Let's reset the relevant control variables */
        skip_readings_when_resuming_after_detection = 0;
        sx9300_conv_done_irq_enabled = false;
        sx9300_conv_done_irq_downcounter = 0;
        
    }
    else
    {
        /* Nothing to do: let's just discard the actual reading */
    }

    RMLOG("\n");

}


/*
 * Emulate a SW interrupt by running the Scheduler function for the IRQ Handler
 */

void sx9300_sw_irq_emulator(void)
{
    sx9300_irq_scheduler(SEMTECH_INTERRUPT, NRF_GPIOTE_POLARITY_HITOLO);
}


#if defined SX9300_FPC_TEST || defined SX9300_TEST_MODE

/*
 * Debugging function for logging information about electrical/sampled values for each of the 4 electrodes
 * WARNING: it may cause the I2C interface of sensor to get into BUSY state in case this function is used at a fast pace (e.g. @30ms)
 */

void sx9300_print_fpc_readings(void)
{
    unsigned char sensor_data[4][10];
    char reg_names[5][18]={"PROXUSEFUL", "PROXAVG", "PROXDIFF", "PROXOFFSET", "SARDELTA-SARRATIO"};
    char electrode_names[4][5]={"CS0A", "CS0B", "CS1A", "CS1B"};
    static bool proximity_detected = false; // triggers a compensation

//    if (proximity_detected)
//    {

        sx9300_read_all_sensors(sensor_data);
    
        uint8_t reg_stat = sx9300_release_irq_line();

        //RMLOG("");
        for (int i=0; i<4; i++)
        {
            //if (/*reg_stat == 0 &&*/  (i==0 || i== 2))   break;

            RMLOG("\nElectrode %s", electrode_names[i]);
            for (int k=0; k<5; k++)
            {
                //if ( /*proximity_detected ||*/ (k==2 && i%2==1) )
                //if (k <= 2)
                    RMLOG("Reg[%d:%d] = 0x%02X-%02X = %s", k*2, 1+k*2, sensor_data[i][k*2], sensor_data[i][1+k*2], reg_names[k]);
#if 0
                    if (k==2)
                    {
                        uint16_t tmpdata;
                        tmpdata = ((sensor_data[i][k*2] << 8) + sensor_data[i][1+k*2]) >> 6;
                        RMLOG("Reg[%d:%d] = 0x%04X = %s", k*2, 1+k*2, tmpdata, reg_names[k]);
                    }
#endif
            }

            if (i%2 == 1)
            {
                unsigned short diff_a = (sensor_data[i-1][4] << 8) + sensor_data[i-1][5];
                unsigned short diff_b = (sensor_data[i  ][4] << 8) + sensor_data[i  ][5];
                unsigned char  ratio  = (diff_a << 6) / diff_b;
                unsigned char  delta  = (diff_a - diff_b) >> 5;
                if ( ( (diff_a - diff_b) >> 4) % 2 ) delta++;

                RMLOG ("SARDELTA-SARRATIO = 0x%02X-0x%02X", delta, ratio);
            }
        }

//    }

    RMLOG("");


    if ( reg_stat > 0x0)
    {   
        if (!proximity_detected)
        {

#ifdef SX9300_REG_PROX_CTRL6_VALUE_OUT
            if (((reg_stat & 0xC) == 0xC) || ((reg_stat & 0x3) == 0x3))
                set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE_OUT);
#endif

            proximity_detected = true;
        }
    }
    else
    {

        if (proximity_detected)
        {

#ifdef SX9300_REG_PROX_CTRL6_VALUE_OUT
            set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL6, SX9300_REG_PROX_CTRL6_VALUE);
#endif

            //sx9300_manual_compensation();
            proximity_detected = false;
        }
    }
}

#endif // defined SX9300_FPC_TEST || defined SX9300_TEST_MODE

/*
 * SX9300 IRQ Event Scheduler
 */

static void sx9300_irq_scheduler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{

    nrf_drv_gpiote_in_event_disable(SEMTECH_INTERRUPT);


//#if defined SX9300_FPC_TEST && defined SX9300_TEST_MODE
#ifdef SX9300_TEST_MODE

    /*
     *  This routine is used for testing the new FPC and print out the Sensor Data Readback Registers for each of the 4 electrodes
     */

     app_sched_event_put(NULL, 0, (app_sched_event_handler_t) sx9300_print_fpc_readings);
     //sx9300_print_fpc_readings();
     //sx9300_release_irq_line();


#else

    /*
     *  When an interrupt is detected, it schedules a run of the IRQ Handler, unless the sensor is not enabled
     */

    if (sx9300_get_power_status() && sx9300_get_txen_status())
    {
        sx9300_irq_counter++;
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t) sx9300_irq_handler);

#ifdef SX9300_FPC_TEST
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t) sx9300_print_fpc_readings);
#endif

    }
    else
    {
        RMLOG("\nSX9300 interrupt ignored because either SX9300 is OFF or TXEN is not set!");
        if (sx9300_get_power_status())
        {
            sx9300_release_irq_line();
        }
    }

#endif  // defined SX9300_FPC_TEST && defined SX9300_TEST_MODE


    nrf_drv_gpiote_in_event_enable(SEMTECH_INTERRUPT, true);

}


/*
 * Register an interrupt handler to call on an active low transition
 */

void sx9300_register_irq(void)
{

    uint32_t err_code;
    
    RMLOG("SX9300: Registering Interrupt");
    
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
    /* SX9300 sensor requires an external pullup for the Interrupt pin */
    in_config.pull = NRF_GPIO_PIN_PULLUP;    
   
    err_code = nrf_drv_gpiote_in_init(SEMTECH_INTERRUPT, &in_config, sx9300_irq_scheduler);
    APP_ERROR_CHECK(err_code);
        
    /* In System OFF mode no interrupt is detected unless we "force" the SENSE field to be set */
    nrf_gpio_cfg_sense_input(SEMTECH_INTERRUPT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
    nrf_drv_gpiote_in_event_enable(SEMTECH_INTERRUPT, true);

    sx9300_irq_registered = true;

    RMLOG("SX9300: Interrupt registered for SX9300");

}


/*
 * Triggers manual comp
 */

#if defined FORCE_COMPENSATION_ON_LOSS_OF_PROXIMITY

static void sx9300_manual_compensation(void)
{

    RMLOG("SX9300: Triggering manual compensation");
 
    waiting_for_compensation = true;

    /* Force a Manual Compensation */
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 0x10);

}

#endif

/*
 * Enable the sensor to generate SX9300_MAX_CONSEC_CONV_DONE number of Conversion Done interrupts 
 */

void sx9300_enable_conv_done_irq(void)
{

    RMLOG("SX9300: Enabling CONVERSION DONE interrupt");

    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_MSK, SX9300_REG_IRQ_MSK_VALUE);

    sx9300_conv_done_irq_enabled = true;

    sx9300_conv_done_irq_downcounter = SX9300_MAX_CONSEC_CONV_DONE;

}


/*
 * Initialises the proximity sensor.
 */

void sx9300_init(void)
{

    if ( !sx9300_get_power_status() ) {

        RMLOG("SX9300: Not yet powered, proceeding with complete initialisation procedure...");

        /* Power it on and get it kicked */
        sx9300_power_on();
        
        if (sx9300_check()) {

            /* Power saving: unitialise the I2C interface */
            //twi_master_uninit();
            
            /* Soft reset and set defaults for the proximity sensor */
            sx9300_reset();

            sx9300_init_timer();

            /* Enable the proximity sensing functionalities */
            sx9300_wake();

            /* Trigger manual compensation */
            /* It is already performend at POR */
            //sx9300_manual_compensation();
            
        }
        else
        {
            RMLOG("SX9300: Problems in initializing SX9300: powering sensor off!");

            /* Power SX9300 off */
            sx9300_power_off();
        }

    }
    else
    {
        
        RMLOG("SX9300: Already powered, proceeding with partial initialising...");

        /* Initialise the I2C interface */
        nrf_gpio_pin_set(SEMTECH_SVDD);
        twi_master_init();

        /* Enable the interrupt handler */
        if (!sx9300_irq_registered)
        {
            sx9300_register_irq();
        }

        sx9300_init_timer();

        RMLOG("SX9300: SX9300 interrupt enabled");
    }

}


/*
 * Put the proximity sensor into sleep mode (proximity sensing disabled)
 */

void sx9300_sleep(void)
{

    RMLOG("SX9300: Sleep the SX9300");
    
    nrf_drv_gpiote_in_event_disable(SEMTECH_INTERRUPT);


#ifdef SX9300_SLEEP_WITH_TXEN
    /* Disable it via the TXEN pin */
    nrf_gpio_pin_clear(SEMTECH_TXEN);
#else
    uint8_t prox_ctrl0;
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, 1, &prox_ctrl0);
    /* DISable all sensing functionalities (actual activity DOES NOT depend on TXEN signal) */
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, prox_ctrl0 & 0xF0);
#endif

}


/*
 * Wakes the proximity sensor from sleep
 */

void sx9300_wake(void)
{
    RMLOG("SX9300: Wake the SX9300");

    if (!sx9300_irq_registered) {
        /* Enable the interrupt handler before awaking the sensor */
        sx9300_register_irq();
    }


#ifdef SX9300_SLEEP_WITH_TXEN

    /* Get SX9300 out of sleep */
    nrf_gpio_pin_set(SEMTECH_TXEN);

#else

    uint8_t prox_ctrl0;
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, 1, &prox_ctrl0);

    /* DISable all sensing functionalities (actual activity DOES NOT depend on TXEN signal) */
#ifdef SX9300_ACTIVE_SENSORS
    RMLOG("sx9300_wake(): SX9300_ACTIVE_SENSORS");
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, (prox_ctrl0 & 0xF0) | SX9300_ACTIVE_SENSORS);
#else 
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, prox_ctrl0 | 0x0F);
#endif // SX9300_ACTIVE_SENSORS

#endif // SX9300_SLEEP_WITH_TXEN

    
    nrf_delay_ms(1);
    nrf_drv_gpiote_in_event_enable(SEMTECH_INTERRUPT, true);
}


/*
 * Checks the proximity state of two sensors (lower nibble matching the doc for the chip (just shifted)
 *  Bit 7 : CS1PROXSTAT (Detected)
 *  Bit 6 : Body
 *  Bit 5 : CS0PROXSTAT (Detected)
 *  Bit 4 : Body
 *
 */

static uint8_t sx9300_check_state(uint8_t irq_src)
{

    uint8_t result;
    bool tx_enabled = sx9300_get_txen_status();
    bool sx9300_powered = sx9300_get_power_status();


    if (!tx_enabled || !sx9300_powered) {
        RMLOG("SX9300: sx9300_check_state(): is sensor disabled");
        return 0x0F;
    }

    /* If reset has happened at runtime, let's reconfigure the sensor */
    if (irq_src & 0x80)
    {
        RMLOG("WARNING: SX9300 has been RESET for unknown reasons! SX9300 will be reconfigured now.");

        /* Soft reset and set defaults for the proximity sensor */
        sx9300_reset();

        /* Reset function clears the TXEN GPIO pin */
        nrf_gpio_pin_set(SEMTECH_TXEN);
        nrf_delay_ms(1);

        /* Invalid reading due to reset condition */
        result = 0x0F;
    }
    else
    {
        /* Read the status register */
        if (i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_STAT, 1, &result) != 0)
        {
            RMLOG("SX9300: Error reading I2C");
            result = 0x0F;
        }
        else
        {
            RMLOG("SX9300: SX9300 irqSrc:status regs [0x%02X]:[0x%02X]", irq_src, result);

#if 0
#if defined SX9300_FPC_TEST && defined USING_ROAM_ELECTRODE_V2

        sx9300_print_fpc_readings();

#endif
#endif

            // Tidy up the return so it can have direct comparison
            result &= 0xF0;
        }
    }
    
    return result;

}


/*
 * Enable fast-scan mode by selecting low value (default: 30ms) for SCANPERIOD
 */

//static void sx9300_enable_fastscan(void)
void sx9300_enable_fastscan(void)
{

    RMLOG("SX9300: Enabling FASTSCAN");

    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, SX9300_REG_PROX_CTRL0_VALUE);

}


/*
 * Enable slow-scan mode by selecting a high value (default: 400ms) for SCANPERIOD
 */

static void sx9300_enable_slowscan(void)
{

    RMLOG("SX9300: Enabling SLOWSCAN");

    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, SX9300_REG_PROX_CTRL0_SLOW);

}


/*
 * Enable Doze Mode. Overall scan-period = SCANPERIOD x DOZEPERIOD.
 */

void sx9300_enable_doze_mode(void)
{

    RMLOG("SX9300: Enabling DOZE MODE");

    /* Enable Doze mode and set DOZEPERIOD to 16x SCANPERIOD = 6.4 seconds */
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL3, 0x40 | SX9300_REG_PROX_CTRL3_VALUE);   

}


/*
 * Disable Doze Mode. Overall scan-period = SCANPERIOD.
 */

static void sx9300_disable_doze_mode(void)
{

    RMLOG("SX9300: Disabling DOZE MODE");

    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL3, SX9300_REG_PROX_CTRL3_VALUE);

}


/*
 * Put the SX9300 into slow speed scanning & interrupt mode
 */

void sx9300_start_slowscan(void)
{

    RMLOG("SX9300: Starting SLOWSCAN");

    /* Set SCANPERIOD to slowest: 400ms */
    sx9300_enable_slowscan();
    
    /* Enable Doze mode and set DOZEPERIOD to 16x SCANPERIOD = 6.4 seconds */
    sx9300_enable_doze_mode();

    /* Disable Compensation and Conversion Done interrupts */
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_MSK, SX9300_REG_IRQ_MSK_DOZE_VALUE);

    /* Set TXEN and enable the interrupt handler */
    sx9300_wake();

}


/*
 * Stop the SX9300 from its slow scan.
 */

void sx9300_stop_slowscan(void)
{

    RMLOG("SX9300: Stopping SLOWSCAN");

    /* Reset SCANPERIO to 30ms */
    sx9300_enable_fastscan();
    /* Reset Doze parameters to default: Doze disabled, and Doze scan period 2xSCANPERIOD */
    sx9300_disable_doze_mode();

    /* Disable Compensation and Conversion Done interrupts */
    set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_MSK, SX9300_REG_IRQ_MSK_DOZE_VALUE);
    
}


/*
 * Return the power status of SX9300 by checking GPIO pin
 */
bool sx9300_get_power_status(void)
{
    return (NRF_GPIO->OUT & (1<<SEMTECH_POWER)) ? true : false;
}


/*
 * Return the power status of SX9300 by checking GPIO pin
 */
bool sx9300_get_i2c_power_status(void)
{
    return (NRF_GPIO->OUT & (1<<SEMTECH_SVDD)) ? true : false;
}


/*
 * Return the TXEN status of SX9300 by checking GPIO pin
 */
bool sx9300_get_txen_status(void)
{

#ifdef SX9300_SLEEP_WITH_TXEN    

    return (NRF_GPIO->OUT & (1<<SEMTECH_TXEN)) ? true : false;

#else

    uint8_t reg_value;
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, 1, &reg_value);
    return (reg_value & 0x0F);

#endif

}


/*
 * Function used at initialisation time for checking i2c configuration and hardware faults
 */

static bool sx9300_check(void)
{
    uint8_t result;
    int err_code;

    /* Lets check the only few regs we can know on reset */
    err_code = i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, 1, &result);

    if (err_code || ((result & 0x0F) != 0x0F))
    {
        RMLOG("SX9300: Error (0x%02X) detecting SX9300: register 0x%02X = 0x%02X", err_code, SX9300_REG_IRQ_SRC, result);

        return false;
    }

    /* Need to read the reg to clear RESETIRQ flag */
    err_code = i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 1, &result);

    if (err_code)
    {
        RMLOG("SX9300: Error (0x%02X) reading I2C: register 0x%02X = 0x%02X", err_code, SX9300_REG_IRQ_SRC, result);
        return false;
    }
    else
    {
        RMLOG("SX9300: sx9300_check(): Reading I2C: register SX9300_REG_IRQ_SRC = 0x%02X", result);
    }

    // All good
    RMLOG("SX9300: SX9300 detected and reset");

    return true;
}


/*
 * Release the interrupt line, freeing SX9300 and allowing the SX9300 to generate new interrupts
 */

static uint8_t sx9300_release_irq_line(void)
{
    uint8_t irq_src;
    uint8_t stat = 0x0;

    /* In case it has not been initialised yet, init TWI */
    twi_master_init();    
    
//#ifdef SX9300_FPC_TEST
#ifdef SX9300_TEST_MODE

    /* When testing a new FPC, let's print out the status information */

    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_STAT, 1, &stat);
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 1, &irq_src);

    RMLOG("\n\nSX9300: IRQ_SRC:STAT regs = 0x%02X:0x%02X", irq_src, stat);

#else

    /* By reading the IRQ SRC registers, SX9300 will release the IRQ line */
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 1, &irq_src);

#endif

    return (stat >> 4);

}


/*
 * Check whether the SX9300 driver is performing a measurement
 */

bool sx9300_measurement_in_progress(void)
{
    RMLOG("SX9300:%s measurement in progress!", measurement_in_progress?"":" NO");
    return measurement_in_progress;
}


/*
 * Initialize the timer for instantaneous proximity check
 */

static void sx9300_init_timer(void)
{

    uint32_t err_code;

    RMLOG("SX9300: Creating timer");
    
    err_code = app_timer_create(&m_sx9300_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                sx9300_timeout_handler);
    APP_ERROR_CHECK(err_code);

}


/*
 * Set a timer for a period of seconds
 */

static void sx9300_set_timer(uint32_t seconds)
{
    RMLOG("SX9300: Starting single shot timer for %u seconds\n\n",seconds);
    timer_started = true;
    app_timer_start(m_sx9300_timer_id, APP_TIMER_TICKS((seconds * 1000), APP_TIMER_PRESCALER), NULL);
}


/*
 * Stops a timer
 */

static void sx9300_stop_timer(void)
{
    RMLOG("SX9300: Stopping timer");
    app_timer_stop(m_sx9300_timer_id);
    timer_started = false;
}


static void sx9300_timeout_handler(void * p_context)
{
    nrf_drv_gpiote_in_event_disable(SEMTECH_INTERRUPT);

    RMLOG("SX9300: sx9300_timeout_handler()");

    timer_started = false;
    
    /* Triggers a driver reset */
    force_reset_drv_param = true;
    sx9300_irq_handler();

    /* Let's emulate a factory restart */ 
    lifesole_set_event_scheduler(LS_EVENT_FACTORY_RESTART);
}


#if 0 // Not used

/*
 * Check if SX9300 is stuck with IRQ line LOW waiting for an interrupt to be serviced
 */

static bool sx9300_get_irq_status(bool service_interrupt)
{
    bool irq_status = (nrf_gpio_pin_read(SEMTECH_INTERRUPT) ? false : true) && sx9300_get_power_status();
    

    if (irq_status) {
        RMLOG("SX9300: interrupt line still set...\n");

        if (service_interrupt)
        {
            sx9300_sw_irq_emulator();
        }
        else
        {
            sx9300_release_irq_line();
        }
    }
    else 
    {
        RMLOG("SX9300: No interrupt pending...\n");
    }

    return irq_status;
}

#endif


/******************************************************************
 *
 *      Functions used for testing
 *
 ******************************************************************/


#if defined SX9300_FPC_TEST || defined SX9300_TEST_MODE || defined USING_ROAM_ELECTRODE_V2 //|| defined USING_ROAM_ELECTRODE_V1

static void sx9300_read_all_sensors(unsigned char sensor_data[4][10])
{
    if (sensor_data == NULL)
    {
        RMLOG("SX9300: sx9300_read_all_sensors(): sensor_data is NULL)");
        return;
    }
        
    
    for (unsigned char i=0; i<4; i++)
    {
        set_register(SEMTECH_SX9300_ADDR, SX9300_REG_SENSOR_SEL, i);
        if (!i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_USE_MSB, 10, sensor_data[i]))
        {
            for(int k=0; k<10; k++)
            {
                i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_USE_MSB+k, 1, &(sensor_data[i][k]));
            }
        }
    }
}

#endif


#ifdef SX9300_TEST_MODE

uint8_t sx9300_check_state_test(void)
{
    uint8_t result, irq_src;

    // Release the interrupt line
    i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_SRC, 1, &irq_src);

    // Read the status register     
    if (i2c_read(SEMTECH_SX9300_ADDR, SX9300_REG_STAT, 1, &result)) {
        RMLOG("Error reading I2C");
        result = 0x0F;
    }
    else {
        //RMLOG("SX9300 [irq src]:[status] reg is [0x%02X]:[0x%02X]", irq_src, (result & 0xF0));
        RMLOG("[0x%02X]:[0x%02X]", irq_src, (result & 0xF0));
        // Tidy up the return so it can have direct comparison
        result &= 0xF0;
    }

    return result;
}


void sx9300_read_all_reg(void)
{
    unsigned char result, reg_addr;
    for (reg_addr = 0; reg_addr < 0x20; reg_addr++) {
        i2c_read(SEMTECH_SX9300_ADDR, reg_addr, 1, &result);
        RMLOG("SX9300 Reg[0x%02X] = 0x%02X", reg_addr, result);
    }
}

bool lifesole_early_latch_check(void)
{
    uint32_t latch = NRF_P0->LATCH;

    if (latch & (1 << SEMTECH_INTERRUPT))
    {    
        if (sx9300_get_power_status())
        {
            RMLOG("SX9300: Early check: SX9300 fired!");
            // Initialise the I2C interface
            twi_master_init();
            
            sx9300_irq_handler();
            sx9300_wake();
            
            return true;
        }
        else
        {
            RMLOG("SX9300: Early check: could have been a previous interrupt that has not been cleared!");
        }
    }

    return false;
}


void sx9300_test(void)
{
    uint32_t err_code;
    unsigned int count=0;

#ifdef SX9300_TEST_MODE
    RMLOG("Testing the SX9300");
#else
    RMLOG("Flag not set for Testing SX9300");
#endif

    if ((*(uint32_t *)0x1000120C) & 1) {
        RMLOG("UICR: pins 9 and 10 set as NFC Antenna!");
    }
    else {
        RMLOG("UICR pins 9 and 10 set as GPIOs!");
    }
/*
    if (lifesole_early_latch_check())
    {
        RMLOG("SX9300: trigger detected after System OFF");
    }
    else
*/

    if(!sx9300_get_power_status())
    {
        
        /* Power on and configure the proximity sensor with default parameters (@30ms) */
        sx9300_init();
        /* Enforcing fastscan in case we come from another state which has enabled slowscan */
        //sx9300_enable_slowscan();
        //sx9300_enable_fastscan();
        /* Let's enforce that we want to wake the sensor in case SX9300 was kept powered but asleep */
        //sx9300_wake();
        //sx9300_enable_conv_done_irq();



        //sx9300_init();

        //sx9300_enable_conv_done_irq();

        //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, SX9300_REG_PROX_CTRL0_VALUE);

/*
        do
        {
*/
            RMLOG("Waiting for compensation to be completed!");
            sx9300_manual_compensation();
            nrf_delay_ms(10);
            sx9300_manual_compensation();

/*            
            err_code = sd_app_evt_wait();
            APP_ERROR_CHECK(err_code);

            app_sched_execute();
        }while (waiting_for_compensation);

//        sx9300_start_slowscan();
//        //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_IRQ_MSK, SX9300_REG_IRQ_MSK_DOZE_VALUE); // | SX9300_IRQ_MSK_COMP_DONE);
        sx9300_wake();        
*/

        //sx9300_wake();

    }
    else
    {
        sx9300_init();
        //sx9300_enable_conv_done_irq();

        //set_register(SEMTECH_SX9300_ADDR, SX9300_REG_PROX_CTRL0, SX9300_REG_PROX_CTRL0_VALUE);

        //sx9300_irq_scheduler(SEMTECH_INTERRUPT, NRF_GPIOTE_POLARITY_HITOLO);

        sx9300_sw_irq_emulator();
        //sx9300_wake();
    }

    

    int counter = 0; //, tx_disable_cnt = 0;

    while(1) {  
        count++;
        RMLOG("INTERRUPT NUM %d", count);
        app_sched_execute();

/*      
        RMLOG("***********************************************************************************\n\n");

        if (!sx9300_get_txen_status())
        {
            tx_disable_cnt++;
            if (tx_disable_cnt == 10)
            {
                sx9300_wake();
                RMLOG("\nGoing into SYSTEM OFF: Doze Mode @ 6.4s");
                RMLOG("\n===================================================================================\n\n");
                NRF_P0->LATCH = 0xFFFFFFFF;

                // Power off (will wake from interrupts)
                sd_power_system_off();
            }
            else
            {
                RMLOG("\nNext proximity measurement in 5 seconds (tx_disable_cnt = %d)...\n", tx_disable_cnt);
                nrf_delay_ms(5000);

                if ((tx_disable_cnt%3)==0) {
                    sx9300_wake();
                    sx9300_enable_conv_done_irq();
                    //sx9300_wake();
                }
                else
                {                   
                    //sx9300_wake();
                    sx9300_start_slowscan();
                }
            }
        }
*/
        //sx9300_release_irq_line();
        err_code = sd_app_evt_wait();
        APP_ERROR_CHECK(err_code);

        counter++;
    }
}

#endif // SX9300_TEST_MODE

