/* 
 * Lifesole application
 * --------------------
 *
 * Author: Chris Moore
 * 
 * LifeSole 
 *
 * (c) Roam Creative Ltd 2017
 *
 */

#ifndef LIFESOLE_H__
#define LIFESOLE_H__


// --------------------------- DEFINES ----------------------------------

#define LIFESOLE_ACTIVITY_SECONDS       0
#define LIFESOLE_INACTIVITY_INIT_TIME   2
#define LIFESOLE_INACTIVITY_SECONDS     30              // !! MUST BE GREAT THAN LIFESOLE_CONFIG_SECONDS - How long before going back to sleep ...

#define LIFESOLE_SAFETY_INACTIVITY_SECONDS  10          // How long inactive when someones wearing it

#define LIFESOLE_CONFIG_SECONDS         5           // Advertise "foot detected" and connectable for 5 seconds after foot in...
#define LIFESOLE_CONFIG_SESSION_SECONDS 30
#define LIFESOLE_ALERT_SECONDS          15          // Advertise "warning" (foot detected) and connectable for 15 seconds
                                    
#define LIFESOLE_GPREGRET_STATE_POS     2                                       // The 2 LSBs are reserverd for the bootloader to check if Roam Reset bits are set 
#define LIFESOLE_GPREGRET_STATE_MASK    (0x1F << LIFESOLE_GPREGRET_STATE_POS)   // 5 middle bits are used for storing proximity status 
#define LIFESOLE_GPREGRET_PROX_POS      7
#define LIFESOLE_GPREGRET_PROX_MASK     (1 << LIFESOLE_GPREGRET_PROX_POS)       // MSB is used to store proximity status


// --------------------------- CONSTANTS ----------------------------------



// --------------------------- ENUM TYPES ----------------------------------

/*
 * If you modify these (especially the order.. don't mess with the order) you will need to change the following
 *  - state_names (in lifesole.c)
 *  - lifesole_handlers (in lifesole.c)
 *  - lifesole_tran (in lifesole.c)
 */

typedef enum {

    LS_STATE_INVALID = 0,

    LS_STATE_FIRST_TIME_USE,
    LS_STATE_WAITING_MOTION,
    LS_STATE_MOTION_DETECTED,
    LS_STATE_MOTION_DELAY_CHECK,
    LS_STATE_BODY_DETECTED,
    LS_STATE_WAITING_NO_MOTION,
    LS_STATE_NO_MOTION_DETECTED,
    LS_STATE_ALERT,
    LS_STATE_ALERT_FINISHED,
    LS_STATE_ENTER_CONFIGURATION,
    LS_STATE_EXIT_CONFIGURATION,

    LS_STATE_MAX

} lifesole_state_t;


/*
 * If you modify these (especially the order.. don't mess with the order) you will need to change the following
 *  - event_names (in lifesole.c)
 *  - lifesole_tran (in lifesole.c)
 */

typedef enum {

    LS_EVENT_NO_EVENT = 0,

    LS_EVENT_FACTORY_RESTART,
    LS_EVENT_WATCHDOG_RESTART,
    LS_EVENT_BROWNOUT_RESTART,
    LS_EVENT_UPGRADE_RESTART,
    LS_EVENT_TIMER_EXPIRED,
    
    LS_EVENT_MOTION_CONFIGURE,
    
    LS_EVENT_MOTION_TRIGGERED,
    LS_EVENT_NO_MOTION_TRIGGERED,
    LS_EVENT_BODY_DETECTED,
    LS_EVENT_NO_BODY_DETECTED,
    LS_EVENT_ENTER_CONFIG_MODE, 
    LS_EVENT_EXIT_CONFIG_MODE,

    LS_EVENT_ALERT_FIRED,

    LS_EVENT_MAX

} lifesole_event_t;


// ------------------------------ STRUCT TYPES ----------------------------------

typedef struct {

    lifesole_state_t     state;
    lifesole_event_t     next_event;    

} lifesole_instance_t;


// This struct is only used in the lifesole_transition[actual status][fired event] matrix.
// It was redundant having the fired event referenced by both the second index of the matrix and the struc variable.
// event_priority can be used for prioritasing transitions in case of concurrent events.  
typedef struct {
    
    uint8_t             event_priority;
    lifesole_state_t    next_state;

} lifesole_transition_t;


typedef void (*lifesole_handler_t)(void);


// --------------------------- Functions ----------------------------------



void lifesole_init(uint32_t reset_reason);
void lifesole_process(void);

void lifesole_set_event_scheduler(int8_t event);
void lifesole_set_state(int8_t state);

uint8_t lifesole_get_sleep(void);
void lifesole_sleep(void);

// Functions for managing proximity detection information in the GPREGRET register shared together with LifeSole State Machine status
void lifesole_set_proximity_gpregret(bool prox);
bool lifesole_get_proximity_gpregret(void);

#endif // LIFESOLE_H__

/** @} */
