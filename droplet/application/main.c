/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * This is the firmware for Roam Creative Ltd iBeacon implementation with various enhancements
 *  to aid battery life and detection.
 *
 * Memory Errors (0x0000 0004)
 *
 * These are caused by not enough memory, you need to adjust these things
 *   ble_enable_params.common_enable_params.vs_uuid_count = VENDOR_SPECIFIC_UUID_COUNT;
 *   ble_enable_params.gatts_enable_params.attr_tab_size = 0x580 + 0x200;
 *   application.ld
 */
 
/*
 * (c) Roam Creative Ltd 2015
 *  
 * Licensing: You need to have a full source code license to utilise this or any of the associated source
 *             files in this project in your own product or derived product.
 *
 *            Contact hello@roamltd.com to discuss licensing terms for hardware, software, firmware and SDKS.
 *
 */


#include <stdbool.h>
#include <stdint.h>
#include "ble_conn_params.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "pstorage.h"
#include "app_button.h"
#include "app_timer.h"
#include "ble_advertising.h"
#include "ble_bas.h"
#include "ble_dis.h"
#include "ble_roam.h"
#include "battery.h"
#include "droplet.h"
#include "logger.h"
#include "simple_uart.h"
#include "bsp.h"
#include "nrf_nvmc.h"
#include "nrf_drv_gpiote.h"
#include "rmcs.h"
#include "settings.h"
#include "app_scheduler.h"
#include "nrf_delay.h"
#include "sx9300.h"
#include "ADXL362.h"
#include "lifesole.h"
#include "nrf_nvic.h"
#include "nrf_drv_saadc.h"
#include "ble_conn_state.h"


/*****************************************************************************************************************
 *
 *                                    Settings that affect memory (change application.ld)
 *
 *****************************************************************************************************************/

#define IS_SRVC_CHANGED_CHARACT_PRESENT  0      /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/


#define CENTRAL_LINK_COUNT              0                                                   /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT           1                                                   /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

/* !!!! IF YOU HAVE STRANGE ERROR 4 or 8 YOU ARE OUT OF MEMORY. ADJUST APPLICATION.LD AND THE MEMORY ALLOCATED TO THE SD !!!!!*/
#define VENDOR_SPECIFIC_UUID_COUNT      4                                                   /**< The number of vendor specific UUIDs used by this example. */


/*****************************************************************************************************************
 *
 *                                    Defines
 *
 *****************************************************************************************************************/

#define MAJ_VAL_OFFSET_IN_BEACON_INFO   18                                /**< Position of the MSB of the Major Value in m_beacon_info array. */
#define APP_BEACON_INFO_LENGTH          0x17                              /**< Total length of information advertised by the Beacon. */
#define APP_ADV_DATA_LENGTH             0x15                              /**< Length of manufacturer specific data in the advertisement. */
#define APP_DEVICE_TYPE                 0x02                              /**< 0x02 refers to Beacon. */
#define APP_MEASURED_RSSI               0xC3                              /**< The Beacon's measured RSSI at 1 meter distance in dBm. */
#define APP_COMPANY_IDENTIFIER          0x0059                            /**< Company identifier for Nordic Semiconductor ASA. as per www.bluetooth.org. */
#define APP_MAJOR_VALUE                 0x01, 0x02                        /**< Major value used to identify Beacons. */ 
#define APP_MINOR_VALUE                 0x03, 0x04                        /**< Minor value used to identify Beacons. */ 
#define APP_BEACON_UUID                 0x99, 0x12, 0x23, 0x34, \
                                        0x45, 0x56, 0x67, 0x78, \
                                        0x89, 0x9a, 0xab, 0xbc, \
                                        0xcd, 0xde, 0xef, 0x99            /**< Proprietary UUID for Beacon. */

#define MAX_RESET_REASONS               POWER_RESETREAS_NFC_Pos + 3       /**< Max index value for the reasons causing nRF52 to RESET */
#define POWER_RESETREAS_POR_BROWNOUT    0

#define ADVERTISING_MAJOR_VALUE         0x1234
/*
 * Minor Value used during advertising.
 * The following values are used during 
 * 0x5677 (Nordic DevKit), 0x5678 (Semtech FPC),  0x5679 (Roam FPC v2),  0x567A (Gurvan's BLE Test),  0x567B (Roam FPC v1)
 */
#define ADVERTISING_MINOR_VALUE         0x5678



/*****************************************************************************************************************
 *
 *                                    Constants
 *
 *****************************************************************************************************************/

#ifdef DROPLET_USE_UART_LOGGING

static char reset_reason_strings[MAX_RESET_REASONS][35] = 
{
    [POWER_RESETREAS_POR_BROWNOUT]    = "RESET-ON-POWER OR BROWNOUT",
    [POWER_RESETREAS_RESETPIN_Pos+1]  = "RESET-PIN TRIGGERED",
    [POWER_RESETREAS_DOG_Pos+1]       = "WATCHDOG FIRED",
    [POWER_RESETREAS_SREQ_Pos+1]      = "SOFTRESET DETECTED",
    [POWER_RESETREAS_LOCKUP_Pos+1]    = "CPU LOCK-UP DETECTED",
    [POWER_RESETREAS_OFF_Pos+1]       = "DETECT SIGNAL FROM GPIO",
    [POWER_RESETREAS_LPCOMP_Pos+1]    = "ANADETECT SIGNAL FROM LPCOMP",
    [POWER_RESETREAS_DIF_Pos+1]       = "ENTERING INTO DEBUG INTERFACE MODE",
    [POWER_RESETREAS_NFC_Pos+1]       = "NFC FIELD",
    [POWER_RESETREAS_NFC_Pos+2]       = "UNKNOWN RESET REASON"
};

#endif // DROPLET_USE_UART_LOGGING

/*****************************************************************************************************************
 *
 *                                    Local Function Forward Definition
 *
 *****************************************************************************************************************/
void advertising_init_data(uint8_t rssi, uint8_t timeout);
void advertising_start(uint8_t alerting, uint8_t timeout);
void advertising_stop(void);

void led_on(uint32_t pin_number);
void led_off(uint32_t pin_number);

#ifdef BLE_TEST_MODE
void ble_test_routine(void);
#endif
void nrf52_pheriperals_check(void);


/*****************************************************************************************************************
 *
 *                                             Extern Variables
 *
 *****************************************************************************************************************/
 
extern ble_rmcs_t           m_rmcs;                                     /**< Roam Configuration Service */
ble_bas_t                   m_bas;                                      /**< Structure used to identify the battery service. */


/*****************************************************************************************************************
 *
 *                                             Static Variables
 *
 *****************************************************************************************************************/
 
static ble_dis_init_t       dis_init;
static uint8_t              m_serial_num[18];                           /**< Serial number is the random address */

static ble_gap_sec_params_t m_sec_params;                               /**< Security requirements for this application. */
uint16_t                    m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */

static uint8_t              percentage_batt_lvl = 0;

static bool                 ble_stack_inited = false;

#ifdef  BATTERY_TIMER
APP_TIMER_DEF(m_battery_timer_id);                                      /**< Battery timer. */
#endif

static uint8_t m_beacon_info[APP_BEACON_INFO_LENGTH] =                    /**< Information advertised by the Beacon. */
{
    APP_DEVICE_TYPE,     // Manufacturer specific information. Specifies the device type in this 
                         // implementation. 
    APP_ADV_DATA_LENGTH, // Manufacturer specific information. Specifies the length of the 
                         // manufacturer specific data in this implementation.
    APP_BEACON_UUID,     // 128 bit UUID value. 
    APP_MAJOR_VALUE,     // Major arbitrary value that can be used to distinguish between Beacons. 
    APP_MINOR_VALUE,     // Minor arbitrary value that can be used to distinguish between Beacons. 
    APP_MEASURED_RSSI    // Manufacturer specific information. The Beacon's measured TX power in 
                         // this implementation. 
};




/**@brief Function for error handling, which is called when an error has occurred.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of error.
 *
 * @param[in] error_code  Error code supplied to the handler.
 * @param[in] line_num    Line number where the handler is called.
 * @param[in] p_file_name Pointer to the file name.
 */

void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
    //led_on(ASSERT_LED_PIN_NO);

    // This call can be used for debug purposes during application development.
    // @note CAUTION: Activating this code will write the stack to flash on an error.
    //                This function should NOT be used in a final product.
    //                It is intended STRICTLY for development/debugging purposes.
    //                The flash write will happen EVEN if the radio is active, thus interrupting
    //                any communication.
    //                Use with care. Un-comment the line below to use.
    //ble_debug_assert_handler(error_code, line_num, p_file_name);

    RMLOG("Error 0x%08X at line %u in file %s", error_code, line_num, p_file_name);

    lifesole_set_event_scheduler(LS_EVENT_FACTORY_RESTART);
    
    // On assert, the system can only recover on reset.
    NVIC_SystemReset();
}


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


static void storage_access_complete_handler(void)
{
        // TODO    
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in]   sys_evt   System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    uint32_t err_code;
    uint32_t count;

    pstorage_sys_event_handler(sys_evt);

    // Check if storage access is in progress.
    err_code = pstorage_access_status_get(&count);
    if ((err_code == NRF_SUCCESS) && (count == 0))
    {
        storage_access_complete_handler();
    }    
}


/*
 * Enable the LED
 */

void leds_init(void)
{
    
#if !DROPLET_RELEASE_MODE

#ifdef LED_CONNECTED_TO_VDD
    RMLOG("leds_init(): LEDs are connected to VDD");
#else
    RMLOG("leds_init(): LEDs are connected to GND");
#endif // LED_CONNECTED_TO_VDD

    // Configure then for output (they are active low, internal pullups)
    nrf_gpio_cfg_output(LED_RED);
    nrf_gpio_cfg_output(LED_GREEN);
    
    // Turn the LEDs off
    led_off(LED_RED);
    led_off(LED_GREEN);

#endif // DROPLET_RELEASE_MODE

}


/**@brief Function for switching a LED on.
 * 
 * It simplifies the code change for supporting different PCBs
 */
void led_on(uint32_t pin_number)
{

#if !DROPLET_RELEASE_MODE


#ifdef LED_CONNECTED_TO_VDD
    // LED are direcly connected to VDD and GPIO pin <pin_number>
    nrf_gpio_pin_clear(pin_number);
#else // LED_CONNECTED_TO_VDD
    // LED are direcly connected to GND and GPIO pin <pin_number>
    nrf_gpio_pin_set(pin_number);
#endif // LED_CONNECTED_TO_VDD


#endif // DROPLET_RELEASE_MODE

}


/**@brief Function for switching a LED off.
 * 
 * It simplifies the code change for supporting different PCBs
 */
void led_off(uint32_t pin_number)
{

#if !DROPLET_RELEASE_MODE


#ifdef LED_CONNECTED_TO_VDD
    // LED are direcly connected to VDD and GPIO pin <pin_number>
    nrf_gpio_pin_set(pin_number);
#else
    // LED are direcly connected to GND and GPIO pin <pin_number>
    nrf_gpio_pin_clear(pin_number);
#endif // LED_CONNECTED_TO_VDD


#endif // DROPLET_RELEASE_MODE

}


/*
 * Start the advertising
 */
void advertising_start(uint8_t alerting, uint8_t timeout)
{
    uint32_t err_code;

    if (ble_conn_state_status(m_conn_handle) == BLE_CONN_STATUS_CONNECTED)
    {
        RMLOG("advertising_start(): BLE seem to be connected (BLE_CONN_STATUS_CONNECTED) to connect handle 0x%04X", m_conn_handle);
        return;
    }

    //advertising_stop();
    err_code = sd_ble_gap_adv_stop();
    RMLOG("advertising_start(): sd_ble_gap_adv_stop() returned err code 0x%08X", err_code);

    // Change the advertising payload
    // Max value for percentage_batt_lvl is 100 (0x64), we can combine alerting information with battery level
    advertising_init_data(alerting ? (0x80 | percentage_batt_lvl) : percentage_batt_lvl, timeout);

    // Start execution.
    err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);

    if (m_conn_handle != BLE_CONN_HANDLE_INVALID || device_is_locked()) {
        RMLOG("Note: not locking...");
    }

    RMLOG("Starting advertising with alerting %s", alerting ? "set" : "not set");
}


/*
 * Stop advertising
 */

void advertising_stop(void)
{
    uint32_t err_code;

    if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        RMLOG("Closing BLE connection...");
        err_code = sd_ble_gap_disconnect(m_conn_handle,  BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        if (err_code != NRF_SUCCESS)
        {
            RMLOG("advertising_stop(): sd_ble_gap_disconnect(m_conn_handle=0x%04X) returned 0x%08X", m_conn_handle, err_code);
            //return err_code;
        }
    }
    else
    {
        RMLOG("Stop advertising...");
        err_code = sd_ble_gap_adv_stop();
        //sd_ble_gap_adv_stop();
        RMLOG("sd_ble_gap_adv_stop() returned 0x%08X", err_code);
    }

    
    //sd_softdevice_disable();
    //ble_stack_inited = false;
    //NRF_RADIO->TASKS_DISABLE = 1;
    //NRF_RADIO->POWER = 0;

}

#if 0
/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code;
#if 0
    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);
    
    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);
#endif
    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}
#endif

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            RMLOG("Advertising fast");
            break;
        case BLE_ADV_EVT_SLOW:
            RMLOG("Advertising slow");
            break;
        case BLE_ADV_EVT_IDLE:
            RMLOG("Idling the advertising");
            //RMLOG("Idling the advertising and going to sleep");
            //sleep_mode_enter();
            break;
        default:
            break;
    }
}


/* 
 * Function for initializing the Advertising functionality.
 * 
 * When unconfigured or in slow down, we send local name + our BCS service
 * 
 */
void advertising_init_data(uint8_t rssi, uint8_t timeout)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_manuf_data_t manuf_specific_data;

    /*
     *  Prepare an iBeacon payload
     */
     
    manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;

    uint16_t major_value = ADVERTISING_MAJOR_VALUE;
    uint16_t minor_value = ADVERTISING_MINOR_VALUE;

    uint8_t index = MAJ_VAL_OFFSET_IN_BEACON_INFO;

    m_beacon_info[index++] = MSB_16(major_value);
    m_beacon_info[index++] = LSB_16(major_value);

    m_beacon_info[index++] = MSB_16(minor_value);
    m_beacon_info[index++] = LSB_16(minor_value);

    // Lets utilise the RSSI byte as our alerting and/or battery level indicator
    RMLOG("Setting RSSI to 0x%02X", rssi);
    m_beacon_info[APP_BEACON_INFO_LENGTH-1] = rssi;
    
    manuf_specific_data.data.p_data = (uint8_t *) m_beacon_info;
    manuf_specific_data.data.size   = APP_BEACON_INFO_LENGTH;

    // Build and set advertising data defaults
    memset(&advdata, 0, sizeof(advdata));

    /*
     * No name, but iBeacon payload (no need to use extra services here in this case we use RSSI to indicate status)
     */

    advdata.name_type               = BLE_ADVDATA_NO_NAME;
    advdata.p_manuf_specific_data   = &manuf_specific_data;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    ble_adv_modes_config_t options = {0};

    options.ble_adv_fast_enabled      = BLE_ADV_FAST_ENABLED;
    options.ble_adv_fast_interval     = APP_ADV_FAST_INTERVAL;
    options.ble_adv_fast_timeout      = timeout != 0? timeout : APP_ADV_FAST_TIMEOUT;

    options.ble_adv_slow_enabled      = BLE_ADV_SLOW_DISABLED; // BLE_ADV_SLOW_ENABLED;
    options.ble_adv_slow_interval     = APP_ADV_SLOW_INTERVAL;
    options.ble_adv_slow_timeout      = timeout != 0? timeout : APP_ADV_SLOW_TIMEOUT;
    
    err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
    APP_ERROR_CHECK(err_code);

    /* Set the power level */
    err_code = sd_ble_gap_tx_power_set(DROPLET_DEFAULT_TX_POWER);
    APP_ERROR_CHECK(err_code);

    RMLOG("Initialising advertising and set power level to %i", DROPLET_DEFAULT_TX_POWER);


}


/*
 * Handler is called on successful battery measurement
 */

static void battery_level_measured_handler(uint8_t percentage)
{
#if 0
    static uint8_t test = 100;
    test --;
    percentage = test;
#endif

    // Save a copy, update the device name as we show the battery life in the device name
    if (percentage != percentage_batt_lvl)
    {
        percentage_batt_lvl = percentage;

 #if 0   // Not in this product

        // Reinit the data since battery indicator has changed and we advertise it faking it in service
        advertising_init_data();
        
        // Restart the advertising with new information
        advertising_start(false, 0);

#endif // #if 0

    }
    

    if (percentage_batt_lvl < BATTERY_WARNING_LEVEL) {
        // TODO: generate event and advertise about warning battery level
        RMLOG("Battery level (%d%) is below warning level (%d%)",percentage_batt_lvl, BATTERY_WARNING_LEVEL);
    }
    else {
        RMLOG("Battery is %d%", percentage_batt_lvl);
    }

}


#ifdef BATTERY_TIMER

/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *          This function will start the ADC.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void battery_level_meas_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);

    battery_start(battery_level_measured_handler);
}


/**@brief Function for starting the application timers.
 */
static void battery_timer_start(void)
{

    uint32_t err_code;

    // Start application timers
    err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);

    // And fire one straight away
    battery_start(battery_level_measured_handler);

}

#endif // BATTERY_TIMER


/**@brief Function for starting battery level functionality.
 */
void battery_level_check(void)
{   
    RMLOG("battery_level_check(): trigger a battery measurement!");

#ifdef BATTERY_TIMER

    /* Start a 24-hour timer for daily battery check */
    battery_timer_start();

#else

    /* Trigger battery measurement now */
    battery_start(battery_level_measured_handler);

    // ADC acquisition time: 10µs (maximum 40µs). We wait a bit longer to be sure battery measurement has been completed before starting advertising
    // Ref: http://infocenter.nordicsemi.com/topic/com.nordic.infocenter.nrf52832.ps.v1.1/saadc.html?cp=2_2_0_36_8#concept_qh3_spp_qr
    nrf_delay_us(100);

#endif
}


/**@brief Function for the Power manager.
 */
void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
    
}


/*
 * Sets the device name accordingly..
 */

static void configure_device_name(void)
{
    ble_gap_conn_sec_mode_t sec_mode;
    uint32_t                err_code;

    RMLOG("Setting device name to %s", get_device_name());
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,(uint8_t const *) get_device_name(), strlen((char *)get_device_name()));
    APP_ERROR_CHECK(err_code);

}


/**@brief Function for the GAP initialization.
 *
 * @details This function shall be used to setup all the necessary GAP (Generic Access Profile) 
 *          parameters of the device. It also sets the permissions and appearance.
 */
void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    
    // Set the devices name at startup..
    configure_device_name();
    
    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

}

/**@brief Function for initializing services that will be used by the application.
 *
 * Initialise all the services that the Beacon supports
 *
 */

void services_init(void)
{
    uint32_t err_code;
    ble_bas_init_t bas_init;
    ble_gap_addr_t   addr;

    // Initialize Battery Service
    memset(&bas_init, 0, sizeof(bas_init));

    // Here the sec level for the Battery Service can be changed/increased.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init.battery_level_char_attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_report_read_perm);

    bas_init.evt_handler          = NULL;
    bas_init.support_notification = false;                                                  // Disable battery notification via BLE.
    bas_init.p_report_ref         = NULL;
    bas_init.initial_batt_level   = 100;

    err_code = ble_bas_init(&m_bas, &bas_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Device Information Service
    memset(&dis_init, 0, sizeof(dis_init));

    // Utilise the MAC address as serial number
    sd_ble_gap_address_get(&addr);
    memset(&m_serial_num, 0, sizeof(m_serial_num));
    sprintf((char *)m_serial_num, "%02X:%02X:%02X:%02X:%02X:%02X", addr.addr[5], addr.addr[4], addr.addr[3], addr.addr[2], addr.addr[1], addr.addr[0]);

    RMLOG("Device serial number is %s", m_serial_num);

    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, MODEL_MANU_NAME);
    ble_srv_ascii_to_utf8(&dis_init.model_num_str, MODEL_NAME);
    ble_srv_ascii_to_utf8(&dis_init.serial_num_str, (char *) m_serial_num);

    ble_srv_ascii_to_utf8(&dis_init.hw_rev_str, MODEL_HW_REVISION);
    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str, MODEL_FW_REVISION);
    ble_srv_ascii_to_utf8(&dis_init.sw_rev_str, MODEL_SW_REVISION);

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);

    err_code = ble_dis_init(&dis_init);
    APP_ERROR_CHECK(err_code);

    /* Init the standard Roam Configuration Service */
	registerRMCS();

}


/**@brief Function for initializing security parameters.
 */
void sec_params_init(void)
{
    m_sec_params.bond         = SEC_PARAM_BOND;
    m_sec_params.mitm         = SEC_PARAM_MITM;
    m_sec_params.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    m_sec_params.oob          = SEC_PARAM_OOB;
    m_sec_params.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    m_sec_params.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    RMLOG("Hit the conn_params_evt");
    
    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    RMLOG("Error with connection parameters");
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;
    
    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code = NRF_SUCCESS;

#if 0
    static ble_gap_evt_auth_status_t m_auth_status;
    ble_gap_enc_info_t *             p_enc_info;
#else
    UNUSED_PARAMETER(m_sec_params);
#endif

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            security_connection();
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            security_disconnection();
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            RMLOG("Disconnected");
            // If the config is changed, or we're in unconfigured mode, just restart when you disconnect.
            if (config_is_changed()) {
                RMLOG("Config changed, restarting");
                wait_for_flash_and_reset();
            }

            // Restart the advertising
            //advertising_start(false, 0);

            // Start the sense timer to disable
            //sense_disconnected();
            
            break;
            
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            RMLOG("%s:%u",__FUNCTION__,__LINE__);
            security_violation();
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
                                                   BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP,
                                                   NULL,
                                                   NULL);
            APP_ERROR_CHECK(err_code);
            
            break;
            
        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_AUTH_STATUS:
            RMLOG("%s:%u",__FUNCTION__,__LINE__);
            break;
            
        case BLE_GAP_EVT_SEC_INFO_REQUEST:
            RMLOG("%s:%u",__FUNCTION__,__LINE__);
            break;

        case BLE_GAP_EVT_TIMEOUT:
            
            if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN /*BLE_GAP_TIMEOUT_SRC_ADVERTISEMENT - CSM: */) {
                RMLOG("Timeout -- Handle this");
                security_disconnection();
                if (config_is_changed()) {
                    RMLOG("Config changed, restarting");
                    wait_for_flash_and_reset();
                }

                // Restart the advertising
                advertising_start(false, 0);
                
                // Start the sense timer to disable
                //sense_disconnected();

            }

            break;

        default:
//            RMLOG("Some BLE event");
            break;
    }

    APP_ERROR_CHECK(err_code);
}

/*
 * Handle the name change attempt
 */

static void ble_gatt_params_on_ble_evt(ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    
    if (p_evt_write->uuid.uuid == BLE_UUID_GAP_CHARACTERISTIC_DEVICE_NAME)
    {
        RMLOG("Name change attempt");
        set_device_name(p_evt_write->data, p_evt_write->len);
    }
    
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the scheduler in the main loop after a BLE stack
 *          event has been received.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    
    on_ble_evt(p_ble_evt);
    
    // Connection parameters
    ble_conn_params_on_ble_evt(p_ble_evt);
    
    // Device Name parameter
    ble_gatt_params_on_ble_evt(p_ble_evt);
        
    // Handle any Roam Core BLE services
    ble_rmcs_on_ble_evt(&m_rmcs, p_ble_evt);
    
    // Handle the Battery services
    ble_bas_on_ble_evt(&m_bas, p_ble_evt);

    // Anything for advertising to handle
    ble_advertising_on_ble_evt(p_ble_evt);

}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
void ble_stack_init(void)
{
    //static bool ble_stack_inited = false;
    uint32_t err_code;
    ble_enable_params_t ble_enable_params;

    if (ble_stack_inited)
    {
        RMLOG("BLE Stack already inited: skipping additional initialisation request");
        return;
    }

    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Adjust for different memory settings
    ble_enable_params.common_enable_params.vs_uuid_count = VENDOR_SPECIFIC_UUID_COUNT;
    ble_enable_params.gatts_enable_params.attr_tab_size = 0x580 + 0x300;

#define BLE_DFU_APP_SUPPORT
#ifdef BLE_DFU_APP_SUPPORT
    ble_enable_params.gatts_enable_params.service_changed = 1;
#endif
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
    
    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    ble_stack_inited = true;

}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
void timers_init(void)
{
#ifdef  BATTERY_TIMER
    
    uint32_t err_code;

    // Create timers
    err_code = app_timer_create(&m_battery_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                battery_level_meas_timeout_handler);
    APP_ERROR_CHECK(err_code);
#else 

    RMLOG("The timer for the battery cannot be used in this version of the firmware and will not be initialised.");

#endif    
}


/*
 * Sort out everything we need to use BLE 
 */

void prepare_advertising(void)
{
    static uint8_t ready=false;

    if (!ready) {

        ready = true;
          
        // Build up the beacon advertisement string
        settings_init();
        
        // Init the default Generic Access Profile stuff
        gap_params_init();

        // Setup all the characteristics/services that are published for configuration
        services_init();
                     
        // Set up the connection & security parameters
        conn_params_init();
        
        sec_params_init();
        

    }

}


/*
 * Enable DC/DC converter
 */

void dcdc_enable(void)
{

#ifdef DCDCEN

    // Enable DC/DC converter on Lifesole rev. 3
    RMLOG("Enabling DC/DC converter (Radio power saving)");
    sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);

#else

    RMLOG("No DC/DC converter support: nothing to enable");    

#endif

}

/*
 * Disable DC/DC converter
 */

void dcdc_disable(void)
{
    // Disable DC/DC converter on Lifesole rev. 3
#ifdef DCDCEN
    if (NRF_RADIO->POWER)
    {
        RMLOG("RADIO is ON: DC/DC converter will not be disabled");
    }
    else 
    {
        RMLOG("RADIO is OFF: Disabling DC/DC converter");
        sd_power_dcdc_mode_set(NRF_POWER_DCDC_DISABLE);
    }
#else
    RMLOG("No DC/DC converter support: nothing to disable");    
#endif
    
}



/**@brief Function for checking if nRF has been reset and why
 *
 * @details After reading RESETREAS register, it clears its content.
 *
 * @return RESETREAS register value
 */

uint32_t check_reset_reason(void)
{
    uint32_t reset_reason = NRF_POWER->RESETREAS;
    // Clear RESETREAS fields by writing '1' to them.
    NRF_POWER->RESETREAS = 0xFFFFFFFF;

#if DROPLET_USE_UART_LOGGING

    char *reset_reason_str;
    int index;

    for (index=0; index < MAX_RESET_REASONS-1; index++)
    {
        if (reset_reason == (1 << (index-1))) break;
    }

    reset_reason_str = reset_reason_strings[index];
    
    RMLOG("Reset reason [0x%08X] = %s", reset_reason, reset_reason_str);

#endif

    return reset_reason;
}

/*
 * Where all the good stuff happens!
 */

int main(void)
{
    uint32_t reason;

    /* Standard things we need to operate */
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    /* Set up the GPIO so we can use interrupts */
    APP_ERROR_CHECK(nrf_drv_gpiote_init());

    /* Setup the UART & logging if in release mode */
    log_init();

    /* Entry message */
    RMLOG ("\n\nLifeSole - v1.0\n");

    /* Determine the reset reason */
    reason = check_reset_reason();

    /* Setup the LEDs if in release mode */
    leds_init();

    /* Initialize the SoftDevice as we need it for the SD_XXXX functions we use */
    RMLOG ("Skipping BLE Stack initialisation for the moment");
    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
    /* Initialize the SoftDevice handler module */
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

    /* Enable DC/DC converter for reducing power consumption especially while using nRF52's RADIO module */
    dcdc_enable();


/* MACROS for test can be defined in file droptlet.h */

   /* Used for testing SX9300 driver. */
#ifdef SX9300_TEST_MODE

    sx9300_test();

#endif


   /* Used for testing BLE Advertising and DFU. */
#ifdef BLE_TEST_MODE

    ble_test_routine();

#endif


    /*
     * Prepare and start executing the state machine for Lifesole depending on what the reset reason was sets initial states
     *  otherwise it will continue from it's last known state/event to handle
     */
    lifesole_init(reason);

    while (1)
    {
        app_sched_execute();

        /* If nothing left to execute and the lifesole_get_sleep flag is true, lets do a deep sleep, otherwise just a low power but still System On mode */
        if (lifesole_get_sleep() && !sx9300_measurement_in_progress() && !adxl362_irq_pending())
        {
            RMLOG("Scheduling Sleep function");

            app_sched_event_put(NULL, 0, (app_sched_event_handler_t) lifesole_sleep);
        }
        else
        {
            /* Let's keep the log messages clean */
            RMLOG("\n****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************\n\n");

            power_manage();

        }  
    }
}


/*
 * Function for testing BLE Advertising and DFU.
 * 
 */

#ifdef BLE_TEST_MODE

void ble_test_routine(void)
{

    RMLOG("TESTING BLUETOOTH");

    /* Triggers a battery measurement. Battery level will be advertised via BLE */
    battery_level_check();

    uint8_t advertising_time;   // seconds
    uint8_t loop_delay_ms = 10; // milliseconds
    uint8_t delay_seconds = 10; // seconds

    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

    led_on(LED_1);    
    led_on(LED_2);

    nrf_delay_ms(1000);

    led_off(LED_1);
    led_off(LED_2);

    while(1)
    {
        RMLOG("\n");

        if (!ble_stack_inited)
        {
            /* Testing the Bluetooth module in advertising mode */
            ble_stack_init();
            prepare_advertising();
        }

        RMLOG("\n");
        advertising_time = 15;
        advertising_start(true, advertising_time);
        led_on(LED_1);
        led_off(LED_2);

        //loop_delay_ms = 10;
        for (int i=0; i < (advertising_time * 1000 / loop_delay_ms); i++)
        {
            nrf_delay_ms(loop_delay_ms);
            app_sched_execute();
        }

        /* Stop the BLE advertising */
        advertising_stop();
        led_off(LED_1);

        RMLOG("Stop advertising for 10 seconds...\n"); 
        //loop_delay_ms = 10;    
        //delay_seconds = 10;            
        for (int i=0; i < (delay_seconds * 1000 / loop_delay_ms); i++)
        {
            nrf_delay_ms(loop_delay_ms);
            app_sched_execute();
        }

        RMLOG("\n");
        
        if (!ble_stack_inited)
        {
            /* Initialize the SoftDevice handler module */
            SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

            /* Testing the Bluetooth module in advertising mode */
            ble_stack_init();
            prepare_advertising();
        }

        /* Testing the Bluetooth module in advertising mode */
        advertising_time = 15;
        advertising_start(false, advertising_time);
        led_on(LED_1);
        led_off(LED_2);

        //loop_delay_ms = 10;
        for (int i=0; i < (advertising_time * 1000 / loop_delay_ms); i++)
        {
            nrf_delay_ms(loop_delay_ms);
            app_sched_execute();
        }

        /* Stop the BLE advertising */
        advertising_stop();
        led_off(LED_1);

        RMLOG("Stop advertising for 10 seconds...\n"); 
        //loop_delay_ms = 10;
        //delay_seconds = 10;       
        for (int i=0; i < (delay_seconds * 1000 / loop_delay_ms); i++)
        {
            nrf_delay_ms(loop_delay_ms);
            app_sched_execute();
        }
        
        if (!ble_stack_inited)
        {
            /* Initialize the SoftDevice handler module */
            SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
        }
    }

}

#endif


/*
 * Function scanning all pheriperals of nRF52 and providing information about which is enabled and its configuration
 * WARNING: it may cause overflow in RX UART interface in case this function is used together with other LOGGING messages
 */

void nrf52_pheriperals_check(void)
{

#ifdef DROPLET_USE_UART_LOGGING

    typedef struct {
        unsigned char reg_name[16];
        uint16_t reg_offset;
        uint8_t num_elements;
        uint32_t bit_mask; // num_elements always = 1
    } registers;


    typedef struct {
        char p_name[8];
        uint32_t base_offset;
        uint8_t num_registers;
        registers reg_list[];
    } peripherals;

#define PERIPHERALS(x)      static struct {char p_name[8]; uint32_t base_offset; uint8_t num_registers; registers reg_list[x]; } 


    PERIPHERALS(9) power_pe = {
        "POWER",
        NRF_POWER_BASE,
        9,
        {
            {"DCDCEN",          0x578,  1,  0x00000001},
            {"RAM[0].POWER",    0x900,  1,  0x00030003},
            {"RAM[1].POWER",    0x910,  1,  0x00030003},
            {"RAM[2].POWER",    0x920,  1,  0x00030003},
            {"RAM[3].POWER",    0x930,  1,  0x00030003},
            {"RAM[4].POWER",    0x940,  1,  0x00030003},
            {"RAM[5].POWER",    0x950,  1,  0x00030003},
            {"RAM[6].POWER",    0x960,  1,  0x00030003},
            {"RAM[7].POWER",    0x970,  1,  0x00030003}            
        }
    };

    PERIPHERALS(6) clock_pe = {
        "CLOCK",
        NRF_CLOCK_BASE,
        6,
        {
            {"HFCLKRUN",        0x408,  1,  0x00000001},
            {"HFCLKSTAT",       0x40C,  1,  0x00010001},
            {"LFCLKRUN",        0x414,  1,  0x00010003},
            {"LFCLKSTAT",       0x418,  1,  0x00010003},
            {"LFCLKSRCCOPY",    0x41C,  1,  0x00000003},
            {"LFCLKSRC",        0x518,  1,  0x00030003}           
        }
    };

    PERIPHERALS(6) radio_pe = {
        "RADIO",
        NRF_RADIO_BASE,
        6,
        {
            {"FREQUENCY",       0x508,  1,  0x0000017F},
            {"TXPOWER",         0x50C,  1,  0x000000FF},
            {"MODE",            0x510,  1,  0x0000000F},
            {"STATE",           0x550,  1,  0x0000000F},
            {"MODECNF0",        0x650,  1,  0x00000301},
            {"POWER",           0xFFC,  1,  0x00000001}         
        }
    };

    PERIPHERALS(1) uart_pe = {
        "UART0",
        NRF_UART0_BASE,
        1,
        {
            {"ENABLE",          0x500,  1,  0x0000000F}    
        }
    };

    PERIPHERALS(2) twi_pe = {
        "TWI0",
        NRF_TWI0_BASE,
        2,
        {
            {"ENABLE",          0x500,  1,  0x0000000F},
            {"FREQUENCY",       0x524,  1,  0xFFFFFFFF}       
        }
    };

    PERIPHERALS(3) spi_pe = {
        "SPI1",
        NRF_SPI1_BASE,
        3,
        {
            {"ENABLE",          0x500,  1,  0x0000000F},
            {"FREQUENCY",       0x524,  1,  0xFFFFFFFF},
            {"CONFIG",          0x554,  1,  0x00000007}       
        }
    };

/*

    PERIPHERALS(0) nfct_pe = {
        "NFCT",
        NRF_NFCT_BASE,
        ,
        {
            {"",          0x000,  1,  0x00000000},
            {"",          0x000,  1,  0x00000000},
            {"",          0x000,  1,  0x00000000}       
        }
    };

    PERIPHERALS(0) fpu_pe = {
        "FPU",
        NRF_FPU_BASE,
        ,
        {
            {"",          0x000,  1,  0x00000000},
            {"",          0x000,  1,  0x00000000},
            {"",          0x000,  1,  0x00000000}       
        }
    };

*/

    PERIPHERALS(8) gpiote_pe = {
        "GPIOTE",
        NRF_GPIOTE_BASE,
        8,
        {
            {"CONFIG[0]",       0x510,  1,  0x00131F03},
            {"CONFIG[1]",       0x514,  1,  0x00131F03},
            {"CONFIG[2]",       0x518,  1,  0x00131F03},
            {"CONFIG[3]",       0x51C,  1,  0x00131F03},
            {"CONFIG[4]",       0x520,  1,  0x00131F03},
            {"CONFIG[5]",       0x524,  1,  0x00131F03},
            {"CONFIG[6]",       0x528,  1,  0x00131F03},
            {"CONFIG[7]",       0x52C,  1,  0x00131F03},
        }
    };

    PERIPHERALS(2) saadc_pe = {
        "SAADC",
        NRF_SAADC_BASE,
        2,
        {
            {"STATUS",          0x400,  1,  0x00000001},
            {"ENABLE",          0x500,  1,  0x00000001}
        }
    };

    PERIPHERALS(8) timer_pe = {
        "TIMER0",
        NRF_TIMER0_BASE,
        8,
        {
            {"MODE",            0x504,  1,  0x00000003},
            {"BITMODE",         0x508,  1,  0x00000003},
            {"CC[0]",           0x540,  1,  0xFFFFFFFF},
            {"CC[1]",           0x544,  1,  0xFFFFFFFF},
            {"CC[2]",           0x548,  1,  0xFFFFFFFF},
            {"CC[3]",           0x54C,  1,  0xFFFFFFFF},
            {"CC[4]",           0x550,  1,  0xFFFFFFFF},
            {"CC[5]",           0x554,  1,  0xFFFFFFFF}
        }
    };

    PERIPHERALS(5) rtc_pe = {
        "RTC0",
        NRF_RTC0_BASE,
        5,
        {
            {"COUNTER",         0x504,  1,  0x00FFFFFF},
            {"CC[0]",           0x540,  1,  0x00FFFFFF},
            {"CC[1]",           0x544,  1,  0x00FFFFFF},
            {"CC[2]",           0x548,  1,  0x00FFFFFF},
            {"CC[3]",           0x54C,  1,  0x00FFFFFF}
        }
    };

    PERIPHERALS(2) wdt_pe = {
        "WDT",
        NRF_WDT_BASE,
        2,
        {
            {"RUNSTATUS",       0x400,  1,  0x00000001},
            {"CONFIG",          0x50C,  1,  0x00000009}
        }
    };

    PERIPHERALS(5) nvmc_pe = {
        "NVMC",
        NRF_NVMC_BASE,
        5,
        {
            {"READY",           0x400,  1,  0x00000001},
            {"CONFIG",          0x504,  1,  0x00000003},
            {"ICACHECNF",       0x540,  1,  0x00000101},
            {"IHIT",            0x548,  1,  0xFFFFFFFF},
            {"IMISS",           0x54C,  1,  0xFFFFFFFF}
        }
    };

    PERIPHERALS(37) gpio_pe = {
        "GPIO",
        NRF_P0_BASE,
        37,
        {
            {"OUT",             0x504,  1,  0xFFFFFFFF},
            {"IN",              0x510,  1,  0xFFFFFFFF},
            {"DIR",             0x514,  1,  0xFFFFFFFF},
            {"LATCH",           0x520,  1,  0xFFFFFFFF},
            {"DETECTMODE",      0x524,  1,  0xFFFFFFFF},
            {"PIN_CNF[00]",     0x700,  1,  0x0003070F},
            {"PIN_CNF[01]",     0x704,  1,  0x0003070F},
            {"PIN_CNF[02]",     0x708,  1,  0x0003070F},
            {"PIN_CNF[03]",     0x70C,  1,  0x0003070F},
            {"PIN_CNF[04]",     0x710,  1,  0x0003070F},
            {"PIN_CNF[05]",     0x714,  1,  0x0003070F},
            {"PIN_CNF[06]",     0x718,  1,  0x0003070F},
            {"PIN_CNF[07]",     0x71C,  1,  0x0003070F},
            {"PIN_CNF[08]",     0x720,  1,  0x0003070F},
            {"PIN_CNF[09]",     0x724,  1,  0x0003070F},
            {"PIN_CNF[10]",     0x728,  1,  0x0003070F},
            {"PIN_CNF[11]",     0x72C,  1,  0x0003070F},
            {"PIN_CNF[12]",     0x730,  1,  0x0003070F},
            {"PIN_CNF[13]",     0x734,  1,  0x0003070F},
            {"PIN_CNF[14]",     0x738,  1,  0x0003070F},
            {"PIN_CNF[15]",     0x73C,  1,  0x0003070F},
            {"PIN_CNF[16]",     0x740,  1,  0x0003070F},
            {"PIN_CNF[17]",     0x744,  1,  0x0003070F},
            {"PIN_CNF[18]",     0x748,  1,  0x0003070F},
            {"PIN_CNF[19]",     0x74C,  1,  0x0003070F},
            {"PIN_CNF[20]",     0x750,  1,  0x0003070F},
            {"PIN_CNF[21]",     0x754,  1,  0x0003070F},
            {"PIN_CNF[22]",     0x758,  1,  0x0003070F},
            {"PIN_CNF[23]",     0x75C,  1,  0x0003070F},
            {"PIN_CNF[24]",     0x760,  1,  0x0003070F},
            {"PIN_CNF[25]",     0x764,  1,  0x0003070F},
            {"PIN_CNF[26]",     0x768,  1,  0x0003070F},
            {"PIN_CNF[27]",     0x76C,  1,  0x0003070F},
            {"PIN_CNF[28]",     0x770,  1,  0x0003070F},
            {"PIN_CNF[29]",     0x774,  1,  0x0003070F},
            {"PIN_CNF[30]",     0x778,  1,  0x0003070F},
            {"PIN_CNF[31]",     0x77C,  1,  0x0003070F}
        }
    };

#define NUM_PERIPHERALS     13

    peripherals * pheriferals_list[NUM_PERIPHERALS] =
    {

        (peripherals *) &power_pe,                       // ((NRF_POWER_Type          *) NRF_POWER_BASE)
        (peripherals *) &clock_pe,                       // ((NRF_CLOCK_Type          *) NRF_CLOCK_BASE)
        (peripherals *) &radio_pe,                       // ((NRF_RADIO_Type          *) NRF_RADIO_BASE)
        (peripherals *) &uart_pe,                        // ((NRF_UART_Type           *) NRF_UART0_BASE)
        (peripherals *) &twi_pe,                         // ((NRF_TWI_Type            *) NRF_TWI0_BASE)
        (peripherals *) &spi_pe,                         // ((NRF_SPI_Type            *) NRF_SPI1_BASE)
    //  (peripherals *) &nfct_pe,                        // ((NRF_NFCT_Type           *) NRF_NFCT_BASE)
        (peripherals *) &gpiote_pe,                      // ((NRF_GPIOTE_Type         *) NRF_GPIOTE_BASE)
        (peripherals *) &saadc_pe,                       // ((NRF_SAADC_Type          *) NRF_SAADC_BASE)
        (peripherals *) &timer_pe,                       // ((NRF_TIMER_Type          *) NRF_TIMER0_BASE)
        (peripherals *) &rtc_pe,                         // ((NRF_RTC_Type            *) NRF_RTC0_BASE)
        (peripherals *) &wdt_pe,                         // ((NRF_WDT_Type            *) NRF_WDT_BASE)
        (peripherals *) &nvmc_pe,                        // ((NRF_NVMC_Type           *) NRF_NVMC_BASE)
    //  (peripherals *) &fpu_pe,                         // ((NRF_FPU_Type            *) NRF_FPU_BASE)
        (peripherals *) &gpio_pe                         // ((NRF_GPIO_Type           *) NRF_P0_BASE)

    };

    RMLOG("\nSTARTING PERIPHERAL SCAN\n");

    peripherals *test_pe;

    for (int i=0; i<NUM_PERIPHERALS; i++)
    {
        test_pe = pheriferals_list[i];

        RMLOG("%8s (0x%08X)", test_pe->p_name, test_pe->base_offset);

        for (int k=0; k < test_pe->num_registers; k++)
        {   
            uint32_t reg_address = test_pe->base_offset + test_pe->reg_list[k].reg_offset;
            uint32_t reg_value = *((uint32_t *) reg_address);

#define REG_SIZE        32  // bits
#define NUM_SEPARATORS  7
#define ARRAY_LENGTH    REG_SIZE + NUM_SEPARATORS + 1

            char reg_value_str[ARRAY_LENGTH];
            int num_separators = 0;

            for (int c=0; c<REG_SIZE; c++)
            {
                if (test_pe->reg_list[k].bit_mask & (1 << c)) // mask bit set
                {
                    if (reg_value & (1 << c))
                    {
                        reg_value_str[ARRAY_LENGTH -c -num_separators -2] = '1';
                    }
                    else
                    {
                        reg_value_str[ARRAY_LENGTH -c -num_separators -2] = '0';
                    }
                }
                else
                {
                    reg_value_str[ARRAY_LENGTH -c -num_separators -2] = 'x';
                }
                    
                if ((c+1)%4 == 0 && c<31)
                {
                    num_separators++;
                    reg_value_str[ARRAY_LENGTH -c -num_separators -2] = ' ';
                }
            }

            reg_value_str[ARRAY_LENGTH-1] = '\0';
            RMLOG("    %16s (0x%04X): %s", test_pe->reg_list[k].reg_name,  test_pe->reg_list[k].reg_offset, reg_value_str);
            //RMLOG("    %16s (0x%04X): 0x%08X = %s", test_pe->reg_list[k].reg_name,  test_pe->reg_list[k].reg_offset, reg_value & test_pe->reg_list[k].bit_mask, reg_value_str);
            
            
            nrf_delay_ms(2); 
        }

    }

    RMLOG("\nPERIPHERAL SCAN COMPLETED\n");

#endif // DROPLET_USE_UART_LOGGING

}


