/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * Roam Configuration Service
 *		- Security Modes
 *		- Counters
 *
 * (c) Roam Creative Ltd 2014
 *
 */

#ifndef BLE_RMCS_H__
#define BLE_RMCS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "droplet.h"

#define RMCS_UUID_BASE {0x4F, 0x52, 0x45, 0x57, 0xB8, 0x84, 0x94, 0xA0, 0xAA, 0xF5, 0xE2, 0x0F, 0x00, 0x00, 0x00, 0x90}

#define RMCS_UUID_SERVICE                   0x1000
#define RMCS_UUID_COUNTER1_CHAR             0x1001
#define RMCS_UUID_COUNTER2_CHAR             0x1002
#define RMCS_UUID_BEACON_CHALLENGE_CHAR     0x1003

#define RMCS_UUID_BEACON_TX_LEVEL_CHAR      0x1004
#define RMCS_UUID_BEACON_PRI_TIMEOUT_CHAR   0x1005
#define RMCS_UUID_BEACON_PRI_RATE_CHAR      0x1006
#define RMCS_UUID_BEACON_SEC_RATE_CHAR      0x1007
#define RMCS_UUID_BEACON_DEVICECTRL_CHAR    0x1008
#define RMCS_UUID_BEACON_SECURITYCTRL_CHAR  0x1009
#define RMCS_UUID_BEACON_SECURITYMODE_CHAR  0x100A
#define RMCS_UUID_BEACON_PASSCODE_CHAR      0x100B


/**
 * Data is directly put into these structures from the BLE service, the byte order doesn't match the native
 *  byte order so we have to translate in and out when we're wanting to use these.
 */

typedef struct {
    
	uint8_t	    counter1[4];
    uint8_t     counter2[4];
    uint8_t     challenge[20];
    uint8_t     devicectrl[2];

    //** DO NOT REORDER AS IT IS RESTORED DIRECTLY FROM FLASH  -- FLASH SAVED FIELDS ONLY BELOW **
    uint8_t     tx_level;
    uint8_t     adv_primary_timeout[2];                                                   // Seconds before switching to secondary rate
    uint8_t     adv_primary_rate[2];
    uint8_t     adv_secondary_rate[2];
    uint8_t     securityctrl[2];
    uint8_t     securitymode[2];
    uint8_t     passcode[20];
    
} __packed ble_rmcs_data_t;


typedef enum {
    rmcs_counter1_char_data,
    rmcs_counter2_char_data,
    rmcs_challenge_data,

    rmcs_tx_level_data,
    rmcs_pri_timeout_data,
    rmcs_pri_rate_data,
    rmcs_sec_rate_data,

    rmcs_devicectrl_data,

    rmcs_securityctrl_data,
    rmcs_securitymode_data,
    rmcs_passcode_data,
	
} rmcs_data_type_t;

// Forward declaration of the ble_rmcs_t type. 
typedef struct ble_rmcs_s ble_rmcs_t;

/**@brief Roam Configuration Service event handler type. */
typedef void (*ble_rmcs_write_handler_t) (ble_rmcs_t * p_rmcs, rmcs_data_type_t type, uint8_t *data);

/**@brief Roam Configuration Service init structure. This contains all options and data needed for
 *        initialization of the service.*/
typedef struct
{
    ble_rmcs_write_handler_t     write_handler;
	ble_rmcs_data_t				 *pData;
} ble_rmcs_init_t;


/**@brief Roam Configuration Service structure. This contains various status information for the service. */
typedef struct ble_rmcs_s
{         
    uint16_t                     service_handle;                 

	ble_gatts_char_handles_t     rmcs_counter1_char_handles;
	ble_gatts_char_handles_t     rmcs_counter2_char_handles;
    ble_gatts_char_handles_t     rmcs_challenge_char_handles;

    ble_gatts_char_handles_t     rmcs_tx_level_char_handles;
    ble_gatts_char_handles_t     rmcs_pri_timeout_char_handles;
    ble_gatts_char_handles_t     rmcs_pri_rate_char_handles;
    ble_gatts_char_handles_t     rmcs_sec_rate_char_handles;

    ble_gatts_char_handles_t     rmcs_devicectrl_char_handles;

    ble_gatts_char_handles_t     rmcs_securityctrl_char_handles;
    ble_gatts_char_handles_t     rmcs_securitymode_char_handles;
    ble_gatts_char_handles_t     rmcs_passcode_char_handles;

	uint8_t                      uuid_type;
    uint16_t                     conn_handle;  
    bool                         is_notifying;
    ble_rmcs_write_handler_t     beacon_write_handler;

} ble_rmcs_t;


/**@brief Initialize the Roam Configuration Service.
 *
 * @param[out]  p_rmcs       
 * @param[in]   p_rmcs_init  
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_rmcs_init(ble_rmcs_t * p_rmcs, const ble_rmcs_init_t * p_rmcs_init);

/**@brief Roam Configuration Service BLE stack event handler.
 *
 * @details Handles all events from the BLE stack of interest to the Roam Configuration Service.
 *
 * @param[in]   p_rmcs      Roam Configuration Service structure.
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 */
void ble_rmcs_on_ble_evt(ble_rmcs_t * p_rmcs, ble_evt_t * p_ble_evt);


#endif // BLE_RMCS_H__

/** @} */
