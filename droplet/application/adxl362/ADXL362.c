/***************************************************************************/
/**
*   @file   ADXL362.c
*   @brief  Implementation of ADXL362 Driver.
*   @author DNechita(Dan.Nechita@analog.com)
********************************************************************************
* Copyright 2012(c) Analog Devices, Inc.
*
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*  - Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*  - Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
*  - Neither the name of Analog Devices, Inc. nor the names of its
*    contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*  - The use of this software may or may not infringe the patent rights
*    of one or more patent holders.  This license does not release you
*    from the requirement that you obtain separate licenses from these
*    patent holders to use this software.
*  - Use of the software either in source or binary form, must be run
*    on or directly connected to an Analog Devices Inc. component.
*
* THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
********************************************************************************
*   SVN Revision: $WCREV$
*******************************************************************************/

// https://developer.mbed.org/users/tkreyche/notebook/adxl362_tkreyche/

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "ble_conn_params.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "nordic_common.h"
#include "softdevice_handler.h"
#include "pstorage.h"
#include "math.h"
#include "app_timer.h"
#include "app_button.h"
#include "droplet.h"
#include "logger.h"
#include "simple_uart.h"
#include "settings.h"
#include "nrf.h"
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"
#include "nrf_delay.h"
#include "nrf_drv_twi.h"
#include "app_util_platform.h"
#include "nrf_drv_spi.h"
#include "app_scheduler.h"
#include "lifesole.h"

#include "ADXL362.h"

/******************************************************************************/
/********************* Constants and Variables Declarations *******************/
/******************************************************************************/

/**< SPI instance index. */
#define SPI_INSTANCE  1

/**< Selected G-force range. */
static char selectedRange = 0;

/**< SPI instance. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);

/**< Flag used to indicate that SPI instance completed the transfer. */
static volatile bool spi_xfer_done;

/**< Flag to keep track wether IRQ Handler for ADXL has already been registered. */
static bool adxl_irq_registered = false;

/**< Flag to keep track wether there is any IRQ still to be serviced  */
static bool adxl_irq_pending = false;


static bool adxl_spi_inited = false;


/******************************************************************************/
/********************* External functions Declarations ************************/
/******************************************************************************/



/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/

static void ADXL362_power_on(void);
static void ADXL362_power_off(void);
static void adxl_irq_handler(void);
static void adxl_enable_system_off_irq_sensing(void);
static void adxl_irq_scheduler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void adxl_register_irq(void);



/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event)
{
    spi_xfer_done = true;
}


/*
 * Initialise the SPI with a particular speed
 */

uint8_t SPI_Init(void)
{
    //https://github.com/annem/ADXL362/blob/master/ADXL362.cpp

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG(SPI_INSTANCE);

    spi_config.irq_priority = ADXL_IRQ_PRIORITY;
    spi_config.frequency    = NRF_SPI_FREQ_8M;      // Max frequency supported by nRF52
    spi_config.miso_pin     = ADXL_MISO_PIN;
    spi_config.mosi_pin     = ADXL_MOSI_PIN;
    spi_config.sck_pin      = ADXL_SCK_PIN;
    spi_config.ss_pin       = ADXL_CS_PIN;
    spi_config.orc          = 0x00;
    
    RMLOG("ADXL362: SPI Init\n MISO  %u\n MOSI  %u\n CLOCK %u\n CS    %u", ADXL_MISO_PIN, ADXL_MOSI_PIN, ADXL_SCK_PIN, ADXL_CS_PIN);
    
    // If initied without event handler, transfers are blocking
    APP_ERROR_CHECK(nrf_drv_spi_init(&spi, &spi_config, NULL /*spi_event_handler*/));
    
    adxl_spi_inited = true;

    RMLOG("ADXL362: SPI interface initialised");
    return 0;
}


/*
 * Used in case ADXL is required to be powered off and set output pins as input 
 */

//static void SPI_Uninit(void)
void SPI_Uninit(void)
{
    nrf_drv_spi_uninit(&spi);
/*
    nrf_gpio_cfg_default(ADXL_SCK_PIN);
    nrf_gpio_cfg_default(ADXL_CS_PIN);
    nrf_gpio_cfg_default(ADXL_MOSI_PIN);
*/
/*
    nrf_gpio_cfg(
            ADXL_SCK_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLDOWN,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);

    
    nrf_gpio_cfg(
            ADXL_CS_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLDOWN,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
                
    nrf_gpio_cfg(
            ADXL_MOSI_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLDOWN,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
*/

    adxl_spi_inited = false;

    nrf_delay_ms(1); 

}


/*
 * Set all GPIO pins to default (input, buffer disconnected, no pull)
 */

void ADXL362_release_gpio_pins(void)
{
    nrf_gpio_cfg_default(ADXL_POWER);
    nrf_gpio_cfg_default(ADXL_SCK_PIN);
    nrf_gpio_cfg_default(ADXL_CS_PIN);
    nrf_gpio_cfg_default(ADXL_MOSI_PIN);
    nrf_gpio_cfg_default(ADXL_MISO_PIN);
    nrf_gpio_cfg_default(ADXL_INT_PIN);

    nrf_delay_ms(1); 
}


/*
 * Write a buffer over SPI to the particular slave
 */

void SPI_Write(uint8_t slave_addr, uint8_t *buffer, uint16_t size)
{
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, buffer, size, NULL, 0));
}


/*
 * Read a buffer over SPI from a particular slave
 */

void SPI_Read(uint8_t slave_addr, uint8_t *buffer, uint16_t size)
{
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi, buffer, size, buffer, size));
}


/*
 * Interface function for handling IRQ from events handler
 */

void adxl_irq_handler(void)
{    
    unsigned char status=0;

    // Retrieve what caused the sensor to trigger
    ADXL362_GetRegisterValue(&status, ADXL362_REG_STATUS, 1);

    if (status & ADXL362_STATUS_AWAKE) {
        lifesole_set_event_scheduler(LS_EVENT_MOTION_TRIGGERED);
    }
    else {
        lifesole_set_event_scheduler(LS_EVENT_NO_MOTION_TRIGGERED);
    }

    adxl_irq_pending = false;
 
}

/*
 * ADXL362 IRQ Event Scheduler
 */
void adxl_irq_scheduler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{    
    nrf_drv_gpiote_in_event_disable(ADXL_INT_PIN);
    RMLOG("\n!");
    adxl_irq_pending = true;
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t) adxl_irq_handler);
    nrf_drv_gpiote_in_event_enable(ADXL_INT_PIN, true);
}


/*
 * Check whether there is any irq pending
 */
bool adxl362_irq_pending(void)
{
    RMLOG("ADXL362:%s IRQ pending!", adxl_irq_pending?"":" NO");
    return adxl_irq_pending;
}



/*
 * ADXL362 IRQ Event Emulator
 */
void adxl_sw_irq_emulator(void)
{
    RMLOG("ADXL362: force scheduling of IRQ handler");
    adxl_irq_scheduler(ADXL_INT_PIN, NRF_GPIOTE_POLARITY_HITOLO);
}


void adxl_enable_system_off_irq_sensing(void)
{
    // When in System-Off GPIO pin can sense either High or Low, not transitions
    nrf_gpio_cfg_sense_input(ADXL_INT_PIN, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);

}


/*
 * Register an interrupt handler to call on an active low transition
 */

void adxl_register_irq(void)
{
    uint32_t err_code;

    if (adxl_irq_registered)
    {
     
        RMLOG("ADXL362: Interrupt was already initialised for ADXL362");
    
    }
    else
    {

        RMLOG("ADXL362: Registering Interrupt");

        nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true);
        in_config.pull = NRF_GPIO_PIN_PULLUP;

        // Registering interrupt pin and handler with GPIOTE module
        err_code =  nrf_drv_gpiote_in_init(ADXL_INT_PIN, &in_config, adxl_irq_scheduler);
        APP_ERROR_CHECK(err_code); 
        
        RMLOG("ADXL362: Interrupt registered");
        adxl_irq_registered = true;

    }    

    // In System OFF mode no interrupt is detected unless we "force" the SENSE field to be set by nrf_gpio_cfg_sense_input()
    adxl_enable_system_off_irq_sensing();
    
    nrf_drv_gpiote_in_event_enable(ADXL_INT_PIN, true); 
}


/*
 * Unregister the interrupt
 */

void adxl_unregister_irq(void)
{
    RMLOG("TODO: ADXL362: UNREGISTER INTERRUPT");
}


/***************************************************************************/
/**
* @brief Initializes communication with the device and checks if the part is
*        present by reading the device id.
*
* @return  0 - the initialization was successful and the device is present;
*         -1 - an error occurred.
*******************************************************************************/

char ADXL362_Init(void)
{
    unsigned char regValue = 0;
    char          status   = -1;

    /* Init SPI module interface */
    status = SPI_Init();
    
    /* Check the sensor PARTID */
    ADXL362_GetRegisterValue(&regValue, ADXL362_REG_PARTID, 1);

    if((regValue != ADXL362_PART_ID))
    {
        RMLOG("ADXL362: Bad part ID = 0x%02X! Let's try to restart the ADXL362", regValue);

        /* Restart the sensor */
        ADXL362_power_on();

        /* Try again to init SPI module interface */
        status = SPI_Init();
    
        /* Check again the sensor PARTID */
        ADXL362_GetRegisterValue(&regValue, ADXL362_REG_PARTID, 1);

        if (regValue != ADXL362_PART_ID)
        {
            RMLOG("ADXL362: Initialisation failed again. Bad part ID = 0x%02X!", regValue);
            
            return -1;
        }
    }
    
    /* Measurement Range: +/- 2g (reset default) */
    selectedRange = 2;

    /* Register IRQ Handler */
    adxl_register_irq();
    
    return status;

}


/***************************************************************************/
/**
* @brief Writes data into a register.
*
* @param registerValue   - Data value to write.
* @param registerAddress - Address of the register.
* @param bytesNumber     - Number of bytes. Accepted values: 0 - 1.
*
* @return None.
*******************************************************************************/

void ADXL362_SetRegisterValue(unsigned short registerValue,
                              unsigned char  registerAddress,
                              unsigned char  bytesNumber)
{
    unsigned char buffer[4] = {0, 0, 0, 0};
    
    if (!adxl_spi_inited)
    {
        SPI_Init();
    }
    
    buffer[0] = ADXL362_WRITE_REG;
    buffer[1] = registerAddress;
    buffer[2] = (registerValue & 0x00FF);
    buffer[3] = (registerValue >> 8);
    SPI_Write(ADXL362_SLAVE_ID, buffer, bytesNumber + 2);

#if 0    // Dump them out
    unsigned char index=0;
    RMLOG("Write to 0x%02X", registerAddress);
    for (index = 0; index < bytesNumber; index++) {
        RMLOG("0x%02X", buffer[index+2]);
    }
#endif
}


/***************************************************************************/
/**
* @brief Performs a burst read of a specified number of registers.
*
* @param pReadData       - The read values are stored in this buffer.
* @param registerAddress - The start address of the burst read.
* @param bytesNumber     - Number of bytes to read.
*
* @return None.
*******************************************************************************/

void ADXL362_GetRegisterValue(unsigned char* pReadData,
                              unsigned char  registerAddress,
                              unsigned char  bytesNumber)
{

    unsigned char index = 0;
    unsigned char buffer[64] = { 0 };
    
    if (!adxl_spi_inited)
    {
        SPI_Init();
    }

    if (pReadData == NULL) {
        pReadData = buffer;
    }    

    /* Buffer initialization */    
    buffer[0] = ADXL362_READ_REG;
    buffer[1] = registerAddress;
    for(index = 0; index < bytesNumber; index++)
    {
        buffer[index + 2] = pReadData[index];
    }
    
    SPI_Read(ADXL362_SLAVE_ID, buffer, bytesNumber + 2);

    for(index = 0; index < bytesNumber; index++)
    {
        pReadData[index] = buffer[index + 2];
    }

#if 0   // Dump them out
    RMLOG("Read from 0x%02X", registerAddress);
    for (index = 0; index < bytesNumber; index++) {
        RMLOG("Reg[%02X] = 0x%02X", index, pReadData[index]);
    }
#endif

}


/***************************************************************************/
/**
 * @brief Configures for motion wakeup
 *
 * Enables Wakeup Mode
 * Enables INT2
 * Enables Loop Mode
 *
 * @return None.
 *******************************************************************************/
void ADXL362_SetMotionMode(uint8_t active, uint8_t inactive)
{
    //unsigned char oldValue = 0;
    unsigned char newValue = 0;
    
    RMLOG("ADXL362: Enabling motion detection mode (w/defaults)");
    
    /* Setting Link-Loop mode for automatically alternating Activity/Inactivity detection */
    newValue = ADXL362_ACT_INACT_CTL_LINKLOOP(ADXL362_MODE_LOOP) | ADXL362_ACT_INACT_CTL_INACT_EN | ADXL362_ACT_INACT_CTL_ACT_EN;
    ADXL362_SetRegisterValue(newValue, ADXL362_REG_ACT_INACT_CTL, 1);

    // Setting the sensitivity/accelerometer range
    ADXL362_SetRange(ADXL362_RANGE_2G);


    /* Setting the rate */
    ADXL362_SetOutputRate(ADXL362_ODR_12_5_HZ);


    /*
     * Set the thresholds, timeouts and enable both act/inact
     * Time => Number of samples = (number of seconds * ODR) /2 (ODR is set 12.5 Hz)
     */

    ADXL362_SetupInactivityDetection(1, ADXL362_INACTIVITY_THRESHOLD , ((12.5 * inactive)/2));

    ADXL362_SetupActivityDetection(1, ADXL362_ACTIVITY_THRESHOLD , ((12.5 * active)/2));


    /* Enable the interrupts as "active low" and map AWAKE bit status to the pin */
    newValue = (ADXL362_INTMAP2_INT_LOW | ADXL362_INTMAP2_INACT | ADXL362_INTMAP2_ACT);
    ADXL362_SetRegisterValue(newValue, ADXL362_REG_INTMAP2, 1);


    // Set up power register and enable the motion detection
    newValue  = ADXL362_POWER_CTL_MEASURE(ADXL362_MEASURE_ON);
    newValue |= ADXL362_POWER_CTL_WAKEUP;
    newValue |= ADXL362_POWER_CTL_AUTOSLEEP; /* it works only if we use Link or Loop modes */
    ADXL362_SetRegisterValue(newValue, ADXL362_REG_POWER_CTL, 1);
    
}


/***************************************************************************/
/**
 * @brief Configures it to stream at some defaults..
 *
 *
 * @return None.
 *******************************************************************************/
 
void ADXL362_SetSensing(unsigned short rate, unsigned short range)
{
    unsigned char newValue = 0;
    
    RMLOG("ADXL362: Enabling sensing defaults");

    // Setting the range
    ADXL362_SetRange(range == 2 ? ADXL362_RANGE_2G :
                     range == 4 ? ADXL362_RANGE_4G :
                     range == 8 ? ADXL362_RANGE_8G :
                     ADXL362_RANGE_2G);
    
    // Setting the rate
    ADXL362_SetOutputRate(rate == 12  ? ADXL362_ODR_12_5_HZ :
                          rate == 25  ? ADXL362_ODR_25_HZ   :
                          rate == 50  ? ADXL362_ODR_50_HZ   :
                          rate == 100 ? ADXL362_ODR_100_HZ  :
                          rate == 200 ? ADXL362_ODR_200_HZ  :
                          rate == 400 ? ADXL362_ODR_400_HZ  :
                          ADXL362_ODR_12_5_HZ);
    
    // Enable ints active low
    newValue = (ADXL362_INTMAP2_INT_LOW | ADXL362_INTMAP2_DATA_READY);
    ADXL362_SetRegisterValue(newValue, ADXL362_REG_INTMAP2, 1);

    // Enable it all
    newValue  = ADXL362_POWER_CTL_MEASURE(ADXL362_MEASURE_ON);
    ADXL362_SetRegisterValue(newValue, ADXL362_REG_POWER_CTL, 1);

    // Enable chip level interrupts
    adxl_register_irq();
    
    // Clear the data bit by reading data..
    short datax,datay,dataz;
    ADXL362_GetXyz(&datax, &datay, &dataz);
    
}


/***************************************************************************/
/**
* @brief Reads multiple bytes from the device's FIFO buffer.
*
* @param pBuffer     - Stores the read bytes.
* @param bytesNumber - Number of bytes to read.
*
* @return None.
*******************************************************************************/

void ADXL362_GetFifoValue(unsigned char* pBuffer, unsigned short bytesNumber)
{
    unsigned char  buffer[512];
    unsigned short index = 0;
    
    buffer[0] = ADXL362_WRITE_FIFO;
    for(index = 0; index < bytesNumber; index++)
    {
        buffer[index + 1] = pBuffer[index];
    }
    SPI_Read(ADXL362_SLAVE_ID, buffer, bytesNumber + 1);
    for(index = 0; index < bytesNumber; index++)
    {
        pBuffer[index] = buffer[index + 1];
    }
}


/***************************************************************************/
/**
* @brief Resets the device via SPI communication bus.
*
* @return None.
*******************************************************************************/

void ADXL362_SoftwareReset(void)
{
    ADXL362_SetRegisterValue(ADXL362_RESET_KEY, ADXL362_REG_SOFT_RESET, 1);
}


/***************************************************************************/
/**
* @brief Places the device into standby/measure mode.
*
* @param pwrMode - Power mode.
*                  Example: 0 - standby mode.
*                  1 - measure mode.
*
* @return None.
*******************************************************************************/

void ADXL362_SetPowerMode(unsigned char pwrMode)
{
    unsigned char oldPowerCtl = 0;
    unsigned char newPowerCtl = 0;
    
    ADXL362_GetRegisterValue(&oldPowerCtl, ADXL362_REG_POWER_CTL, 1);
    newPowerCtl = oldPowerCtl & ~ADXL362_POWER_CTL_MEASURE(0x3);
    newPowerCtl = newPowerCtl |
    (pwrMode * ADXL362_POWER_CTL_MEASURE(ADXL362_MEASURE_ON));
    ADXL362_SetRegisterValue(newPowerCtl, ADXL362_REG_POWER_CTL, 1);
}


/***************************************************************************/
/**
* @brief Selects the measurement range.
*
* @param gRange - Range option.
*                  Example: ADXL362_RANGE_2G  -  +-2 g
*                           ADXL362_RANGE_4G  -  +-4 g
*                           ADXL362_RANGE_8G  -  +-8 g
*
* @return None.
*******************************************************************************/

void ADXL362_SetRange(unsigned char gRange)
{
    unsigned char oldFilterCtl = 0;
    unsigned char newFilterCtl = 0;
    
    ADXL362_GetRegisterValue(&oldFilterCtl, ADXL362_REG_FILTER_CTL, 1);
    newFilterCtl = oldFilterCtl & ~ADXL362_FILTER_CTL_RANGE(0x3);
    newFilterCtl = newFilterCtl | ADXL362_FILTER_CTL_RANGE(gRange);
    ADXL362_SetRegisterValue(newFilterCtl, ADXL362_REG_FILTER_CTL, 1);
    selectedRange = (1 << gRange) * 2;
}


/***************************************************************************/
/**
* @brief Selects the Output Data Rate of the device.
*
* @param outRate - Output Data Rate option.
*                  Example: ADXL362_ODR_12_5_HZ  -  12.5Hz
*                           ADXL362_ODR_25_HZ    -  25Hz
*                           ADXL362_ODR_50_HZ    -  50Hz
*                           ADXL362_ODR_100_HZ   -  100Hz
*                           ADXL362_ODR_200_HZ   -  200Hz
*                           ADXL362_ODR_400_HZ   -  400Hz
*
* @return None.
*******************************************************************************/

void ADXL362_SetOutputRate(unsigned char outRate)
{
    unsigned char oldFilterCtl = 0;
    unsigned char newFilterCtl = 0;
    
    ADXL362_GetRegisterValue(&oldFilterCtl, ADXL362_REG_FILTER_CTL, 1);
    newFilterCtl = oldFilterCtl & ~ADXL362_FILTER_CTL_ODR(0x7);
    newFilterCtl = newFilterCtl | ADXL362_FILTER_CTL_ODR(outRate);
    ADXL362_SetRegisterValue(newFilterCtl, ADXL362_REG_FILTER_CTL, 1);
}


/***************************************************************************/
/**
* @brief Reads the 3-axis raw data from the accelerometer.
*
* @param x - Stores the X-axis data(as two's complement).
* @param y - Stores the Y-axis data(as two's complement).
* @param z - Stores the Z-axis data(as two's complement).
*
* @return None.
*******************************************************************************/

void ADXL362_GetXyz(short* x, short* y, short* z)
{
    unsigned char xyzValues[6] = {0, 0, 0, 0, 0, 0};
    
    ADXL362_GetRegisterValue(xyzValues, ADXL362_REG_XDATA_L, 6);
    *x = ((short)xyzValues[1] << 8) + xyzValues[0];
    *y = ((short)xyzValues[3] << 8) + xyzValues[2];
    *z = ((short)xyzValues[5] << 8) + xyzValues[4];
}


/***************************************************************************/
/**
* @brief Reads the 3-axis raw data from the accelerometer and converts it to g.
*
* @param x - Stores the X-axis data.
* @param y - Stores the Y-axis data.
* @param z - Stores the Z-axis data.
*
* @return None.
*******************************************************************************/

void ADXL362_GetGxyz(float* x, float* y, float* z)
{
    unsigned char xyzValues[6] = {0, 0, 0, 0, 0, 0};
    
    ADXL362_GetRegisterValue(xyzValues, ADXL362_REG_XDATA_L, 6);
    *x = ((short)xyzValues[1] << 8) + xyzValues[0];
    *x /= (1000 / (selectedRange / 2));
    *y = ((short)xyzValues[3] << 8) + xyzValues[2];
    *y /= (1000 / (selectedRange / 2));
    *z = ((short)xyzValues[5] << 8) + xyzValues[4];
    *z /= (1000 / (selectedRange / 2));
}


/***************************************************************************/
/**
* @brief Reads the temperature of the device.
*
* @return tempCelsius - The value of the temperature(degrees Celsius).
*******************************************************************************/

float ADXL362_ReadTemperature(void)
{
    unsigned char rawTempData[2] = {0, 0};
    short         signedTemp     = 0;
    float         tempCelsius    = 0;
    
    ADXL362_GetRegisterValue(rawTempData, ADXL362_REG_TEMP_L, 2);
    signedTemp = (short)(rawTempData[1] << 8) + rawTempData[0];
    tempCelsius = (float)signedTemp * 0.065;
    
    return tempCelsius;
}


/***************************************************************************/
/**
* @brief Configures the FIFO feature.
*
* @param mode         - Mode selection.
*                       Example: ADXL362_FIFO_DISABLE      -  FIFO is disabled.
*                                ADXL362_FIFO_OLDEST_SAVED -  Oldest saved mode.
*                                ADXL362_FIFO_STREAM       -  Stream mode.
*                                ADXL362_FIFO_TRIGGERED    -  Triggered mode.
* @param waterMarkLvl - Specifies the number of samples to store in the FIFO.
* @param enTempRead   - Store Temperature Data to FIFO.
*                       Example: 1 - temperature data is stored in the FIFO
*                                    together with x-, y- and x-axis data.
*                                0 - temperature data is skipped.
*
* @return None.
*******************************************************************************/

void ADXL362_FifoSetup(unsigned char  mode,
                       unsigned short waterMarkLvl,
                       unsigned char  enTempRead)
{
    unsigned char writeVal = 0;
    
    writeVal = ADXL362_FIFO_CTL_FIFO_MODE(mode) |
    (enTempRead * ADXL362_FIFO_CTL_FIFO_TEMP) |
    ADXL362_FIFO_CTL_AH;
    ADXL362_SetRegisterValue(writeVal, ADXL362_REG_FIFO_CTL, 1);
    ADXL362_SetRegisterValue(waterMarkLvl, ADXL362_REG_FIFO_SAMPLES, 2);
}


/***************************************************************************/
/**
* @brief Configures activity detection.
*
* @param refOrAbs  - Referenced/Absolute Activity Select.
*                    Example: 0 - absolute mode.
*                             1 - referenced mode.
* @param threshold - 11-bit unsigned value that the adxl362 samples are
*                    compared to.
* @param time      - 8-bit value written to the activity timer register. The 
*                    amount of time (in seconds) is: time / ODR, where ODR - is 
*                    the output data rate.
*
* @return None.
*******************************************************************************/

void ADXL362_SetupActivityDetection(unsigned char  refOrAbs,
                                    unsigned short threshold,
                                    unsigned char  time)
{
    unsigned char oldActInactReg = 0;
    unsigned char newActInactReg = 0;
    
    /* Configure motion threshold and activity timer. */
    ADXL362_SetRegisterValue((threshold & 0x7FF), ADXL362_REG_THRESH_ACT_L, 2);
    RMLOG("ADXL362: Setting Activity timer to %u cycles.", time);
    ADXL362_SetRegisterValue(time, ADXL362_REG_TIME_ACT, 1);

    /* Enable activity interrupt and select a referenced or absolute configuration. */
    ADXL362_GetRegisterValue(&oldActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
    newActInactReg = oldActInactReg & ~ADXL362_ACT_INACT_CTL_ACT_REF;
    newActInactReg |= ADXL362_ACT_INACT_CTL_ACT_EN |
    (refOrAbs * ADXL362_ACT_INACT_CTL_ACT_REF);
    ADXL362_SetRegisterValue(newActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
}


/***************************************************************************/
/**
* @brief Configures inactivity detection.
*
* @param refOrAbs  - Referenced/Absolute Inactivity Select.
*                    Example: 0 - absolute mode.
*                             1 - referenced mode.
* @param threshold - 11-bit unsigned value that the adxl362 samples are
*                    compared to.
* @param time      - 16-bit value written to the inactivity timer register. The 
*                    amount of time (in seconds) is: time / ODR, where ODR - is  
*                    the output data rate.
*
* @return None.
*******************************************************************************/

void ADXL362_SetupInactivityDetection(unsigned char  refOrAbs,
                                      unsigned short threshold,
                                      unsigned short time)
{
    unsigned char oldActInactReg = 0;
    unsigned char newActInactReg = 0;
    
    /* Configure motion threshold and inactivity timer. */
    ADXL362_SetRegisterValue((threshold & 0x7FF),
                             ADXL362_REG_THRESH_INACT_L,
                             2);
    RMLOG("ADXL362: Setting Inactivity timer to %u cycles.", time);
    ADXL362_SetRegisterValue(time, ADXL362_REG_TIME_INACT_L, 2);

    /* Enable inactivity interrupt and select a referenced or absolute configuration. */
    ADXL362_GetRegisterValue(&oldActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
    newActInactReg = oldActInactReg & ~ADXL362_ACT_INACT_CTL_INACT_REF;
    newActInactReg |= ADXL362_ACT_INACT_CTL_INACT_EN |
    (refOrAbs * ADXL362_ACT_INACT_CTL_INACT_REF);
    ADXL362_SetRegisterValue(newActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
}


/***************************************************************************/
/**
* @brief Disable activity detection.
*
* @return None.
*******************************************************************************/

void ADXL362_DisableActivityDetection(void)
{
    unsigned char oldActInactReg = 0;
    unsigned char newActInactReg = 0;

    /* Disable activity interrupt. */
    ADXL362_GetRegisterValue(&oldActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
    newActInactReg = oldActInactReg & ~ADXL362_ACT_INACT_CTL_ACT_EN;
    ADXL362_SetRegisterValue(newActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
}


/***************************************************************************/
/**
* @brief Disable inactivity detection.
*
* @return None.
*******************************************************************************/

void ADXL362_DisableInactivityDetection(void)
{
    unsigned char oldActInactReg = 0;
    unsigned char newActInactReg = 0;

    /* Disable inactivity interrupt. */
    ADXL362_GetRegisterValue(&oldActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
    newActInactReg = oldActInactReg & ~ADXL362_ACT_INACT_CTL_INACT_EN;
    ADXL362_SetRegisterValue(newActInactReg, ADXL362_REG_ACT_INACT_CTL, 1);
}


/***************************************************************************/
/**
* @brief Set GPIO pin driving VDD_ADXL.
*
* @return None.
*******************************************************************************/

void ADXL362_power_on(void)
{
    /* We want to guarantee ADXL's caps are completely discharged before power it ON */
    ADXL362_power_off();

    /* Set the GPIO as control pins */
    nrf_gpio_pin_set(ADXL_POWER);
    nrf_gpio_cfg_output(ADXL_POWER);

    /* Power-up to standby: 5ms */
    nrf_delay_ms(10);

    RMLOG("ADXL362: Powered ON");

}


/***************************************************************************/
/**
* @brief Clear GPIO pin driving VDD_ADXL and discharge the capacitors
*        connected to VDD_ADXL.
*
* @return None.
*******************************************************************************/

void ADXL362_power_off(void)
{
    /* Disable SPI interface in case it has been enabled */
    SPI_Uninit();

    /* Set ADXL power GPIO pin LOW */
    nrf_gpio_pin_clear(ADXL_POWER);
    nrf_gpio_cfg_output(ADXL_POWER);

    /* Discharge the 2 caps (C=100 nF each) connected to ADXL_POWER (VDD_ADXL on schematics) */
    nrf_gpio_cfg_input(ADXL_POWER, NRF_GPIO_PIN_PULLDOWN);
    /* Given a max pulldown resistor of 16kΩ (R), discharge time = 5*RC = 5 * 16kΩ * 2x100nF = 16 ms (rounded up to 20ms) */
    nrf_delay_ms(20);
    /* Actually after testing it seems the AXDL requires more that additional 200ms in order to reset cleanly */
    nrf_delay_ms(500);

    /* Set GPIO pin for ADXL_POWER to default (input, no pull) */
    //nrf_gpio_cfg_default(ADXL_POWER);

    /* Set all GPIO pins used by ADXL362 to default (input, no pull, no sense, S0S1, input disconnect) */
    ADXL362_release_gpio_pins();

    RMLOG("ADXL362: Powered OFF");
    
}


/***************************************************************************/
/**
* @brief Go through the hardware startup sequence and power up the motion
*        sensor, setup it's interrupt handler and other general good stuff
*
* @return None.
*******************************************************************************/

uint8_t ADXL362_Start(void)
{
    /* Power up the sensor */
    ADXL362_power_on();

    /* Let's try to initialize the sensor */
    if (ADXL362_Init() != 0) {

        RMLOG("ADXL362: FAILED to initialise the communication interface with the sensor");

        /* Sensor not working: power it off */
        ADXL362_power_off();

        /* Release GPIO pin and set them to default */
        ADXL362_release_gpio_pins();
        
        return false;
    }
    
    /* Configure the ADXL internal registers */
    RMLOG("ADXL362: Configuring the motion controller to motion mode");
    ADXL362_SetMotionMode(LIFESOLE_ACTIVITY_SECONDS, LIFESOLE_INACTIVITY_INIT_TIME);

    return true;

}


/*
 * Perform a self-test procedure
 */

void ADXL362_SelfTest(void)
{
    RMLOG ("ADXL362: Running Self-Test for 200 ms");

    /* Start the self-test */
    ADXL362_SetRegisterValue(1, ADXL362_REG_SELF_TEST, 1);
    
    /* Let the self-test run for one sampling period (@ 6.25Hz, T=160ms)*/
    nrf_delay_ms(200); 
    
    /* Stop the self-test */
    ADXL362_SetRegisterValue(0, ADXL362_REG_SELF_TEST, 1);

}
