/* 
 * Droplet Firmware
 * ----------------
 *
 * Author: Chris Moore
 * 
 * This is the firmware for Roam Creative Ltd iBeacon implementation with various enhancements
 *  to aid battery life and detection.
 *
 * (c) Roam Creative Ltd 2014
 *
 */

#ifndef DROPLET_H__
#define DROPLET_H__

// --------------------------- INCLUDES ----------------------------------

#include <stdbool.h>
#include <stdint.h>
#include "app_util.h"
#include "app_timer_appsh.h"
#include "ble_bas.h"

// --------------------------- CONSTANTS ----------------------------------


// --------------------------- CONSTANTS ----------------------------------

#define LED_R_MSK  (1UL << LED_RED)
#define LED_G_MSK  (1UL << LED_GREEN)

#define BOOTLOADER_BUTTON_PIN           BUTTON_1                                    /**< Button used to enter DFU mode. */
#define CONFIG_MODE_BUTTON_PIN          BUTTON_2                                    /**< Button used to enter config mode. */
#define APP_GPIOTE_MAX_USERS            2  
#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)  

#define ASSERT_LED_PIN_NO      LED_RED

#define APP_CFG_NON_CONN_ADV_TIMEOUT  0                                             /**< Time for which the device must be advertising in non-connectable mode (in seconds). 0 disables timeout. */

// -----------------------------------
#define DEVICE_NAME                     MODEL_NAME                   				/**< Name of device. Will be included in the advertising data. */

// Device Information
#define MODEL_MANU_NAME                 "Roam Creative NZ Ltd"
#define MODEL_SERIAL_NUMBER				"LIFE_00000001"

#define MODEL_HW_REVISION				"R2.1"																			/**< Hardware revision */
#define MODEL_FW_REVISION				"SDK11-S132v2.0"																			/**< Soft device version */
#if DROPLET_RELEASE_MODE
  #define MODEL_SW_REVISION				"R4.0"																			/**< Roam's droplet software version */
#else
  #define MODEL_SW_REVISION				"S4.0"																			/**< Roam's droplet software version */
#endif

#define APP_ADV_FAST_INTERVAL           160                                          /**< The advertising interval (in units of 0.625 ms). The default value corresponds to 100 ms */
#define APP_ADV_SLOW_INTERVAL           320                                         /**< Slow advertising interval (in units of 0.625 ms). The default value corresponds to 200 ms */
#define APP_ADV_FAST_TIMEOUT            0 // 30 //120                               /**< The advertising time-out in units of seconds. */
#define APP_ADV_SLOW_TIMEOUT            0                                           /**< The advertising time-out in units of seconds. */


#define APP_ADV_INTERVAL                MSEC_TO_UNITS(100, UNIT_0_625_MS)           /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS      0	                                         /**< The advertising timeout (in units of seconds). */

#define MIN_CONN_INTERVAL               (MSEC_TO_UNITS(15, UNIT_1_25_MS))             /**< Minimum acceptable connection interval (11.25 milliseconds). */
#define MAX_CONN_INTERVAL               (MSEC_TO_UNITS(30, UNIT_1_25_MS))             /**< Maximum acceptable connection interval (15 milliseconds). */

#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(5000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#if 0 // Original...
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(20000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)  /**< Time between each call to sd_ble_gap_conn_param_update after the first (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */
#else
 #define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(200000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
 #define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(50000, APP_TIMER_PRESCALER)  /**< Time between each call to sd_ble_gap_conn_param_update after the first (5 seconds). */
 #define MAX_CONN_PARAMS_UPDATE_COUNT    30                                           /**< Number of attempts before giving up the connection parameter negotiation. */
#endif

#define SEC_PARAM_TIMEOUT               30                                          /**< Timeout for Pairing Request or Security Request (in seconds). */
#define SEC_PARAM_BOND                  0                                           /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                           /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                        /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                           /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                           /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                          /**< Maximum encryption key size. */

// -----------------------------------------


#define DEAD_BEEF                    	0xDEADBEEF                        /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define APP_MEASURED_RSSI							0xC3															// Default


#define APP_TIMER_PRESCALER         0                                   /**< RTC prescaler value used by app_timer */
#define APP_TIMER_MAX_TIMERS        6                                   /**< One for each module + one for ble_conn_params + a few extra */
#define APP_TIMER_OP_QUEUE_SIZE     6                                   /**< Maximum number of timeout handlers pending execution */

#define FLASH_LED_INTERVAL          			 APP_TIMER_TICKS(2000, APP_TIMER_PRESCALER)
#define BUZZER_PIP_INTERVAL                      APP_TIMER_TICKS(250,  APP_TIMER_PRESCALER)
#define BUZZER_SHORT_INTERVAL          			 APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER)
#define BUZZER_LONG_INTERVAL          			 APP_TIMER_TICKS(3000, APP_TIMER_PRESCALER)
#define ADV_REVIEW_INTERVAL	          			 APP_TIMER_TICKS(1000 * 60, APP_TIMER_PRESCALER)			// Every minute we check whether to swap


#define SCHED_MAX_EVENT_DATA_SIZE       sizeof(app_timer_event_t)       /**< Maximum size of scheduler events. Note that scheduler BLE stack events do not contain any data, as the events are being pulled from the stack in the event handler. */
#define SCHED_QUEUE_SIZE                60                              /**< Maximum number of events in the scheduler queue. */

#define MAGIC_FLASH_BYTE 0x34                                           /**< Magic byte used to recognise that flash has been written */


#define DROPLET_PASSCODE_RETRIES		3																		// Number of retries before we lock down and ignore DFU attempts
#define DROPLET_LOCKOUT_MINUTES			15																	// 15 minutes of lockout after failed DFU attempts

#define DROPLET_MIN_ADV_RATE_MS			100 							/**< Don't use 20, doesn't like it when advertising when connected */
#define DROPLET_MAX_ADV_RATE_MS			1500							/**< 1.5 seconds is enough */

#define BATTERY_LEVEL_MEAS_INTERVAL                 APP_TIMER_TICKS(24*60*60*1000, APP_TIMER_PRESCALER)        /**< Battery level measurement interval (ticks) */
#define BATTERY_WARNING_LEVEL                       10  // Level percentage
    
#define DROPLET_DEFAULT_SECURITY_MODE               SECURITY_MODE_NO_PASSWORD
#define DROPLET_DEFAULT_PASSCODE                    "sixthsense"
#define DROPLET_DEFAULT_TX_POWER				    +4
#define DROPLET_DEFAULT_ADV_RATE	         	    350
#define DROPLET_DEFAULT_SECOND_ADV_RATE	      	    1000
#define DROPLET_DEFAULT_CHANGE_ADV_RATE	      	    100


#define SEC_CORRECT_ANSWER                          APP_TIMER_TICKS((30 * 1000), APP_TIMER_PRESCALER)  /**< Amount of ms without correct answer before kicking */
#define SEC_KICKOFF_DELAY                           APP_TIMER_TICKS((15 * 60 * 1000), APP_TIMER_PRESCALER) /**< Amount of ms that connectable is disabled */
#define SENSE_DISABLE_TIMEOUT                       APP_TIMER_TICKS((30 * 1000), APP_TIMER_PRESCALER)      /**< Amount of ms sensors run without connection */

#define PROX_CHECK_TIMER                            APP_TIMER_TICKS((1 * 1000), APP_TIMER_PRESCALER)      /**< Amount of ms sensors run without connection */

#define DEVICE_NAME_MAX_LEN                         32

// -----------------------------------------

/* Define/Undefine following MACROs */

/*
 * Enable a 24-hour timer to be set for performing a daily check of the battery level
 * LifeSole will be most of the time in SystemOff mode, thus not allowing any timer to last 24h.
 */
#undef BATTERY_TIMER

/*
 * Enable the Bluetooth Battery Service to send information about battery level at measurement completed
 * LifeSole does not need such functionality, since battery level is checked before starting advertising.
 */
#undef BLE_BAS_BATTERY_LEVEL_UPDATE

// Enable StateMachine multiple event management
// TODO: to remove all code related to it, since such funtionality is not required anymore.
#undef CONCURRENT_EVENTS_SUPPORT

/*
 * Following MACRO specify which electrode is connected to the SX9300.
 * Only one of the following macros can be defined at a time.
 */
#undef USING_SEMTECH_ELECTRODE
#undef USING_ROAM_ELECTRODE_V1	// Long Version
#define USING_ROAM_ELECTRODE_V2	// Form-factor as Semtech original one

/*
 * This MACRO allows LifeSole to save power by not starting advertising in case of a BLE disconnection after the
 * Smartphone app has connected to LifeSole while it's advertising (during ALERT and CONFIG states).
 * In case of ALERT state, a connection event is interpreted as a notification that the warning has been received,
 * thus it is possible to move to next state and stop the RADIO module asap in order to save power.
 * In case of CONFIG state, a connection event will restart the timer in order to allow enough time to the phone app
 * to configure (not implemented yet) LifeSole parameters (e.g. inativity timeout value). Once the app has completed
 * the configuration, it is supposed to disconnect. A disconnect (as well as a timeout) event, will trigger LifeSole
 * to move to next state in order to stop RADIO module. Thus, it will not start advertising again.
 * In addition, during CONFIG state, it is possible to trigger a DFU, by connecting and sending proper value (0x8000)
 * to DFU service/characteristic. 
 */
#ifdef APPLICATION
#define NO_ADVERTISING_AFTER_DISCONNECT 
#endif

// Enable routine performing BLE testing infinite loop (SX9300_TEST_MODE must not be defined)
#undef BLE_TEST_MODE

// Enable routine performing SX9300 testing infinite loop
#undef SX9300_TEST_MODE
// Provides additional LOG messages related to FPCs (it can be used together or without SX9300_TEST_MODE)
#undef SX9300_FPC_TEST


// --------------------------- ENUM TYPES ----------------------------------


typedef enum
{
	DROPLET_MODE_ENABLED 	  = (1 << 0)                            // not used on sensor
} droplet_mode_t;


// ------------------------------ STRUCTS ----------------------------------

/*
 * This is the simple data config structure that is written down to flash.
 *
 *    ** DO NOT REARRANGE THEY ARE RAW COPIED TO MODULES STRUCTURES **
 *
 *           ie: restoreSenseConfig(..)
 *
 */

typedef struct
{
    uint8_t     magic_byte;                                                             // Whether the config is valid
    uint8_t     device_name[DEVICE_NAME_MAX_LEN];                                       // Device name (31 chars)
    
    // Roam Configuration Service - Must match the BLE services struct size/order
    uint8_t     tx_level;
    uint16_t    adv_primary_timeout;                                                    // Seconds before switching to secondary rate
	uint16_t    adv_primary_rate;
    uint16_t    adv_secondary_rate;
    uint16_t    security_ctrl;                                                          // Security mode
	uint16_t    security_mode;                                                          // Security mode
	uint8_t     passcode[20];															// Passcode (20characters)

} __packed flash_db_layout_t;

typedef union
{
    flash_db_layout_t data;
    uint32_t padding[CEIL_DIV(sizeof(flash_db_layout_t), 4)];
} flash_db_t;


#endif // DROPLET_H__
