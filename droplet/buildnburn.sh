function fail {
echo Failed. Stopping.
exit
}

echo Building bootloader
make -C bootloader
[ ! $? ] && fail

echo Building application
make -C application
[ ! $? ] && fail

# Burn
echo Burning Bootloader
nrfjprog program -b bootloader/nrf52832_xxaa_s132.hex
[ ! $? ] && fail

echo Burning Softdevice
nrfjprog program -s ./../binaries/s132_nrf52_1.0.0-3.alpha_softdevice.hex
[ ! $? ] && fail

echo Burning Application
nrfjprog program -c application/_build/nrf52832_xxaa.hex
[ ! $? ] && fail

echo "Success :)"