Roam Creative Ltd - Lifesole 2017
 - Bootloader started
NRF_FICR->CODEPAGESIZE : 0x1000, CODE_PAGE_SIZE: 0x1000 
Roam Creative Ltd - Lifesole 2017
 - Bootloader started
 - No buttons on this board
 - No DFU in progress
 - NOT Initing BLE
 - NOT Initing Scheduler
 - Initing Softdevice Handler
 - No bootloader button


LifeSole - v1.0

Reset reason [0x00010000] = DETECT SIGNAL FROM GPIO
leds_init(): LEDs are connected to GND
Skipping BLE Stack initialisation for the moment
Enabling DC/DC converter (Radio power saving)
Creating timer for general usage
Restoring preserved state (2) with GPIO wakeup fired

State changing from LS_STATE_WAITING_MOTION (0x88) ==>> LS_STATE_WAITING_MOTION (0x88)
Restoring drivers interrupt handlers and SPI/TWI interfaces
SX9300: Already powered, proceeding with partial initialising...
SX9300: I2C inited at address 0x28 SCL is 9 and SDA is 12
SX9300: Registering Interrupt
SX9300: Interrupt registered for SX9300
SX9300: Creating timer
SX9300: SX9300 interrupt enabled
ADXL362: SPI Init
 MISO  4
 MOSI  3
 CLOCK 2
 CS    5
ADXL362: SPI interface initialised
ADXL362: Registering Interrupt
ADXL362: Interrupt registered
Using the latch (0x00000040), determine what fired the wakeup
Was ADXL362
ADXL362: force scheduling of IRQ handler

!
Scheduling event: LS_EVENT_MOTION_TRIGGERED
Fire event: LS_EVENT_MOTION_TRIGGERED

State changing from LS_STATE_WAITING_MOTION (0x88) ==>> LS_STATE_MOTION_DETECTED (0x8C)

We entered ls_motion_detected
Clearing previous request to SLEEP if any
ADXL362: Setting Inactivity timer to 187 cycles.
SX9300: Already powered, proceeding with partial initialising...
SX9300: Creating timer
SX9300: SX9300 interrupt enabled
SX9300: Enabling FASTSCAN
SX9300: Wake the SX9300
SX9300: Enabling CONVERSION DONE interrupt
SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]
SX9300: Starting single shot timer for 20 seconds


SX9300: Enabling FASTSCAN
SX9300: Enabling CONVERSION DONE interrupt
SX9300: Disabling DOZE MODE
SX9300: Skipping first samples after re-enabling TX (10)

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]
+
SX9300: Body detected
SX9300: PROXDIFF-0B 0x07FA
SX9300: smaller PROXDIFF 0x07FA
SX9300: PROXDIFF too big -> set SX9300_REG_PROX_CTRL6 to max  0x1F
SX9300: previous SX9300_REG_PROX_CTRL6 (0x1F) bigger than/equal to the new one! Not changing it!
SX9300: Stopping timer
Proximity being set to True in GPREGRET register.
Scheduling event: LS_EVENT_BODY_DETECTED
SX9300: Sleep the SX9300


Fire event: LS_EVENT_BODY_DETECTED

State changing from LS_STATE_MOTION_DETECTED (0x8C) ==>> LS_STATE_ENTER_CONFIGURATION (0xA8)

We entered ls_enter_configuration
Clearing previous request to SLEEP if any
battery_level_check(): trigger a battery measurement!
Configuring the ADC for battery measurement
Triggering a battery measurement
adc_result 809, batt_lvl_in_milli_volts 2844, percentage_batt_lvl 34
In this version of the firmware BLE battery level service is not used
Battery is 34
Trigger BLE advertising for starting a configuration process
Flash storage requires 64 bytes
Restoring configuration
Restoring RMCS config of 31 bytes
Setting device name to Lifesole
Device serial number is C6:45:86:A8:7C:B9
Initialised configuration service
advertising_start(): sd_ble_gap_adv_stop() returned err code 0x00000008
Setting RSSI to 0x22
Initialising advertising and set power level to 4
Advertising fast
Starting advertising with alerting not set
Starting single shot timer for 5 seconds



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


Idling the advertising

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode **************Scheduling event: LS_EVENT_TIMER_EXPIRED
****


Fire event: LS_EVENT_TIMER_EXPIRED

State changing from LS_STATE_ENTER_CONFIGURATION (0xA8) ==>> LS_STATE_EXIT_CONFIGURATION (0xAC)

We entered ls_exit_configuration
Clearing previous request to SLEEP if any
Stop advertising...
sd_ble_gap_adv_stop() returned 0x00000008
Stopping timer
Scheduling event: LS_EVENT_EXIT_CONFIG_MODE
Fire event: LS_EVENT_EXIT_CONFIG_MODE

State changing from LS_STATE_EXIT_CONFIGURATION (0xAC) ==>> LS_STATE_BODY_DETECTED (0x94)

We entered ls_body_detected
ADXL362: force scheduling of IRQ handler

!
Reconfiguring motion timeouts for inactivity to 10 seconds
ADXL362: Setting Inactivity timer to 62 cycles.
ADXL362: Running Self-Test for 200 ms
Waiting for inactivty now that the foot is in there
Setting request to SLEEP

Scheduling event: LS_EVENT_MOTION_TRIGGERED
Fire event: LS_EVENT_MOTION_TRIGGERED
Invalid state event transitions from LS_STATE_BODY_DETECTED with event LS_EVENT_MOTION_TRIGGERED!

We entered ls_invalid
Clearing previous request to SLEEP if any
Setting request to SLEEP

SX9300: NO measurement in progress!
ADXL362: NO IRQ pending!
Scheduling Sleep function
SX9300: Unitialising TWI/I2C module...
SX9300: Releasing TWI/I2C GPIO pins...
SX9300: I2C interface at address 0x28 has been unitialised

Entering Sleep

==================== nRF52832 in SYSTEM OFF (Lowest Power Mode) ====================


Roam Creative Ltd - Lifesole 2017
 - Bootloader started
NRF_FICR->CODEPAGESIZE : 0x1000, CODE_PAGE_SIZE: 0x1000 
Roam Creative Ltd - Lifesole 2017
 - Bootloader started
 - No buttons on this board
 - No DFU in progress
 - NOT Initing BLE
 - NOT Initing Scheduler
 - Initing Softdevice Handler
 - No bootloader button


LifeSole - v1.0

Reset reason [0x00010000] = DETECT SIGNAL FROM GPIO
leds_init(): LEDs are connected to GND
Skipping BLE Stack initialisation for the moment
Enabling DC/DC converter (Radio power saving)
Creating timer for general usage
Restoring preserved state (5) with GPIO wakeup fired

State changing from LS_STATE_BODY_DETECTED (0x94) ==>> LS_STATE_BODY_DETECTED (0x94)
Restoring drivers interrupt handlers and SPI/TWI interfaces
SX9300: Already powered, proceeding with partial initialising...
SX9300: I2C inited at address 0x28 SCL is 9 and SDA is 12
SX9300: Registering Interrupt
SX9300: Interrupt registered for SX9300
SX9300: Creating timer
SX9300: SX9300 interrupt enabled
ADXL362: SPI Init
 MISO  4
 MOSI  3
 CLOCK 2
 CS    5
ADXL362: SPI interface initialised
ADXL362: Registering Interrupt
ADXL362: Interrupt registered
Using the latch (0x00000040), determine what fired the wakeup
Was ADXL362
ADXL362: force scheduling of IRQ handler

!
Scheduling event: LS_EVENT_NO_MOTION_TRIGGERED
Fire event: LS_EVENT_NO_MOTION_TRIGGERED

State changing from LS_STATE_BODY_DETECTED (0x94) ==>> LS_STATE_NO_MOTION_DETECTED (0x9C)

We entered ls_no_motion_detected
Clearing previous request to SLEEP if any
SX9300: Wake the SX9300
SX9300: Enabling CONVERSION DONE interrupt
SX9300: Enabling FASTSCAN
SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]
SX9300: Starting single shot timer for 20 seconds


SX9300: Enabling FASTSCAN
SX9300: Enabling CONVERSION DONE interrupt
SX9300: Disabling DOZE MODE
SX9300: Skipping first samples after re-enabling TX (10)

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]
+
SX9300: Body detected
SX9300: PROXDIFF-0B 0x07FA
SX9300: smaller PROXDIFF 0x07FA
SX9300: PROXDIFF too big -> set SX9300_REG_PROX_CTRL6 to max  0x1F
SX9300: previous SX9300_REG_PROX_CTRL6 (0x1F) bigger than/equal to the new one! Not changing it!
SX9300: Stopping timer
Proximity being set to True in GPREGRET register.
Scheduling event: LS_EVENT_BODY_DETECTED
SX9300: Sleep the SX9300


Fire event: LS_EVENT_BODY_DETECTED

State changing from LS_STATE_NO_MOTION_DETECTED (0x9C) ==>> LS_STATE_ALERT (0xA0)

We entered ls_alert
Clearing previous request to SLEEP if any
SX9300: Sleep the SX9300
battery_level_check(): trigger a battery measurement!
Configuring the ADC for battery measurement
Triggering a battery measurement
adc_result 809, batt_lvl_in_milli_volts 2844, percentage_batt_lvl 34
In this version of the firmware BLE battery level service is not used
Battery is 34
Trigger BLE advertising of alert
Flash storage requires 64 bytes
Restoring configuration
Restoring RMCS config of 31 bytes
Setting device name to Lifesole
Device serial number is C6:45:86:A8:7C:B9
Initialised configuration service
advertising_start(): sd_ble_gap_adv_stop() returned err code 0x00000008
Setting RSSI to 0xA2
Initialising advertising and set power level to 4
Advertising fast
Starting advertising with alerting set
Starting single shot timer for 15 seconds



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


Idling the advertising

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ***********Scheduling event: LS_EVENT_TIMER_EXPIRED
*******


Fire event: LS_EVENT_TIMER_EXPIRED

State changing from LS_STATE_ALERT (0xA0) ==>> LS_STATE_ALERT_FINISHED (0xA4)

We entered ls_alert_finished
Clearing previous request to SLEEP if any
Stop advertising...
sd_ble_gap_adv_stop() returned 0x00000008
Stopping timer
ADXL362: force scheduling of IRQ handler

!
Scheduling event: LS_EVENT_NO_MOTION_TRIGGERED
Fire event: LS_EVENT_NO_MOTION_TRIGGERED

State changing from LS_STATE_ALERT_FINISHED (0xA4) ==>> LS_STATE_WAITING_MOTION (0x88)

We entered ls_waiting_motion
SX9300: Body had been detected previously. Let's double check!
SX9300: Wake the SX9300
SX9300: Enabling CONVERSION DONE interrupt
SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]
SX9300: Starting single shot timer for 20 seconds


SX9300: Enabling FASTSCAN
SX9300: Enabling CONVERSION DONE interrupt
SX9300: Disabling DOZE MODE
SX9300: Skipping first samples after re-enabling TX (10)

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]

****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]



****************** nRF52832 in SYSTEM ON (Lowest Power SLEEP Mode ******************


SX9300: SX9300 irqSrc:status regs [0x09]:[0xC0]
+
SX9300: Body detected
SX9300: PROXDIFF-0B 0x07FA
SX9300: smaller PROXDIFF 0x07FA
SX9300: PROXDIFF too big -> set SX9300_REG_PROX_CTRL6 to max  0x1F
SX9300: previous SX9300_REG_PROX_CTRL6 (0x1F) bigger than/equal to the new one! Not changing it!
SX9300: Stopping timer
Proximity being set to True in GPREGRET register.
Scheduling event: LS_EVENT_BODY_DETECTED
SX9300: Sleep the SX9300


Fire event: LS_EVENT_BODY_DETECTED

State changing from LS_STATE_WAITING_MOTION (0x88) ==>> LS_STATE_WAITING_MOTION (0x88)

We entered ls_waiting_motion
SX9300: Sleep the SX9300
ADXL362: force scheduling of IRQ handler

!
Setting request to SLEEP

Scheduling event: LS_EVENT_NO_MOTION_TRIGGERED
Fire event: LS_EVENT_NO_MOTION_TRIGGERED
Invalid state event transitions from LS_STATE_WAITING_MOTION with event LS_EVENT_NO_MOTION_TRIGGERED!

We entered ls_invalid
Clearing previous request to SLEEP if any
Setting request to SLEEP

SX9300: NO measurement in progress!
ADXL362: NO IRQ pending!
Scheduling Sleep function
SX9300: Unitialising TWI/I2C module...
SX9300: Releasing TWI/I2C GPIO pins...
SX9300: I2C interface at address 0x28 has been unitialised

Entering Sleep

==================== nRF52832 in SYSTEM OFF (Lowest Power Mode) ====================


