PROJECT_PATH = $(shell pwd)

all: application bootloader merge&flash
	

application:
	$(PROJECT_PATH)/release/application.sh

bootloader:
	$(PROJECT_PATH)/release/bootloader.sh


merge_flash:
	$(PROJECT_PATH)/release/merge_flash.sh
